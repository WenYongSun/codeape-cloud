package com.code.ape.codeape.device.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.admin.api.feign.RemoteUserService;
import com.code.ape.codeape.common.core.constant.SecurityConstants;
import com.code.ape.codeape.common.core.util.AssertUtil;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.device.api.dto.QcRecordDTO;
import com.code.ape.codeape.device.api.entity.QcRecord;
import com.code.ape.codeape.device.api.entity.QcRecordUnique;
import com.code.ape.codeape.device.api.vo.QcRecordVO;
import com.code.ape.codeape.device.mapper.QcRecordUniqueMapper;
import com.code.ape.codeape.device.service.DeviceInfoService;
import com.code.ape.codeape.device.service.PaperInfoService;
import com.code.ape.codeape.device.service.QcLiquidInfoService;
import com.code.ape.codeape.device.service.QcRecordUniqueService;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 质控记录
 * 该实现类用于处理PDA质控记录逻辑
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-28 12:15:42
 */
@Service
@Slf4j
public class QcRecordPDAServiceImpl extends QcRecordServiceImpl {


	@Resource
	private TransactionTemplate transactionTemplate;

	public QcRecordPDAServiceImpl(QcLiquidInfoService qcLiquidInfoService, PaperInfoService paperInfoService, DeviceInfoService deviceInfoService, QcRecordUniqueMapper qcRecordUniqueMapper, RemoteUserService remoteUserService, QcRecordUniqueService qcRecordUniqueService) {
		super(qcLiquidInfoService, paperInfoService, deviceInfoService, qcRecordUniqueMapper, remoteUserService, qcRecordUniqueService);
	}


	@Override
	public boolean support(String clientId) {
		return StrUtil.equals(SecurityConstants.PDA,clientId);
	}

	@Override
	public Boolean saveQcRecord(QcRecordDTO dto){
		return super.saveQcRecord(dto);
	}

	@Override
	public Boolean deleteQcRecord(Long id){
		AssertUtil.isTrue(false,"该客户端不支持该接口");
		return Boolean.FALSE;
	}

	@Override
	public Boolean updateQcRecord(QcRecordDTO dto){
		AssertUtil.isTrue(false,"该客户端不支持该接口");
		return Boolean.FALSE;
	}

	@Override
	public Page<QcRecordVO> listPage(Page<QcRecordVO> page, QcRecordDTO dto){
		return super.listPage(page,dto);
	}



	/**
	 * 批量新增质控记录，PDA端
	 * 此接口由PDA离线新增接口，省去如下校验：
	 * 1. 试纸
	 * 2. 质控液
	 * 3. 结果
	 * 4. 设备
	 * @param dtos 质控记录集合
	 */
	@Override
	public Boolean saveBatchQcRecord(List<QcRecordDTO> dtos) {
		//step1 加入防重表，这里可能存在重复上传的数据，如果批量上传，会导致全部失败，因此这里单条添加
		for (QcRecordDTO dto : dtos) {
			try{
				insertQcRecord(dto);
			}catch (Exception ex){
				if (log.isErrorEnabled())
					log.error("添加一条质控记录异常",ex);
			}
		}
		return Boolean.TRUE;
	}

	private void insertQcRecord(QcRecordDTO dto){
		//开启编程事务，保证防重表和业务表原子性
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				QcRecordUnique recordUnique = BeanUtil.toBean(dto, QcRecordUnique.class);
				int uniqueRows = getQcRecordUniqueMapper().insert(recordUnique);
				QcRecord qcRecord = BeanUtil.toBean(dto, QcRecord.class);
				qcRecord.setId(recordUnique.getId());
				//插入业务表
				int qcRecordRows=baseMapper.insert(qcRecord);
				Assert.isTrue(qcRecordRows==1,"添加一条质控记录失败");
			}
		}) ;
	}

	@Override
	public int getOrder() {
		return Ordered.HIGHEST_PRECEDENCE+1;
	}
}
