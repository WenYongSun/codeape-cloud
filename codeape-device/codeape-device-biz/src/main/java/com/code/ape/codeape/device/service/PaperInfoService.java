

package com.code.ape.codeape.device.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.device.api.dto.PaperInfoDTO;
import com.code.ape.codeape.device.api.entity.PaperInfo;
import com.code.ape.codeape.device.api.vo.PaperInfoVO;

import java.util.List;

/**
 * 血糖试纸表
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-27 17:14:50
 */
public interface PaperInfoService extends IService<PaperInfo> {
	Boolean savePaper(PaperInfoDTO dto);

	/**
	 * 分页查询
	 * @param page
	 * @param dto
	 * @return
	 */
	Page<PaperInfoVO> listByPage(Page<PaperInfoVO> page, PaperInfoDTO dto);

	Boolean updatePaper(PaperInfoDTO dto);

	List<PaperInfoVO> getByDeptId(PaperInfoDTO dto);
}
