package com.code.ape.codeape.device.event.listener;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.code.ape.codeape.common.core.config.TaskExecutorConfiguration;
import com.code.ape.codeape.device.api.entity.DeviceInfo;
import com.code.ape.codeape.device.event.QcRecordEvent;
import com.code.ape.codeape.device.service.DeviceInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.time.LocalDateTime;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description 事件监听器
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class QcRecordEventListener {

	private final DeviceInfoService deviceInfoService;


	/**
	 * 事务提交成功之后执行
	 * @Async 异步执行
	 * @param event 事件传递的数据源
	 */
	@Async(TaskExecutorConfiguration.PRIMARY_POOL_NAME)
	@TransactionalEventListener(QcRecordEvent.class)
	public void updateQcRecord(QcRecordEvent event) {
		if (log.isDebugEnabled())
			log.debug("接收到更新质控时间的事件....");
		DeviceInfo deviceInfo=(DeviceInfo)event.getSource();
		deviceInfoService.update(Wrappers.<DeviceInfo>lambdaUpdate()
				.set(DeviceInfo::getLastQcTime, LocalDateTime.now())
				.eq(DeviceInfo::getId,deviceInfo.getId()));
	}
}
