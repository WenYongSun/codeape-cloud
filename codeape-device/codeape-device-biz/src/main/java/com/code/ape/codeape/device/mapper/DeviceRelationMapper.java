package com.code.ape.codeape.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.ape.codeape.device.api.entity.DeviceRelation;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
@Mapper
public interface DeviceRelationMapper extends BaseMapper<DeviceRelation> {
	List<Long> selectByDeviceId(Long id);
}
