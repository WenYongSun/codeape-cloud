
package com.code.ape.codeape.device.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.log.annotation.SysLog;
import com.code.ape.codeape.common.security.annotation.InjectAuth;
import com.code.ape.codeape.common.security.util.SecurityUtils;
import com.code.ape.codeape.device.api.dto.QcRecordDTO;
import com.code.ape.codeape.device.api.vo.QcRecordVO;
import com.code.ape.codeape.device.strategy.StrategyServiceRunner;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;


/**
 * 质控记录
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-28 12:15:42
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/qc/record" )
@Tag(name = "质控记录管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class QcRecordController {


    /**
     * 分页查询
     * @param page 分页对象
     * @param dto 质控记录
     * @return
     */
    @Operation(summary = "分页查询", description = "分页查询")
    @GetMapping("/page" )
	@InjectAuth
    @PreAuthorize("@pms.hasPermission('device_qcrecord_get')" )
    public R getQcRecordPage(Page<QcRecordVO> page, QcRecordDTO dto) {
        return R.ok(StrategyServiceRunner.getQcRecordService(dto.getClientId()).listPage(page,dto));
    }


    /**
     * 通过id查询质控记录
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id查询", description = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('device_qcrecord_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(StrategyServiceRunner.getQcRecordService(Objects.requireNonNull(SecurityUtils.getUser()).getClientId()).getById(id));
    }

    /**
     * 新增质控记录
     * @param dto 质控记录
     * @return R
     */
    @Operation(summary = "新增质控记录", description = "新增质控记录")
    @SysLog("新增质控记录" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('device_qcrecord_add')" )
    public R save(@Validated(value = {QcRecordDTO.QcRecordADD.class}) @RequestBody QcRecordDTO dto) {
    	return R.ok(StrategyServiceRunner.getQcRecordService(Objects.requireNonNull(SecurityUtils.getUser()).getClientId()).saveQcRecord(dto));
    }

	@Operation(summary = "批量新增质控记录-PDA端使用", description = "批量新增质控记录-PDA端使用")
	@SysLog("批量新增质控记录" )
	@PostMapping("/batch/save")
	@PreAuthorize("@pms.hasPermission('device_qcrecord_add')" )
	public R saveBatch(@Validated(value = {QcRecordDTO.QcRecordADD.class,QcRecordDTO.QcRecordPDAAdd.class}) @RequestBody List<QcRecordDTO> dtos) {
		return R.ok(StrategyServiceRunner.getQcRecordService(Objects.requireNonNull(SecurityUtils.getUser()).getClientId()).saveBatchQcRecord(dtos));
	}


	/**
     * 修改质控记录
     * @param dto 质控记录
     * @return R
     */
    @Operation(summary = "修改质控记录", description = "修改质控记录")
    @SysLog("修改质控记录" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('device_qcrecord_edit')" )
    public R updateById(@Validated(value = {QcRecordDTO.QcRecordUpdate.class}) @RequestBody QcRecordDTO dto) {
        return R.ok(StrategyServiceRunner.getQcRecordService(Objects.requireNonNull(SecurityUtils.getUser()).getClientId()).updateQcRecord(dto));
    }

    /**
     * 通过id删除质控记录
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id删除质控记录", description = "通过id删除质控记录")
    @SysLog("通过id删除质控记录" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('device_qcrecord_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(StrategyServiceRunner.getQcRecordService(Objects.requireNonNull(SecurityUtils.getUser()).getClientId()).deleteQcRecord(id));
    }

}
