

package com.code.ape.codeape.device.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.common.core.constant.CacheConstants;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.log.annotation.SysLog;
import com.code.ape.codeape.common.security.annotation.IgnoreAuth;
import com.code.ape.codeape.common.security.annotation.InjectAuth;
import com.code.ape.codeape.common.security.annotation.Inner;
import com.code.ape.codeape.common.security.annotation.RepeatSubmit;
import com.code.ape.codeape.common.security.util.SecurityUtils;
import com.code.ape.codeape.device.api.dto.DeviceInfoDTO;
import com.code.ape.codeape.device.api.entity.DeviceInfo;
import com.code.ape.codeape.device.api.vo.DeviceInfoVO;
import com.code.ape.codeape.device.service.DeviceInfoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * 设备信息
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-25 16:20:29
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/device" )
@Tag(name = "设备管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class DeviceInfoController {

    private final DeviceInfoService deviceInfoService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param dto 设备
     * @return
     */
    @Operation(summary = "分页查询", description = "分页查询")
    @GetMapping("/page" )
	@InjectAuth
    @PreAuthorize("@pms.hasPermission('device_deviceinfo_get')" )
    public R<Page<DeviceInfoVO>> getDeviceInfoPage(Page<DeviceInfoVO> page, DeviceInfoDTO dto) {
    	return R.ok(deviceInfoService.listPage(page,dto));
    }

	@Operation(summary = "查询当前医院所有设备SN", description = "查询当前医院所有设备SN")
	@GetMapping("/hos/sn" )
	@PreAuthorize("@pms.hasPermission('device_deviceinfo_get')" )
	@Cacheable(value = CacheConstants.DEVICE_DEVICE_SN,
			key = "T(com.code.ape.codeape.common.security.util.SecurityUtils).getUser().hosId"
			,unless = "T(cn.hutool.core.util.StrUtil).isNotBlank(#sn)")
	public R<List<DeviceInfoVO>> listDeviceSn(@Parameter(description = "设备SN") String sn) {
    	return R.ok(deviceInfoService.list(Wrappers.<DeviceInfo>lambdaQuery()
				.eq(DeviceInfo::getHosId,Objects.requireNonNull(SecurityUtils.getUser()).getHosId())
				.likeLeft(StrUtil.isNotBlank(sn),DeviceInfo::getSn,sn))
				.stream().map(entity-> DeviceInfoVO.builder()
				.id(entity.getId())
				.sn(entity.getSn())
				.build()).collect(Collectors.toList()));
	}


    /**
     * 通过id查询设备
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id查询", description = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('device_deviceinfo_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(deviceInfoService.getById(id));
    }

	@Operation(summary = "通过Sn查询", description = "通过Sn查询")
	@GetMapping("/sn/{sn}" )
	@IgnoreAuth
	@Inner
	public R<DeviceInfoVO> getBySn(@PathVariable("sn" ) String sn) {
		return R.ok(deviceInfoService.getDeviceInfoBySn(sn));
	}

    /**
     * 新增设备
     * @param dto 设备
     * @return R
     */
    @Operation(summary = "新增设备", description = "新增设备")
    @SysLog("新增设备" )
    @PostMapping
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('device_deviceinfo_add')" )
    public R save(@Validated(value = {DeviceInfoDTO.DeviceInfoAdd.class}) @RequestBody DeviceInfoDTO dto) {
		return R.ok(deviceInfoService.addDevice(dto));
    }

    /**
     * 修改设备
     * @param dto 设备
     * @return R
     */
    @Operation(summary = "修改设备", description = "修改设备")
    @SysLog("修改设备" )
    @PutMapping
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('device_deviceinfo_edit')" )
    public R updateById(@Validated(value = {DeviceInfoDTO.DeviceInfoUpdate.class}) @RequestBody DeviceInfoDTO dto) {
        return R.ok(deviceInfoService.updateDevice(dto));
    }

    /**
     * 通过id删除设备
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id删除设备", description = "通过id删除设备")
    @SysLog("通过id删除设备" )
    @DeleteMapping("/{id}" )
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('device_deviceinfo_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(deviceInfoService.removeDeviceById(id));
    }

}
