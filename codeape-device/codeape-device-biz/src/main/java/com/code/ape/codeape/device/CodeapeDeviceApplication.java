
package com.code.ape.codeape.device;

import com.code.ape.codeape.common.feign.annotation.EnableCodeapeFeignClients;
import com.code.ape.codeape.common.job.annotation.EnableCodeapeXxlJob;
import com.code.ape.codeape.common.security.annotation.EnableCodeapeResourceServer;
import com.code.ape.codeape.common.security.component.TTLSecurityContextHolderStrategy;
import com.code.ape.codeape.common.swagger.annotation.EnableCodeapeDoc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * 设备管理
 */
@EnableCodeapeDoc
@EnableCodeapeResourceServer
@EnableCodeapeFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@EnableCodeapeXxlJob    //开启xxl-job
public class CodeapeDeviceApplication {

	public static void main(String[] args) {

		SpringApplication.run(CodeapeDeviceApplication.class, args);
	}

}
