

package com.code.ape.codeape.device.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.log.annotation.SysLog;
import com.code.ape.codeape.common.security.annotation.InjectAuth;
import com.code.ape.codeape.common.security.annotation.RepeatSubmit;
import com.code.ape.codeape.device.api.dto.QcLiquidInfoDTO;
import com.code.ape.codeape.device.api.vo.QcLiquidInfoVO;
import com.code.ape.codeape.device.service.QcLiquidInfoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 质控液
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-27 20:52:30
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/liquid" )
@Tag(name = "质控液管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class QcLiquidInfoController {

    private final QcLiquidInfoService qcLiquidInfoService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param dto 质控液
     * @return
     */
    @Operation(summary = "分页查询", description = "分页查询")
    @GetMapping("/page" )
	@InjectAuth
    @PreAuthorize("@pms.hasPermission('device_qcliquidinfo_get')" )
    public R<Page<QcLiquidInfoVO>> getQcLiquidInfoPage(Page<QcLiquidInfoVO> page, QcLiquidInfoDTO dto) {
    	return R.ok(qcLiquidInfoService.listPage(page,dto));
    }

	@Operation(summary = "根据科室ID、样本类型查询质控液信息", description = "根据科室ID、样本类型查询质控液信息")
	@PostMapping("/deptId" )
	@InjectAuth
	@PreAuthorize("@pms.hasPermission('device_qcliquidinfo_get')" )
	public R<List<QcLiquidInfoVO>> getPaperInfoPage(@Validated(value = QcLiquidInfoDTO.QcLiquidInfoGetByDeptId.class) @RequestBody QcLiquidInfoDTO dto) {
		return R.ok(qcLiquidInfoService.getByDeptId(dto));
	}


    /**
     * 通过id查询质控液
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id查询", description = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('device_qcliquidinfo_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(qcLiquidInfoService.getById(id));
    }

    /**
     * 新增质控液
     * @param dto 质控液
     * @return R
     */
    @Operation(summary = "新增质控液", description = "新增质控液")
    @SysLog("新增质控液" )
    @PostMapping
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('device_qcliquidinfo_add')" )
    public R save(@Validated(value = {QcLiquidInfoDTO.QcLiquidInfoAdd.class}) @RequestBody QcLiquidInfoDTO dto) {
        return R.ok(qcLiquidInfoService.saveQcLiquidInfo(dto));
    }

    /**
     * 修改质控液
     * @param dto 质控液
     * @return R
     */
    @Operation(summary = "修改质控液", description = "修改质控液")
    @SysLog("修改质控液" )
    @PutMapping
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('device_qcliquidinfo_edit')" )
    public R updateById(@Validated(value = {QcLiquidInfoDTO.QcLiquidInfoUpdate.class}) @RequestBody QcLiquidInfoDTO dto) {
        return R.ok(qcLiquidInfoService.updateQcLiquidInfo(dto));
    }

    /**
     * 通过id删除质控液
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id删除质控液", description = "通过id删除质控液")
    @SysLog("通过id删除质控液" )
    @DeleteMapping("/{id}" )
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('device_qcliquidinfo_del')" )
    public R removeById(@Parameter(description = "ID") @PathVariable Long id) {
        return R.ok(qcLiquidInfoService.removeById(id));
    }

}
