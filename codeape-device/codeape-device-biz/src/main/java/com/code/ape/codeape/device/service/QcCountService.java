
package com.code.ape.codeape.device.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.device.api.dto.QcCountDTO;
import com.code.ape.codeape.device.api.entity.QcCount;
import com.code.ape.codeape.device.api.vo.QcCountVO;

/**
 * 质控统计
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-29 16:51:29
 */
public interface QcCountService extends IService<QcCount> {
	/**
	 * 执行统计任务
	 * @param hosId
	 */
	void executeJob(Long hosId);

	Page<QcCountVO> listPage(Page<QcCountVO> page, QcCountDTO dto);
}
