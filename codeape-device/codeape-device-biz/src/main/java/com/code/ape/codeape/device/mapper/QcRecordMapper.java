
package com.code.ape.codeape.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.device.api.dto.DeviceInfoDTO;
import com.code.ape.codeape.device.api.dto.QcRecordDTO;
import com.code.ape.codeape.device.api.entity.QcRecord;
import com.code.ape.codeape.device.api.vo.DeviceInfoVO;
import com.code.ape.codeape.device.api.vo.QcRecordVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 质控记录
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-28 12:15:42
 */
@Mapper
public interface QcRecordMapper extends BaseMapper<QcRecord> {
	Page<QcRecordVO> selectQcRecordPage(Page<QcRecordVO> page, @Param("dto") QcRecordDTO dto);
}
