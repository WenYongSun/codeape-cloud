
package com.code.ape.codeape.device.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.device.api.dto.QcRecordDTO;
import com.code.ape.codeape.device.api.entity.QcRecord;
import com.code.ape.codeape.device.api.vo.QcRecordVO;
import com.code.ape.codeape.device.strategy.StrategyService;

import java.util.List;

/**
 * 质控记录
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-28 12:15:42
 */
public interface QcRecordService extends IService<QcRecord>, StrategyService {
	Boolean saveQcRecord(QcRecordDTO dto);
	Boolean saveBatchQcRecord(List<QcRecordDTO> dtos);
	Boolean deleteQcRecord(Long id);
	Boolean updateQcRecord(QcRecordDTO dto);
	Page<QcRecordVO> listPage(Page<QcRecordVO> page, QcRecordDTO dto);
}
