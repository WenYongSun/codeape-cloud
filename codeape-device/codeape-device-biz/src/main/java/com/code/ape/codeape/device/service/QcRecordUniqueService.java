

package com.code.ape.codeape.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.device.api.entity.QcRecordUnique;

public interface QcRecordUniqueService extends IService<QcRecordUnique> {
}

