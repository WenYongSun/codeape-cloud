package com.code.ape.codeape.device.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.ape.codeape.device.api.entity.QcRecordUnique;
import com.code.ape.codeape.device.mapper.QcRecordUniqueMapper;
import com.code.ape.codeape.device.service.QcRecordUniqueService;
import org.springframework.stereotype.Service;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
@Service
public class QcRecordUniqueServiceImpl extends ServiceImpl<QcRecordUniqueMapper, QcRecordUnique> implements QcRecordUniqueService {
}
