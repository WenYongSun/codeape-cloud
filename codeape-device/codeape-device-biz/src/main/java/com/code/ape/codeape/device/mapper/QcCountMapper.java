
package com.code.ape.codeape.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.device.api.dto.QcCountDTO;
import com.code.ape.codeape.device.api.dto.QcRecordDTO;
import com.code.ape.codeape.device.api.entity.QcCount;
import com.code.ape.codeape.device.api.vo.QcCountVO;
import com.code.ape.codeape.device.api.vo.QcRecordVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 质控统计
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-29 16:51:29
 */
@Mapper
public interface QcCountMapper extends BaseMapper<QcCount> {
	List<QcCount> selectQcCount(Long hosId);

	/**
	 * 插入数据，存在则更新
	 * @param list
	 * @return
	 */
	int insertOrUpdate(@Param("list") List<QcCount> list);

	Page<QcCountVO> selectQcCountVO(Page<QcCountVO> page, @Param("dto") QcCountDTO dto);
}
