package com.code.ape.codeape.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.device.api.entity.DeviceRelation;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
public interface DeviceRelationService extends IService<DeviceRelation> {
}
