
package com.code.ape.codeape.device.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.log.annotation.SysLog;
import com.code.ape.codeape.common.security.annotation.InjectAuth;
import com.code.ape.codeape.common.security.annotation.RepeatSubmit;
import com.code.ape.codeape.device.api.dto.PaperInfoDTO;
import com.code.ape.codeape.device.api.vo.PaperInfoVO;
import com.code.ape.codeape.device.service.PaperInfoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Executor;


/**
 * 试纸
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-27 17:14:50
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/paper" )
@Tag(name = "试纸管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class PaperInfoController {

    private final PaperInfoService paperInfoService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param dto 试纸
     * @return
     */
    @Operation(summary = "分页查询", description = "分页查询")
    @GetMapping("/page" )
	@InjectAuth
    @PreAuthorize("@pms.hasPermission('device_paperinfo_get')" )
    public R<Page<PaperInfoVO>> getPaperInfoPage(Page<PaperInfoVO> page, PaperInfoDTO dto) {
        return R.ok(paperInfoService.listByPage(page, dto));
    }

	@Operation(summary = "根据科室ID、样本类型查询试纸信息", description = "根据科室ID、样本类型查询试纸信息")
	@PostMapping("/deptId" )
	@InjectAuth
	@PreAuthorize("@pms.hasPermission('device_paperinfo_get')" )
	public R<List<PaperInfoVO>> getPaperInfoPage(@Validated(value = PaperInfoDTO.PaperInfoGetById.class) @RequestBody PaperInfoDTO dto) {
		return R.ok(paperInfoService.getByDeptId(dto));
	}



    /**
     * 通过id查询试纸
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id查询", description = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('device_paperinfo_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(paperInfoService.getById(id));
    }


    /**
     * 新增试纸
     * @param dto 试纸
     * @return R
     */
    @Operation(summary = "新增试纸", description = "新增试纸")
    @SysLog("新增试纸" )
    @PostMapping
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('device_paperinfo_add')" )
    public R save(@Validated(value = {PaperInfoDTO.PaperInfoAdd.class}) @RequestBody PaperInfoDTO dto) {
        return R.ok(paperInfoService.savePaper(dto));
    }

    /**
     * 修改试纸
     * @param dto 试纸
     * @return R
     */
    @Operation(summary = "修改试纸", description = "修改试纸")
    @SysLog("修改试纸" )
	@RepeatSubmit
    @PutMapping
    @PreAuthorize("@pms.hasPermission('device_paperinfo_edit')" )
    public R updateById(@Validated(value = {PaperInfoDTO.PaperInfoUpdate.class}) @RequestBody PaperInfoDTO dto) {
        return R.ok(paperInfoService.updatePaper(dto));
    }

    /**
     * 通过id删除试纸
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id删除试纸", description = "通过id删除试纸")
    @SysLog("通过id删除试纸" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('device_paperinfo_del')" )
    public R removeById(@Parameter(description = "试纸ID") @PathVariable Long id) {
        return R.ok(paperInfoService.removeById(id));
    }

}
