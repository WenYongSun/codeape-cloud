

package com.code.ape.codeape.device.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.device.api.dto.DeviceInfoDTO;
import com.code.ape.codeape.device.api.dto.QcLiquidInfoDTO;
import com.code.ape.codeape.device.api.entity.QcLiquidInfo;
import com.code.ape.codeape.device.api.vo.DeviceInfoVO;
import com.code.ape.codeape.device.api.vo.QcLiquidInfoVO;

import java.util.List;

/**
 * 质控液
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-27 20:52:30
 */
public interface QcLiquidInfoService extends IService<QcLiquidInfo> {
	Boolean saveQcLiquidInfo(QcLiquidInfoDTO dto);

	Page<QcLiquidInfoVO> listPage(Page<QcLiquidInfoVO> page, QcLiquidInfoDTO dto);

	Boolean updateQcLiquidInfo(QcLiquidInfoDTO dto);

	Boolean removeUpdateQcLiquidInfoById(Long id);

	List<QcLiquidInfoVO> getByDeptId(QcLiquidInfoDTO dto);
}

