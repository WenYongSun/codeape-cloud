

package com.code.ape.codeape.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.device.api.dto.QcLiquidInfoDTO;
import com.code.ape.codeape.device.api.entity.QcLiquidInfo;
import com.code.ape.codeape.device.api.vo.QcLiquidInfoVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 质控液
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-27 20:52:30
 */
@Mapper
public interface QcLiquidInfoMapper extends BaseMapper<QcLiquidInfo> {

	Page<QcLiquidInfoVO> selectQcLiquidInfoPage(Page<QcLiquidInfoVO> page, @Param("dto") QcLiquidInfoDTO dto);
	QcLiquidInfoVO selectInfoVOById(Long id);

}
