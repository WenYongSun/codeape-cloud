package com.code.ape.codeape.device.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.ape.codeape.common.core.constant.enums.DelFlagEnum;
import com.code.ape.codeape.device.api.dto.QcCountDTO;
import com.code.ape.codeape.device.api.entity.QcCount;
import com.code.ape.codeape.device.api.vo.QcCountVO;
import com.code.ape.codeape.device.mapper.QcCountMapper;
import com.code.ape.codeape.device.mapper.QcRecordMapper;
import com.code.ape.codeape.device.service.QcCountService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 质控统计
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-29 16:51:29
 */
@Service
@RequiredArgsConstructor
public class QcCountServiceImpl extends ServiceImpl<QcCountMapper, QcCount> implements QcCountService {

	@Transactional
	@Override
	public void executeJob(Long hosId) {
		//step1 获取当前医院的统计
		List<QcCount> list = baseMapper.selectQcCount(hosId);
		if (CollectionUtil.isEmpty(list))
			return;
		//设置唯一Id
		list.stream().peek(data-> {
			data.setId(IdWorker.getId());
			data.setDelFlag(DelFlagEnum.NORMAL.getValue());
		}).collect(Collectors.toList());
		//step2 插入数据
		baseMapper.insertOrUpdate(list);
	}

	@Override
	public Page<QcCountVO> listPage(Page<QcCountVO> page, QcCountDTO dto) {
		return baseMapper.selectQcCountVO(page, dto);
	}
}
