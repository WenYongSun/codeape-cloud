package com.code.ape.codeape.device.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.ape.codeape.device.api.entity.DeviceRelation;
import com.code.ape.codeape.device.mapper.DeviceRelationMapper;
import com.code.ape.codeape.device.service.DeviceRelationService;
import org.springframework.stereotype.Service;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
@Service
public class DeviceRelationServiceImpl extends ServiceImpl<DeviceRelationMapper,DeviceRelation> implements DeviceRelationService {
}
