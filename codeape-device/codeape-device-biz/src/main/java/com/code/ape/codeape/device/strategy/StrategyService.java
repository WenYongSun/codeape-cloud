package com.code.ape.codeape.device.strategy;

import org.springframework.core.Ordered;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description 策略接口
 */
public interface StrategyService extends Ordered {
	/**
	 * 是否支持该策略
	 * @param clientId  客户端ID
	 * @return
	 */
	boolean support(String clientId);
}
