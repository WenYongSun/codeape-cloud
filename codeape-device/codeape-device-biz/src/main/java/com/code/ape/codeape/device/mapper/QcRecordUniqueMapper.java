
package com.code.ape.codeape.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.ape.codeape.device.api.entity.QcRecord;
import com.code.ape.codeape.device.api.entity.QcRecordUnique;
import org.apache.ibatis.annotations.Mapper;

/**
 * 质控记录防重
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-28 12:15:42
 */
@Mapper
public interface QcRecordUniqueMapper extends BaseMapper<QcRecordUnique> {

}
