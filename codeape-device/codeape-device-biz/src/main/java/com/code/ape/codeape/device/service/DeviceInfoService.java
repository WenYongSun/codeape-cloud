
package com.code.ape.codeape.device.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.device.api.dto.DeviceInfoDTO;
import com.code.ape.codeape.device.api.entity.DeviceInfo;
import com.code.ape.codeape.device.api.vo.DeviceInfoVO;

/**
 * 设备信息表
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-25 16:20:29
 */
public interface DeviceInfoService extends IService<DeviceInfo> {

	Boolean addDevice(DeviceInfoDTO dto);

	Page<DeviceInfoVO> listPage(Page<DeviceInfoVO> page, DeviceInfoDTO dto);

	Boolean updateDevice(DeviceInfoDTO dto);

	Boolean removeDeviceById(Long id);

	/**
	 * 通过设备Sn号查询设备信息
	 * @param sn 设备Sn
	 * @return 设备信息
	 */
	DeviceInfoVO getDeviceInfoBySn(String sn);
}
