
package com.code.ape.codeape.device.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.ape.codeape.admin.api.entity.SysUser;
import com.code.ape.codeape.admin.api.feign.RemoteUserService;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.core.util.RetOps;
import com.code.ape.codeape.common.security.service.CodeapeUser;
import com.code.ape.codeape.common.security.util.SecurityUtils;
import com.code.ape.codeape.device.api.dto.PaperInfoDTO;
import com.code.ape.codeape.device.api.entity.DeviceInfo;
import com.code.ape.codeape.device.api.entity.PaperInfo;
import com.code.ape.codeape.device.api.util.DeviceCheckUtil;
import com.code.ape.codeape.device.api.vo.DeviceInfoVO;
import com.code.ape.codeape.device.api.vo.PaperInfoVO;
import com.code.ape.codeape.device.mapper.PaperInfoMapper;
import com.code.ape.codeape.device.service.PaperInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 试纸
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-27 17:14:50
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class PaperInfoServiceImpl extends ServiceImpl<PaperInfoMapper, PaperInfo> implements PaperInfoService {

	private final RemoteUserService remoteUserService;

	@Transactional
    @Override
    public Boolean savePaper(PaperInfoDTO dto) {
		//step1 校验批号规则
		Assert.isTrue(DeviceCheckUtil.checkPaperBatchNum(dto.getBatchNum()),"试纸批号不符合规则！");
		//step2 入库
		PaperInfo paperInfo = new PaperInfo();
		BeanUtil.copyProperties(dto,paperInfo);
		//填充一些必要的信息
		//入库人的ID
		CodeapeUser codeapeUser = Objects.requireNonNull(SecurityUtils.getUser());
		paperInfo.setEnterId(codeapeUser.getId());
		//医院ID
		paperInfo.setHosId(codeapeUser.getHosId());
		int paperRows = baseMapper.insert(paperInfo);
		Assert.isTrue(paperRows==1,"试纸添加失败！");
		return Boolean.TRUE;
    }

	@Override
	public Page<PaperInfoVO> listByPage(Page<PaperInfoVO> page, PaperInfoDTO dto) {
    	//step1 查询，权限控制：1. 科室，2 医院
		Page<PaperInfoVO> pageVo = baseMapper.selectPaperPage(page, dto);
		//step2 获取入库人姓名
		List<PaperInfoVO> records = pageVo.getRecords();
		//入库人的全部的id
		Set<Long> enterIds = records.stream().filter(Objects::nonNull).map(PaperInfoVO::getEnterId).collect(Collectors.toSet());

		//step3 调用admin服务获取，入库人姓名不是展示必须，即使服务调用失败也不抛出异常，try起来
		if (CollectionUtil.isNotEmpty(enterIds)) {
			List<SysUser> enterList=new ArrayList<>();
			try{
				R<List<SysUser>> resultEnterUser = remoteUserService.listUserInfoByIds(enterIds);
				enterList= RetOps.of(resultEnterUser).getData().orElse(new ArrayList<>());
			}catch (Exception ex){
				log.error("调用用户服务异常，参数：{}，异常信息：{}", JSON.toJSON(enterIds),ex.getLocalizedMessage());
			}

			if (CollectionUtil.isNotEmpty(enterList)){
				Map<Long, SysUser> userMap = enterList.stream().collect(Collectors.groupingBy(SysUser::getUserId, Collectors.collectingAndThen(Collectors.toList(), list -> list.get(0))));
				records.stream().peek(entity-> entity.setEnterName(Optional.ofNullable(userMap.get(entity.getEnterId())).orElse(new SysUser()).getName())).collect(Collectors.toList());
			}
		}

		//todo 统计试纸剩余量

		return pageVo;
	}

	@Transactional
	@Override
	public Boolean updatePaper(PaperInfoDTO dto) {
		//直接更新
		PaperInfo paperInfo = new PaperInfo();
		BeanUtil.copyProperties(dto,paperInfo);
		int paperRows = baseMapper.updateById(paperInfo);
		Assert.isTrue(paperRows==1,"修改失败！");
		return Boolean.TRUE;
	}

    @Override
    public List<PaperInfoVO> getByDeptId(PaperInfoDTO dto) {
		CodeapeUser codeapeUser = Objects.requireNonNull(SecurityUtils.getUser());
		List<PaperInfo> paperInfo = baseMapper.selectList(Wrappers.<PaperInfo>lambdaQuery()
				.eq(PaperInfo::getDeptId, dto.getDeptId())
				.eq(PaperInfo::getSampleType,dto.getSampleType())
				.eq(PaperInfo::getHosId, codeapeUser.getHosId())
				//未失效
				.gt(PaperInfo::getExpiryDate, LocalDateTime.now())
				.in(CollectionUtil.isNotEmpty(dto.getDataAuth()), PaperInfo::getDeptId, dto.getDataAuth()));
		return BeanUtil.copyToList(paperInfo,PaperInfoVO.class);
	}
}
