package com.code.ape.codeape.device.strategy;

import cn.hutool.extra.spring.SpringUtil;
import com.code.ape.codeape.device.service.QcRecordService;
import org.springframework.core.Ordered;

import java.util.Comparator;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description 策略模式执行器
 */
public class StrategyServiceRunner {

	public static ConcurrentHashMap<String,QcRecordService> qcRecordService=new ConcurrentHashMap<>();

	public static QcRecordService getQcRecordService(String clientId){
		QcRecordService recordService = StrategyServiceRunner.qcRecordService.get(clientId);
		if (Objects.isNull(recordService)){
			Map<String, QcRecordService> userDetailsServiceMap = SpringUtil
					.getBeansOfType(QcRecordService.class);
			Optional<QcRecordService> optional = userDetailsServiceMap.values()
					.stream()
					.filter(service -> service.support(clientId))
					.max(Comparator.comparingInt(Ordered::getOrder));
			QcRecordService service = optional.orElseThrow(() -> new RuntimeException("当前客户端不支持！"));
			qcRecordService.put(clientId,service);
			return service;
		}else{
			return recordService;
		}
	}

}
