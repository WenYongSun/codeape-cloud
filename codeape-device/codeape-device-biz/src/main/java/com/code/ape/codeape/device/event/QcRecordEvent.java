package com.code.ape.codeape.device.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description 质控事件
 */
@Getter
public class QcRecordEvent extends ApplicationEvent {
	public QcRecordEvent(Object source) {
		super(source);
	}
}
