
package com.code.ape.codeape.device.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.security.annotation.InjectAuth;
import com.code.ape.codeape.device.api.dto.QcCountDTO;
import com.code.ape.codeape.device.api.vo.QcCountVO;
import com.code.ape.codeape.device.quartz.QcRecordQuartz;
import com.code.ape.codeape.device.service.QcCountService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 质控统计
 *
 * 只允许查询，内部通过定时任务每天生成统计，不实时：{@link QcRecordQuartz}
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-29 16:51:29
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/qc/count" )
@Tag(name = "质控统计管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class QcCountController {

    private final QcCountService qcCountService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param dto 质控统计
     * @return
     */
    @Operation(summary = "分页查询", description = "分页查询")
    @GetMapping("/page" )
	@InjectAuth
    @PreAuthorize("@pms.hasPermission('device_qccount_get')" )
    public R<Page<QcCountVO>> getQcCountPage(Page<QcCountVO> page, QcCountDTO dto) {
    	return R.ok(qcCountService.listPage(page,dto));
    }


    /**
     * 通过id查询质控统计
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id查询", description = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('device_qccount_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(qcCountService.getById(id));
    }

}
