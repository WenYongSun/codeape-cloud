

package com.code.ape.codeape.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.device.api.dto.PaperInfoDTO;
import com.code.ape.codeape.device.api.entity.PaperInfo;
import com.code.ape.codeape.device.api.vo.PaperInfoVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 血糖试纸表
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-27 17:14:50
 */
@Mapper
public interface PaperInfoMapper extends BaseMapper<PaperInfo> {

	Page<PaperInfoVO> selectPaperPage(Page<PaperInfoVO> page, @Param("dto") PaperInfoDTO dto);

	PaperInfoVO selectPaperVoById(Long id);
}
