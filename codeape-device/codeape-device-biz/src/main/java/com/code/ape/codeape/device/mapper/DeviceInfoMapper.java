
package com.code.ape.codeape.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.device.api.dto.DeviceInfoDTO;
import com.code.ape.codeape.device.api.entity.DeviceInfo;
import com.code.ape.codeape.device.api.vo.DeviceInfoVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 设备信息表
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-25 16:20:29
 */
@Mapper
public interface DeviceInfoMapper extends BaseMapper<DeviceInfo> {
	Page<DeviceInfoVO> selectDevicePage(Page<DeviceInfoVO> page, @Param("dto")DeviceInfoDTO dto);
}
