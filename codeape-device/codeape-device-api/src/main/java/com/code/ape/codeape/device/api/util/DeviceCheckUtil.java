package com.code.ape.codeape.device.api.util;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description 设备校验工具类
 */
public class DeviceCheckUtil {
	/**
	 * todo 设备SN号校验，根据自己平台设备SN号的规则校验
	 * @param sn
	 * @return
	 */
	public static boolean checkSn(String sn){
		return true;
	}

	/**
	 * todo 试纸批号校验  根据自己平台设备SN号的规则校验
	 * @param batchNum
	 * @return
	 */
	public static boolean checkPaperBatchNum(String batchNum){
		return true;
	}

	/**
	 * todo 质控液批号校验  根据自己平台设备SN号的规则校验
	 * @param batchNum
	 * @return
	 */
	public static boolean checkQcLiquid(String batchNum){
		return true;
	}
}
