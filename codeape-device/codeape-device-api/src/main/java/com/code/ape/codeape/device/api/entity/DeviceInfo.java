
package com.code.ape.codeape.device.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import com.code.ape.codeape.device.api.type.handler.SeparatorToListTypeHandler;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 设备信息表
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-25 16:20:29
 */
@Data
@TableName(value = "device_info",autoResultMap = true)
@EqualsAndHashCode(callSuper = true)
@Schema(description = "设备信息表")
public class DeviceInfo extends BaseEntity {

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="id")
    private Long id;

    /**
     * hosId
     */
    @Schema(description ="hosId")
    private Long hosId;

    /**
     * SN号，设备的唯一识别号
     */
    @Schema(description ="SN号，设备的唯一识别号")
    private String sn;

    /**
     * GSM设备Imei号
     */
    @Schema(description ="GSM设备Imei号")
    private Long imei;

    /**
     * 设备类型
     */
    @Schema(description ="设备类型")
    private String deviceType;

    /**
     * poct编号
     */
    @Schema(description ="poct编号")
    private String poctNum;

    /**
     * 设备名称
     */
    @Schema(description ="设备名称")
    private String productName;

    /**
     * app版本信息
     */
    @Schema(description ="app版本信息")
    private String appVersion;

    /**
     * 设备主板软件信息
     */
    @Schema(description ="设备主板软件信息")
    private String biosVersion;

    /**
     * 设备的IP地址
     */
    @Schema(description ="设备的IP地址")
    private String ip;

    /**
     * 入库人
     */
    @Schema(description ="入库人")
    private Long enterId;

    /**
     * 入库时间
     */
    @Schema(description ="入库时间")
    private LocalDateTime enterTime;

    /**
     * 科室/病区id
     */
    @Schema(description ="科室/病区id")
    private Long deptId;

    /**
     * 是否上传本地备份库，0 不上传 1 上传
     */
    @Schema(description ="是否上传本地备份库，0 不上传 1 上传")
    private Integer enableDb;

    /**
     * 设备状态 0正常，1报修
     */
    @Schema(description ="设备状态 0正常，1报修")
    private Integer devStatus;

    /**
     * 报修备注
     */
    @Schema(description ="报修备注")
    private String comment;

    /**
     * 设备绑定模块，0.全部，1.住院，2.门诊，3.体检
     */
    @Schema(description ="设备绑定模块，0.全部，1.住院，2.门诊，3.体检")
    private Integer bindModule;

    /**
     * 设备权限id,以逗号分割,1:血糖检测,2:血酮检测,3:尿酸检测,4:住院,5:门诊 6 体检
     */
    @Schema(description ="设备权限id,以逗号分割,1:血糖检测,2:血酮检测,3:尿酸检测,4:住院,5:门诊 6 体检")
	@TableField(typeHandler = SeparatorToListTypeHandler.class)
    private List<Long> devAuthId;

    /**
     * 检测次数
     */
    @Schema(description ="检测次数")
    private Integer checkTotal;

    /**
     * 质控次数
     */
    @Schema(description ="质控次数")
    private Integer qcTotal;

    /**
     * 最后检测时间
     */
    @Schema(description ="最后检测时间")
    private LocalDateTime lastCheckTime;

    /**
     * 最后质控时间
     */
    @Schema(description ="最后质控时间")
    private LocalDateTime lastQcTime;

    /**
     * 9801有线ip
     */
    @Schema(description ="9801有线ip")
    private String lanIp;

    /**
     * 9801有线mac
     */
    @Schema(description ="9801有线mac")
    private String lanMac;

    /**
     * 9801无线IP
     */
    @Schema(description ="9801无线IP")
    private String wifiIp;

    /**
     * 9801无线mac
     */
    @Schema(description ="9801无线mac")
    private String wifiMac;

    /**
     * 删除标记  -1：已删除  0：正常
     */
    @TableLogic
    @Schema(description ="删除标记  -1：已删除  0：正常")
    private String delFlag;


}
