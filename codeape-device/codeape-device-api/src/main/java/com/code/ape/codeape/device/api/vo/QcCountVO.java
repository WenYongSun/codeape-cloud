package com.code.ape.codeape.device.api.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 质控统计
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-29 16:49:49
 */
@Data
@Schema(description = "质控统计")
public class QcCountVO  {

    /**
     * id
     */
    @Schema(description ="id")
    private Long id;


    /**
     * 科室Id
     */
    @Schema(description ="科室Id")
    private Long deptId;

    /**
     * 质控总数
     */
    @Schema(description ="质控总数")
    private Long qcTotal;

    /**
     * 不通过次数
     */
    @Schema(description ="不通过次数")
    private Long noTotal;

    /**
     * 通过次数
     */
    @Schema(description ="通过次数")
    private Long yesTotal;

	/**
	 * 样本类型 1血糖,2血酮,3尿酸
	 */
	@Schema(description ="样本类型 1血糖,2血酮,3尿酸")
	private Integer sampleType;

    /**
     * 最后质控时间
     */
    @Schema(description ="最后质控时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime lastQcTime;

    @Schema(description = "成功率")
    private String yesRate;

}
