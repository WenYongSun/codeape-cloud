package com.code.ape.codeape.device.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 质控记录
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-28 12:15:42
 */
@Data
@TableName("qc_record")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "质控记录")
public class QcRecord extends BaseEntity {

    /**
     * id 这里的ID不自增，使用防重表的ID
     */
    @TableId(type = IdType.NONE)
    @Schema(description ="id")
    private Long id;

    /**
     * hosId
     */
    @Schema(description ="hosId")
    private Long hosId;

    /**
     * 质控值
     */
    @Schema(description ="质控值")
    private Float value;

    /**
     * 样本类型 1血糖,2血酮,3尿酸
     */
    @Schema(description ="样本类型 1血糖,2血酮,3尿酸")
    private Integer sampleType;

    /**
     * sn号
     */
    @Schema(description ="sn号")
    private String sn;

    /**
     * 质控结果 1：合格，0：不合格
     */
    @Schema(description ="质控结果 1：合格，0：不合格")
    private Integer result;

    /**
     * 质控时间
     */
    @Schema(description ="质控时间")
    private LocalDateTime measureTime;

    /**
     * 质控操作员user_id
     */
    @Schema(description ="质控操作员user_id")
    private Long operatorUserId;

    /**
     * 试纸主键id
     */
    @Schema(description ="试纸主键id")
    private Long paperId;

    /**
     * 质控液主键id
     */
    @Schema(description ="质控液主键id")
    private Long liquidId;

    /**
     * 科室id
     */
    @Schema(description ="科室id")
    private Long deptId;

    /**
     * 删除标记  -1：已删除  0：正常
     */
    @TableLogic
    @Schema(description ="删除标记  -1：已删除  0：正常")
    private String delFlag;


}
