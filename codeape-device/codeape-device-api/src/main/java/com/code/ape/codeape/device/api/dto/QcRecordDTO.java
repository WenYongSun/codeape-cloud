package com.code.ape.codeape.device.api.dto;

import com.code.ape.codeape.common.core.entity.BaseParam;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 质控记录
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-28 12:15:42
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "质控记录")
public class QcRecordDTO extends BaseParam {

	@NotNull(message = "ID不能为空",groups = {QcRecordUpdate.class,QcRecordDelete.class})
    @Schema(description ="id")
    private Long id;

    /**
     * 质控值
     */
    @NotNull(message = "质控值不能为空",groups = {QcRecordADD.class,QcRecordUpdate.class})
    @Schema(description ="质控值")
    private Float value;

    /**
     * 样本类型 1血糖,2血酮,3尿酸
     */
    @Schema(description ="样本类型 1血糖,2血酮,3尿酸")
	@NotNull(message = "样本类型不能为空",groups = {QcRecordADD.class,QcRecordUpdate.class})
    private Integer sampleType;

    /**
     * sn号
     */
    @Schema(description ="sn号")
	@NotBlank(message = "设备SN号不能为空",groups = {QcRecordADD.class,QcRecordUpdate.class})
    private String sn;

    /**
     * 质控结果 1：合格，0：不合格
     */
    @Schema(description ="质控结果 1：合格，0：不合格")
	@NotNull(message = "质控结果不能为空",groups = {QcRecordPDAAdd.class})
    private Integer result;

    /**
     * 质控时间
     */
    @Schema(description ="质控时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message = "质控值不能为空",groups = {QcRecordADD.class,QcRecordUpdate.class})
    private Date measureTime;


    /**
     * 试纸主键id
     */
    @Schema(description ="试纸主键id")
	@NotNull(message = "质控值不能为空",groups = {QcRecordADD.class,QcRecordUpdate.class})
    private Long paperId;

    /**
     * 质控液主键id
     */
    @Schema(description ="质控液主键id")
	@NotNull(message = "质控值不能为空",groups = {QcRecordADD.class,QcRecordUpdate.class})
    private Long liquidId;

    /**
     * 科室id
     */
    @Schema(description ="科室id")
	@NotNull(message = "质控值不能为空",groups = {QcRecordADD.class,QcRecordUpdate.class})
    private Long deptId;

    public interface QcRecordADD{}

	public interface QcRecordUpdate{}

	public interface QcRecordDelete{}

	/**
	 * todo 用于PDA的参数校验，因为PDA存在离线+在线的逻辑
	 */
	public interface QcRecordPDAAdd{}


}
