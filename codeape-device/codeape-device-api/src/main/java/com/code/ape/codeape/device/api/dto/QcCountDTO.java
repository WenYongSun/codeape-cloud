package com.code.ape.codeape.device.api.dto;

import com.code.ape.codeape.common.core.entity.BaseParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 质控统计
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-29 16:49:49
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "质控统计")
public class QcCountDTO extends BaseParam {

    /**
     * id
     */
    @Schema(description ="id")
    private Long id;

    /**
     * 科室Id
     */
    @Schema(description ="科室Id")
    private Long deptId;

}
