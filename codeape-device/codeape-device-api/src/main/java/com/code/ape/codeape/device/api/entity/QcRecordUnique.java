package com.code.ape.codeape.device.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 质控记录
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-28 12:15:42
 */
@Data
@TableName("qc_record_unique")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "质控记录防重表")
public class QcRecordUnique extends BaseEntity {

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="id")
    private Long id;

    /**
     * 质控值
     */
    @Schema(description ="质控值")
    private Float value;


    /**
     * sn号
     */
    @Schema(description ="sn号")
    private String sn;


    /**
     * 质控时间
     */
    @Schema(description ="质控时间")
    private LocalDateTime measureTime;

    /**
     * 删除标记  -1：已删除  0：正常  不走逻辑删除，直接删除 去掉@TableLogic注解
     */
    @Schema(description ="删除标记  -1：已删除  0：正常")
    private String delFlag;
}
