package com.code.ape.codeape.device.api.vo;

import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 质控记录
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-28 12:15:42
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "质控记录")
public class QcRecordVO extends BaseEntity {

    /**
     * id 这里的ID不自增，使用防重表的ID
     */
    @Schema(description ="id")
    private Long id;

    /**
     * 质控值
     */
    @Schema(description ="质控值")
    private Float value;

    /**
     * 样本类型 1血糖,2血酮,3尿酸
     */
    @Schema(description ="样本类型 1血糖,2血酮,3尿酸")
    private Integer sampleType;

    /**
     * sn号
     */
    @Schema(description ="sn号")
    private String sn;

    /**
     * 质控结果 1：合格，0：不合格
     */
    @Schema(description ="质控结果 1：合格，0：不合格")
    private Integer result;

    /**
     * 质控时间
     */
    @Schema(description ="质控时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime measureTime;

    /**
     * 质控操作员user_id
     */
    @Schema(description ="质控操作员user_id")
    private Long operatorUserId;

	@Schema(description ="质控操作员姓名")
    private String operatorName;

    /**
     * 试纸主键id
     */
    @Schema(description ="试纸主键id")
    private Long paperId;

    /**
     * 质控液主键id
     */
    @Schema(description ="质控液主键id")
    private Long liquidId;

    /**
     * 科室id
     */
    @Schema(description ="科室id")
    private Long deptId;

    @Schema(description = "试纸相关信息")
    private PaperInfoVO paperInfoVO;

    @Schema(description = "质控液相关信息")
    private QcLiquidInfoVO qcLiquidInfoVO;
}
