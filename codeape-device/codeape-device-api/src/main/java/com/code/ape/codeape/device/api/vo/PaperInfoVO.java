package com.code.ape.codeape.device.api.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PaperInfoVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@Schema(description ="id")
	private Long id;

	/**
	 * 试纸批号
	 */
	@Schema(description ="试纸批号")
	private String batchNum;

	/**
	 * 规格 0:50片;1:100片;2:10片;3:25片;4:200片
	 */
	@Schema(description ="规格 0:50片;1:100片;2:10片;3:25片;4:200片")
	private Integer specs;

	/**
	 * 数量
	 */
	@Schema(description ="数量")
	private Long paperNum;

	/**
	 * 科室ID
	 */
	@Schema(description ="科室ID")
	private Long deptId;

	/**
	 * 生产日期 ，简写PD
	 */
	@Schema(description ="生产日期")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	private LocalDateTime productionDate;

	/**
	 * 到期时间,简写exd
	 */
	@Schema(description ="过期时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	private LocalDateTime expiryDate;

	/**
	 * 样本类型 1血糖,2血酮,3尿酸
	 */
	@Schema(description ="样本类型 1血糖,2血酮,3尿酸")
	private Integer sampleType;

	/**
	 * 低浓度上限
	 */
	@Schema(description ="低浓度上限")
	private Float lowMaxLimit;

	/**
	 * 低浓度下限
	 */
	@Schema(description ="低浓度下限")
	private Float lowMinLimit;

	/**
	 * 中浓度上限
	 */
	@Schema(description ="中浓度上限")
	private Float mediumMaxLimit;

	/**
	 * 中浓度下限
	 */
	@Schema(description ="中浓度下限")
	private Float mediumMinLimit;

	/**
	 * 高浓度上限
	 */
	@Schema(description ="高浓度上限")
	private Float highMaxLimit;

	/**
	 * 高浓度下限
	 */
	@Schema(description ="高浓度下限")
	private Float highMinLimit;

	@Schema(description = "入库人ID")
	private Long enterId;

	/**
	 * 入库人
	 */
	@Schema(description ="入库人")
	private String enterName;
}
