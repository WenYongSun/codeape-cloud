package com.code.ape.codeape.device.api.dto;

import com.code.ape.codeape.common.core.entity.BaseParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DeviceInfoDTO extends BaseParam {

	@NotNull(message = "ID不能为空",groups = {DeviceInfoDelete.class,DeviceInfoUpdate.class})
	private Long id;

	@NotBlank(message = "SN号不能为空",groups = {DeviceInfoAdd.class,DeviceInfoUpdate.class})
	@Length(min = 11,max = 11,message = "请输入符合正确的SN号",groups = {DeviceInfoAdd.class,DeviceInfoUpdate.class})
	private String sn;


	/**
	 * poct编号
	 */
	private String poctNum;

	/**
	 * 科室/病区id
	 */
	@NotNull(message = "科室ID不能为空",groups = {DeviceInfoAdd.class,DeviceInfoUpdate.class})
	private Long deptId;

	/**
	 * 是否上传本地备份库，0 不上传 1 上传
	 */
	@NotNull(message = "上传备份库不能为空",groups = {DeviceInfoAdd.class,DeviceInfoUpdate.class})
	private Integer enableDb;

	/**
	 * 设备状态 0正常，1报修
	 */
	@NotNull(message = "状态不能为空",groups = {DeviceInfoAdd.class,DeviceInfoUpdate.class})
	private Integer devStatus;

	/**
	 * 设备权限id,以逗号分割,1:血糖检测,2:血酮检测,3:尿酸检测,4:住院,5:门诊 6 体检
	 */
	@NotNull(message = "设备使用不能为空",groups = {DeviceInfoAdd.class,DeviceInfoUpdate.class})
	private List<Long> devAuthId;

	/**
	 * 关联科室，涉及到设备的权限
	 */
	private List<Long> deptAuths;

	public interface DeviceInfoAdd{}

	public interface DeviceInfoDelete{}

	public interface DeviceInfoUpdate{}
}
