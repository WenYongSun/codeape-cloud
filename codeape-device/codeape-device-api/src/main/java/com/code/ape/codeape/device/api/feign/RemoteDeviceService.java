package com.code.ape.codeape.device.api.feign;

import com.code.ape.codeape.common.core.constant.SecurityConstants;
import com.code.ape.codeape.common.core.constant.ServiceNameConstants;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.device.api.vo.DeviceInfoVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
@FeignClient(contextId = "remoteDeviceService", value = ServiceNameConstants.DEVICE_SERVICE)
public interface RemoteDeviceService {

	/**
	 * 通过Sn查询
	 * @param sn 设备SN号
	 * @return 设备详细信息
	 */
	@GetMapping(value = "/device/sn/{sn}",headers = SecurityConstants.HEADER_FROM_IN)
	R<DeviceInfoVO> getBySn(@PathVariable("sn" ) String sn);
}
