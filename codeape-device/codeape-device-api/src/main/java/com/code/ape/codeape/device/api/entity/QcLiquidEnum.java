package com.code.ape.codeape.device.api.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description 质控液浓度枚举类
 */
@Getter
@RequiredArgsConstructor
public enum QcLiquidEnum {

	LOW_TYPE(1,"低浓度"),
	MIDDLE_TYPE(2,"中浓度"),
	HIGH_TYPE(3,"高浓度");

	private final int value;

	private final String type;

}
