package com.code.ape.codeape.device.api.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 质控液
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-27 20:52:30
 */
@Data
@Schema(description = "质控液")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class QcLiquidInfoVO implements Serializable {

    /**
     * id
     */
    @Schema(description ="id")
    private Long id;


    /**
     * 试纸科室
     */
    @Schema(description ="试纸科室")
    private Long deptId;

    /**
     * 样本类型 1血糖,2血酮,3尿酸
     */
    @Schema(description ="样本类型 1血糖,2血酮,3尿酸")
    private Integer sampleType;

    /**
     * 质控液批号
     */
    @Schema(description ="质控液批号")
    private String batchNum;

    /**
     * 数量
     */
    @Schema(description ="数量")
    private Integer liquidNum;

    /**
     * 规格 1:2ml  2:5ml 3:10ml 4:15ml 5:20ml  6:25ml
     */
    @Schema(description ="规格 1:2ml  2:5ml 3:10ml 4:15ml 5:20ml  6:25ml")
    private Integer specs;

    /**
     * 浓度分类,0低浓度,1中浓度,2高浓度
     */
    @Schema(description ="浓度分类,1低浓度,2中浓度,3高浓度")
    private Integer type;

    /**
     * 生产日期
     */
    @Schema(description ="生产日期")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private LocalDateTime productionDate;

    /**
     * 到期时间
     */
    @Schema(description ="到期时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private LocalDateTime expiryDate;

    @Schema(description = "入库人Id")
    private Long enterId;


    @Schema(description = "入库人姓名")
    private String enterName;
}
