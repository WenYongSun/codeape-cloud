package com.code.ape.codeape.device.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.code.ape.codeape.common.core.entity.BaseParam;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
@Schema(description = "质控液")
@Data
@EqualsAndHashCode(callSuper = true)
public class QcLiquidInfoDTO extends BaseParam {

	/**
	 * id
	 */
	@Schema(description ="id")
	@NotNull(message = "ID不能为空",groups = {QcLiquidInfoUpdate.class,QcLiquidInfoDelete.class})
	private Long id;

	/**
	 * 试纸科室
	 */
	@Schema(description ="科室")
	@NotNull(message = "科室不能为空",groups = {QcLiquidInfoAdd.class,QcLiquidInfoGetByDeptId.class})
	private Long deptId;

	/**
	 * 样本类型 1血糖,2血酮,3尿酸
	 */
	@Schema(description ="样本类型 1血糖,2血酮,3尿酸")
	@NotNull(message = "类型不能为空",groups = {QcLiquidInfoAdd.class,QcLiquidInfoGetByDeptId.class})
	private Integer sampleType;

	/**
	 * 质控液批号
	 */
	@Schema(description ="质控液批号")
	@NotBlank(message = "质控液批号批号不能为空",groups = {QcLiquidInfoAdd.class})
	private String batchNum;

	/**
	 * 数量
	 */
	@Schema(description ="数量")
	@NotNull(message = "数量不能为空",groups = {QcLiquidInfoAdd.class})
	private Integer liquidNum;

	/**
	 * 规格 规格 1:2ml  2:5ml 3:10ml 4:15ml 5:20ml  6:25ml
	 */
	@Schema(description ="规格 1:2ml  2:5ml 3:10ml 4:15ml 5:20ml  6:25ml")
	@NotNull(message = "规格不能为空",groups = {QcLiquidInfoAdd.class})
	@Range(min = 1,max = 6,message = "规格不符合规则",groups = {QcLiquidInfoAdd.class})
	private Integer specs;

	/**
	 * 浓度分类,1低浓度,2中浓度,3高浓度
	 */
	@Schema(description ="浓度分类,1低浓度,2中浓度,3高浓度")
	@NotNull(message = "浓度分类不能为空",groups = {QcLiquidInfoAdd.class})
	private Integer type;

	/**
	 * 生产日期
	 */
	@Schema(description ="生产日期")
	@NotNull(message = "生产日期不能为空",groups = {QcLiquidInfoAdd.class})
	@Past(message = "生产日期不能是未来时间",groups = {QcLiquidInfoAdd.class})
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	private Date productionDate;

	/**
	 * 到期时间
	 */
	@Schema(description ="过期时间")
	@NotNull(message = "过期时间不能为空",groups = {QcLiquidInfoAdd.class})
//	@Future(message = "过期时间不能为过去时间",groups = {QcLiquidInfoAdd.class})
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	private Date expiryDate;

	public interface QcLiquidInfoAdd{}

	public interface QcLiquidInfoDelete{}

	public interface QcLiquidInfoUpdate{}

	public interface QcLiquidInfoGetByDeptId{}
}
