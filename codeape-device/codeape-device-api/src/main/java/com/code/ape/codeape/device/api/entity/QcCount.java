package com.code.ape.codeape.device.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 质控统计
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-29 16:49:49
 */
@Data
@TableName("qc_count")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "质控统计")
public class QcCount extends BaseEntity {

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="id")
    private Long id;

    /**
     * 医院Id
     */
    @Schema(description ="医院Id")
    private Long hosId;

    /**
     * 科室Id
     */
    @Schema(description ="科室Id")
    private Long deptId;

    /**
     * 质控总数
     */
    @Schema(description ="质控总数")
    private Long qcTotal;

    /**
     * 不通过次数
     */
    @Schema(description ="不通过次数")
    private Long noTotal;

    /**
     * 通过次数
     */
    @Schema(description ="通过次数")
    private Long yesTotal;

    /**
     * 最后质控时间
     */
    @Schema(description ="最后质控时间")
    private LocalDateTime lastQcTime;

    @Schema(description = "成功率")
    private Float yesRate;

	/**
	 * 样本类型 1血糖,2血酮,3尿酸
	 */
	@Schema(description ="样本类型 1血糖,2血酮,3尿酸")
	private Integer sampleType;

    /**
     * 删除标记  -1：已删除  0：正常
     */
    @TableLogic
    @Schema(description ="删除标记  -1：已删除  0：正常")
    private String delFlag;


}
