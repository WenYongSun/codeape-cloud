package com.code.ape.codeape.device.api.dto;

import com.code.ape.codeape.common.core.entity.BaseParam;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Date;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
@Schema(description = "试纸")
@Data
@EqualsAndHashCode(callSuper = true)
public class PaperInfoDTO extends BaseParam {

	/**
	 * id
	 */
	@NotNull(message = "ID不能为空",groups = {PaperInfoDelete.class,PaperInfoUpdate.class})
	@Schema(description ="id")
	private Long id;

	/**
	 * 试纸批号
	 */
	@Schema(description ="试纸批号")
	@NotBlank(message = "试纸批号不能为空",groups = {PaperInfoAdd.class})
	private String batchNum;

	/**
	 * 规格 0:50片;1:100片;2:10片;3:25片;4:200片
	 */
	@Schema(description ="规格 0:50片;1:100片;2:10片;3:25片;4:200片")
	@NotNull(message = "试纸规格不能为空",groups = {PaperInfoAdd.class})
	private Integer specs;

	/**
	 * 数量
	 */
	@Schema(description ="数量")
	@NotNull(message = "试纸数量不能为空",groups = {PaperInfoAdd.class})
	private Long paperNum;

	/**
	 * 科室ID
	 */
	@Schema(description ="科室ID")
	@NotNull(message = "科室不能为空",groups = {PaperInfoAdd.class,PaperInfoGetById.class})
	private Long deptId;

	/**
	 * 生产日期 ，简写PD
	 */
	@Schema(description ="生产日期")
	@NotNull(message = "生产日期不能为空",groups = {PaperInfoAdd.class})
	@Past(message = "生产日期不能为未来时间",groups = {PaperInfoAdd.class})
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	private Date productionDate;

	/**
	 * 到期时间,简写exd
	 */
	@Schema(description ="到期时间")
	@NotNull(message = "过期日期不能为空",groups = {PaperInfoAdd.class})
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	private Date expiryDate;

	/**
	 * 样本类型 1血糖,2血酮,3尿酸
	 */
	@Schema(description ="样本类型 1血糖,2血酮,3尿酸")
	@NotNull(message = "样本类型不能为空",groups = {PaperInfoAdd.class,PaperInfoGetById.class})
	private Integer sampleType;

	/**
	 * 低浓度上限
	 */
	@Schema(description ="低浓度上限")
	@NotNull(message = "低浓度上限不能为空",groups = {PaperInfoAdd.class})
	private Float lowMaxLimit;

	/**
	 * 低浓度下限
	 */
	@Schema(description ="低浓度下限")
	@NotNull(message = "低浓度上限不能为空",groups = {PaperInfoAdd.class})
	private Float lowMinLimit;

	/**
	 * 中浓度上限
	 */
	@Schema(description ="中浓度上限")
	@NotNull(message = "中浓度上限不能为空",groups = {PaperInfoAdd.class})
	private Float mediumMaxLimit;

	/**
	 * 中浓度下限
	 */
	@Schema(description ="中浓度下限")
	@NotNull(message = "中浓度下限不能为空",groups = {PaperInfoAdd.class})
	private Float mediumMinLimit;

	/**
	 * 高浓度上限
	 */
	@Schema(description ="高浓度上限")
	@NotNull(message = "高浓度上限不能为空",groups = {PaperInfoAdd.class})
	private Float highMaxLimit;

	/**
	 * 高浓度下限
	 */
	@Schema(description ="高浓度下限")
	@NotNull(message = "高浓度下限不能为空",groups = {PaperInfoAdd.class})
	private Float highMinLimit;

	public interface  PaperInfoAdd{}
	public interface  PaperInfoDelete{}
	public interface  PaperInfoUpdate{}
	public interface  PaperInfoGetById{}
}
