package com.code.ape.codeape.device.api.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
@Data
@Schema(description = "设备信息")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DeviceInfoVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@Schema(description ="id")
	private Long id;

	/**
	 * SN号，设备的唯一识别号
	 */
	@Schema(description ="SN号，设备的唯一识别号")
	private String sn;

	@Schema(description = "设备所在医院的ID")
	private Long hosId;


	/**
	 * poct编号
	 */
	@Schema(description ="poct编号")
	private String poctNum;

	/**
	 * 设备名称
	 */
	@Schema(description ="设备名称")
	private String productName;


	/**
	 * 入库人
	 */
	@Schema(description ="入库人")
	private Long enterId;

	/**
	 * 入库人姓名
	 */
	@Schema(description ="入库人姓名")
	private String enterName;

	/**
	 * 入库时间
	 */
	@Schema(description ="入库时间")
	private LocalDateTime enterTime;

	/**
	 * 科室/病区id
	 */
	@Schema(description ="科室")
	private Long deptId;

	/**
	 * 是否上传本地备份库，0 不上传 1 上传
	 */
	@Schema(description ="是否上传本地备份库，0 不上传 1 上传")
	private Integer enableDb;

	/**
	 * 设备状态 0正常，1报修
	 */
	@Schema(description ="设备状态 0正常，1报修")
	private Integer devStatus;


	/**
	 * 设备权限id
	 */
	@Schema(description ="设备权限id")
	private List<Long> devAuthId;

	/**
	 * 检测次数
	 */
	@Schema(description ="检测次数")
	private Long checkTotal;

	/**
	 * 质控次数
	 */
	@Schema(description ="质控次数")
	private Long qcTotal;

	/**
	 * 最后检测时间
	 */
	@Schema(description ="最后检测时间")
	private LocalDateTime lastCheckTime;

	/**
	 * 最后质控时间
	 */
	@Schema(description ="最后质控时间")
	private LocalDateTime lastQcTime;

	/**
	 * 关联科室，涉及到设备的权限
	 */
	private List<Long> deptAuths;
}
