package com.code.ape.codeape.device.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 质控液
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-27 20:52:30
 */
@Data
@TableName("qc_liquid_info")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "质控液")
public class QcLiquidInfo extends BaseEntity {

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="id")
    private Long id;

    /**
     * 医院id
     */
    @Schema(description ="医院id")
    private Long hosId;

    /**
     * 试纸科室
     */
    @Schema(description ="试纸科室")
    private Long deptId;

    /**
     * 样本类型 1血糖,2血酮,3尿酸
     */
    @Schema(description ="样本类型 1血糖,2血酮,3尿酸")
    private Integer sampleType;

    /**
     * 质控液批号
     */
    @Schema(description ="质控液批号")
    private String batchNum;

    /**
     * 数量
     */
    @Schema(description ="数量")
    private Integer liquidNum;

    /**
     * 规格 0:2ml  1:5ml 2:10ml 3:15ml 4:20ml  5:25ml
     */
    @Schema(description ="规格 0:2ml  1:5ml 2:10ml 3:15ml 4:20ml  5:25ml")
    private Integer specs;

    /**
     * 浓度分类,0低浓度,1中浓度,2高浓度
     */
    @Schema(description ="浓度分类,0低浓度,1中浓度,2高浓度")
    private Integer type;

    /**
     * 生产日期
     */
    @Schema(description ="生产日期")
    private LocalDateTime productionDate;

    /**
     * 到期时间
     */
    @Schema(description ="到期时间")
    private LocalDateTime expiryDate;

    /**
     * 入库人，填写user_id
     */
    @Schema(description ="入库人，填写user_id")
    private Long enterId;

    /**
     * 删除标记  -1：已删除  0：正常
     */
    @Schema(description ="删除标记  -1：已删除  0：正常")
	@TableLogic
    private String delFlag;


}
