package com.code.ape.codeape.device.api.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DeviceRelation extends Model<DeviceRelation> {
	private static final long serialVersionUID = 1L;

	/**
	 * 设备ID
	 */
	@Schema(description = "设备ID")
	private Long deviceId;

	/**
	 * 科室ID
	 */
	@Schema(description = "科室ID")
	private Long deptId;
}
