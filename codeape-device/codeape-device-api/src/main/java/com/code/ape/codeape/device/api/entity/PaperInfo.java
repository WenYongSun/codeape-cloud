package com.code.ape.codeape.device.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 试纸表
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-27 17:13:30
 */
@Data
@TableName("paper_info")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "试纸")
public class PaperInfo extends BaseEntity {

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="id")
    private Long id;

    /**
     * 医院id
     */
    @Schema(description ="医院id")
    private Long hosId;

    /**
     * 试纸批号
     */
    @Schema(description ="试纸批号")
    private String batchNum;

    /**
     * 规格 0:50片;1:100片;2:10片;3:25片;4:200片
     */
    @Schema(description ="规格 0:50片;1:100片;2:10片;3:25片;4:200片")
    private Integer specs;

    /**
     * 数量
     */
    @Schema(description ="数量")
    private Long paperNum;

    /**
     * 科室ID
     */
    @Schema(description ="科室ID")
    private Long deptId;

    /**
     * 生产日期 ，简写PD
     */
    @Schema(description ="生产日期 ，简写PD")
    private LocalDateTime productionDate;

    /**
     * 到期时间,简写exd
     */
    @Schema(description ="到期时间,简写exd")
    private LocalDateTime expiryDate;

    /**
     * 样本类型 1血糖,2血酮,3尿酸
     */
    @Schema(description ="样本类型 1血糖,2血酮,3尿酸")
    private Integer sampleType;

    /**
     * 低浓度上限
     */
    @Schema(description ="低浓度上限")
    private Float lowMaxLimit;

    /**
     * 低浓度下限
     */
    @Schema(description ="低浓度下限")
    private Float lowMinLimit;

    /**
     * 中浓度上限
     */
    @Schema(description ="中浓度上限")
    private Float mediumMaxLimit;

    /**
     * 中浓度下限
     */
    @Schema(description ="中浓度下限")
    private Float mediumMinLimit;

    /**
     * 高浓度上限
     */
    @Schema(description ="高浓度上限")
    private Float highMaxLimit;

    /**
     * 高浓度下限
     */
    @Schema(description ="高浓度下限")
    private Float highMinLimit;

    /**
     * 入库人
     */
    @Schema(description ="入库人")
    private Long enterId;

    /**
     * 0-正常，1-删除
     */
    @TableLogic
    @Schema(description ="0-正常，1-删除")
    private String delFlag;
}
