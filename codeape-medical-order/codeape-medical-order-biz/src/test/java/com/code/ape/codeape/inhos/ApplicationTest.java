package com.code.ape.codeape.inhos;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CodeapeMedicalOrderApplication.class)
public class ApplicationTest {

	@Test
	public void test1() throws Exception {
	}
}
