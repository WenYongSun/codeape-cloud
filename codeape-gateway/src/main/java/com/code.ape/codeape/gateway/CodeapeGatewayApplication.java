

package com.code.ape.codeape.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2018年06月21日
 * <p>
 * 网关应用
 */
@EnableDiscoveryClient
@SpringBootApplication
public class CodeapeGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeapeGatewayApplication.class, args);
	}

}
