/*
 Navicat Premium Data Transfer

 Source Server         : codeape-mysql
 Source Server Type    : MySQL
 Source Server Version : 50742
 Source Host           : codeape-mysql:3306
 Source Schema         : codeape

 Target Server Type    : MySQL
 Target Server Version : 50742
 File Encoding         : 65001

 Date: 11/06/2023 21:12:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_warn
-- ----------------------------
DROP TABLE IF EXISTS `sys_warn`;
CREATE TABLE `sys_warn`  (
  `id` bigint(20) NOT NULL,
  `hos_id` bigint(20) NOT NULL COMMENT '医院Id',
  `in_hos` int(1) NOT NULL COMMENT '0 院内,1 院外, 2 门诊 ,3 体检',
  `value_up` float(5, 2) NOT NULL COMMENT '预警高值',
  `value_down` float(5, 2) NOT NULL COMMENT '预警低值',
  `model` int(11) NOT NULL COMMENT '1 血糖 2 尿酸 3 血酮',
  `gender` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '性别，来源字典 用于尿酸',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '0-正常，1-删除',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '检测预警' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_warn
-- ----------------------------
INSERT INTO `sys_warn` VALUES (1667859170378280961, 1659018792143663105, 0, 22.10, 3.90, 1, '', '0', 'admin', 'admin', '2023-06-11 19:39:48', '2023-06-11 19:39:48');
INSERT INTO `sys_warn` VALUES (1667872687105970178, 1659018792143663105, 1, 22.10, 3.90, 1, '', '0', 'admin', 'admin', '2023-06-11 20:33:31', '2023-06-11 20:33:31');
INSERT INTO `sys_warn` VALUES (1667872754751705090, 1659018792143663105, 2, 22.10, 3.90, 1, '', '0', 'admin', 'admin', '2023-06-11 20:33:47', '2023-06-11 20:33:47');
INSERT INTO `sys_warn` VALUES (1667874198401781761, 1659018792143663105, 3, 22.10, 3.90, 1, '', '0', 'admin', 'admin', '2023-06-11 20:39:31', '2023-06-11 20:39:31');
INSERT INTO `sys_warn` VALUES (1667874409702428673, 1659018792143663105, 0, 428.00, 208.00, 2, '1', '0', 'admin', 'admin', '2023-06-11 20:40:22', '2023-06-11 20:40:22');
INSERT INTO `sys_warn` VALUES (1667874515390500865, 1659018792143663105, 0, 357.00, 155.00, 2, '2', '0', 'admin', 'admin', '2023-06-11 20:40:47', '2023-06-11 20:40:47');
INSERT INTO `sys_warn` VALUES (1667874669967380481, 1659018792143663105, 1, 428.00, 208.00, 2, '1', '0', 'admin', 'admin', '2023-06-11 20:41:24', '2023-06-11 20:41:24');
INSERT INTO `sys_warn` VALUES (1667874711847505921, 1659018792143663105, 1, 357.00, 155.00, 2, '2', '0', 'admin', 'admin', '2023-06-11 20:41:34', '2023-06-11 20:41:34');
INSERT INTO `sys_warn` VALUES (1667875048851443713, 1659018792143663105, 2, 428.00, 208.00, 2, '1', '0', 'admin', 'admin', '2023-06-11 20:42:54', '2023-06-11 20:42:54');
INSERT INTO `sys_warn` VALUES (1667875118254592001, 1659018792143663105, 2, 357.00, 155.00, 2, '2', '0', 'admin', 'admin', '2023-06-11 20:43:10', '2023-06-11 20:43:10');
INSERT INTO `sys_warn` VALUES (1667875226039816194, 1659018792143663105, 3, 428.00, 208.00, 2, '1', '0', 'admin', 'admin', '2023-06-11 20:43:36', '2023-06-11 20:43:36');
INSERT INTO `sys_warn` VALUES (1667875279601078274, 1659018792143663105, 3, 357.00, 155.00, 2, '2', '0', 'admin', 'admin', '2023-06-11 20:43:49', '2023-06-11 20:43:49');
INSERT INTO `sys_warn` VALUES (1667875489152700417, 1659018792143663105, 0, 0.40, 0.30, 3, '', '0', 'admin', 'admin', '2023-06-11 20:44:39', '2023-06-11 20:44:39');
INSERT INTO `sys_warn` VALUES (1667875531594862594, 1659018792143663105, 1, 0.40, 0.30, 3, '', '0', 'admin', 'admin', '2023-06-11 20:44:49', '2023-06-11 20:44:49');
INSERT INTO `sys_warn` VALUES (1667875596942118914, 1659018792143663105, 2, 0.40, 0.30, 3, '', '0', 'admin', 'admin', '2023-06-11 20:45:05', '2023-06-11 20:45:05');
INSERT INTO `sys_warn` VALUES (1667875636704120834, 1659018792143663105, 3, 0.04, 0.30, 3, '', '0', 'admin', 'admin', '2023-06-11 20:45:14', '2023-06-11 20:45:14');

INSERT INTO `sys_menu` VALUES (1686464473609, '检测预警配置', '', '/admin/syswarn/index', 1000, 'icon-quanxianguanli1', 9, '0', '0', '0', NULL, '2018-01-20 13:17:19', 'admin', '2023-06-11 19:19:13');
INSERT INTO `sys_menu` VALUES (1686464473610, '检测预警查看', 'api_syswarn_get', NULL, 1686464473609, '1', 0, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1686464473611, '检测预警新增', 'api_syswarn_add', NULL, 1686464473609, '1', 1, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1686464473612, '检测预警修改', 'api_syswarn_edit', NULL, 1686464473609, '1', 2, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1686464473613, '检测预警删除', 'api_syswarn_del', NULL, 1686464473609, '1', 3, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');


SET FOREIGN_KEY_CHECKS = 1;
