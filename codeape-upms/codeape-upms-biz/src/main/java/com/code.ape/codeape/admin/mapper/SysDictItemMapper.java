
package com.code.ape.codeape.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.ape.codeape.admin.api.entity.SysDictItem;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典项
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2019/03/19
 */
@Mapper
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {

}
