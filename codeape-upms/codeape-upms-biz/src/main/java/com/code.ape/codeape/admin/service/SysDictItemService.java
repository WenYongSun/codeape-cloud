
package com.code.ape.codeape.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.admin.api.entity.SysDictItem;

/**
 * 字典项
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2019/03/19
 */
public interface SysDictItemService extends IService<SysDictItem> {

	/**
	 * 删除字典项
	 * @param id 字典项ID
	 * @return
	 */
	void removeDictItem(Long id);

	/**
	 * 更新字典项
	 * @param item 字典项
	 * @return
	 */
	void updateDictItem(SysDictItem item);

}
