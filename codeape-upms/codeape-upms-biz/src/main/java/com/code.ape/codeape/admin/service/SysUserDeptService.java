
package com.code.ape.codeape.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.admin.api.entity.SysUserDept;

/**
 * 用户、科室关系
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-19 20:32:39
 */
public interface SysUserDeptService extends IService<SysUserDept> {

}
