
package com.code.ape.codeape.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.admin.api.dto.SysLogDTO;
import com.code.ape.codeape.admin.api.entity.SysLog;
import com.code.ape.codeape.admin.service.SysLogService;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.security.annotation.IgnoreAuth;
import com.code.ape.codeape.common.security.annotation.Inner;
import com.code.ape.codeape.common.security.annotation.RepeatSubmit;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 日志表 前端控制器
 * </p>
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @since 2019/2/1
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/log")
@Tag(name = "日志管理模块")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class SysLogController {

	private final SysLogService sysLogService;

	/**
	 * 简单分页查询
	 * @param page 分页对象
	 * @param sysLog 系统日志
	 * @return
	 */
	@GetMapping("/page")
	public R<IPage<SysLog>> getLogPage(Page page, SysLogDTO sysLog) {
		return R.ok(sysLogService.getLogByPage(page, sysLog));
	}

	/**
	 * 删除日志
	 * @param id ID
	 * @return success/false
	 */
	@DeleteMapping("/{id:\\d+}")
	@RepeatSubmit
	@PreAuthorize("@pms.hasPermission('sys_log_del')")
	public R<Boolean> removeById(@PathVariable Long id) {
		return R.ok(sysLogService.removeById(id));
	}

	/**
	 * 插入日志
	 * @param sysLog 日志实体
	 * @return success/false
	 */
	@Inner
	//todo 异步插入日志，暂时没配置异步token传递，先不走security拦截校验的逻辑（@IgnoreAuth）
	@IgnoreAuth
	@RepeatSubmit
	@PostMapping
	public R<Boolean> save(@Valid @RequestBody SysLog sysLog) {
		return R.ok(sysLogService.save(sysLog));
	}

	/**
	 * 导出excel 表格
	 * @param sysLog 查询条件
	 * @return EXCEL
	 */
	@ResponseExcel
	@RepeatSubmit
	@GetMapping("/export")
	@PreAuthorize("@pms.hasPermission('sys_log_import_export')")
	public List<SysLog> export(SysLogDTO sysLog) {
		return sysLogService.getLogList(sysLog);
	}

}
