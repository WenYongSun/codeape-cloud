
package com.code.ape.codeape.admin.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.ape.codeape.admin.api.entity.constant.SysPostConstant;
import com.code.ape.codeape.admin.api.entity.SysPost;
import com.code.ape.codeape.admin.api.vo.PostExcelVO;
import com.code.ape.codeape.admin.mapper.SysPostMapper;
import com.code.ape.codeape.admin.service.SysPostService;
import com.code.ape.codeape.common.core.exception.ErrorCodes;
import com.code.ape.codeape.common.core.util.AssertUtil;
import com.code.ape.codeape.common.core.util.MsgUtils;
import com.code.ape.codeape.common.core.util.R;
import com.pig4cloud.plugin.excel.vo.ErrorMessage;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 岗位管理表 服务实现类
 *
 * @author codeape code generator
 * @date 2022-03-15 17:18:40
 */
@Service
public class SysPostServiceImpl extends ServiceImpl<SysPostMapper, SysPost> implements SysPostService {

	/**
	 * 导入岗位
	 * @param excelVOList 岗位列表
	 * @param bindingResult 错误信息列表
	 * @return ok fail
	 */
	@Override
	public R importPost(List<PostExcelVO> excelVOList, BindingResult bindingResult) {
		// 通用校验获取失败的数据
		List<ErrorMessage> errorMessageList = (List<ErrorMessage>) bindingResult.getTarget();

		// 个性化校验逻辑
		List<SysPost> postList = this.list();

		// 执行数据插入操作 组装 PostDto
		for (PostExcelVO excel : excelVOList) {
			Set<String> errorMsg = new HashSet<>();
			// 检验岗位名称或者岗位编码是否存在
			boolean existPost = postList.stream()
				.anyMatch(post -> excel.getPostName().equals(post.getPostName())
						|| excel.getPostCode().equals(post.getPostCode()));

			if (existPost) {
				errorMsg.add(MsgUtils.getMessage(ErrorCodes.SYS_POST_NAMEORCODE_EXISTING, excel.getPostName(),
						excel.getPostCode()));
			}

			// 数据合法情况
			if (CollUtil.isEmpty(errorMsg)) {
				insertExcelPost(excel);
			}
			else {
				// 数据不合法
				errorMessageList.add(new ErrorMessage(excel.getLineNum(), errorMsg));
			}
		}
		if (CollUtil.isNotEmpty(errorMessageList)) {
			return R.failed(errorMessageList);
		}
		return R.ok();
	}

	/**
	 * 导出excel 表格
	 * @return
	 */
	@Override
	public List<PostExcelVO> listPost() {
		List<SysPost> postList = this.list(Wrappers.emptyWrapper());
		// 转换成execl 对象输出
		return postList.stream().map(post -> {
			PostExcelVO postExcelVO = new PostExcelVO();
			BeanUtil.copyProperties(post, postExcelVO);
			return postExcelVO;
		}).collect(Collectors.toList());
	}

    @Override
    public Boolean removePostById(Long id) {
		//step1 查询
		SysPost sysPost = baseMapper.selectById(id);
		AssertUtil.isTrue(Objects.nonNull(sysPost),"岗位不存在！");

		//step2 校验内置编码
		AssertUtil.isTrue(!SysPostConstant.POST_CODE_LIST.contains(sysPost.getPostCode()),"系统内置标识，不允许删除！");

		//step3 执行删除逻辑
		int postRows = baseMapper.deleteById(id);
		AssertUtil.isTrue(postRows==1,"删除失败！");
		return Boolean.TRUE;
	}

	@Override
	public Boolean updatePostById(SysPost sysPost) {
		//step1 查询信息
		SysPost post = baseMapper.selectById(sysPost.getPostId());
		AssertUtil.isTrue(Objects.nonNull(post),"岗位不存在！");
		//step2 校验内置编码
		AssertUtil.isTrue(!SysPostConstant.POST_CODE_LIST.contains(post.getPostCode()),"系统内置标识，不允许修改！");
		//step3 不再校验添加的PostCode了，数据库加了唯一索引
		int postRows = baseMapper.updateById(post);
		AssertUtil.isTrue(postRows==1,"修改失败！");
		return Boolean.TRUE;
	}

	/**
	 * 插入excel Post
	 */
	private void insertExcelPost(PostExcelVO excel) {
		SysPost sysPost = new SysPost();
		BeanUtil.copyProperties(excel, sysPost);
		this.save(sysPost);
	}

}
