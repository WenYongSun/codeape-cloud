

package com.code.ape.codeape.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.ape.codeape.admin.api.entity.SysDeptRelation;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @since 2019/2/1
 */
@Mapper
public interface SysDeptRelationMapper extends BaseMapper<SysDeptRelation> {

	/**
	 * 删除部门节点关系
	 * @param deptRelation 待删除的某一个部门节点
	 */
	void deleteDeptRelations(SysDeptRelation deptRelation);

	/**
	 * 删除部门节点关系,同时删除所有关联此部门子节点的部门关系
	 * @param id 待删除的部门节点ID
	 */
	void deleteDeptRelationsById(Long id);

	/**
	 * 新增部门节点关系
	 * @param deptRelation 待新增的部门节点关系
	 */
	void insertDeptRelations(SysDeptRelation deptRelation);

}
