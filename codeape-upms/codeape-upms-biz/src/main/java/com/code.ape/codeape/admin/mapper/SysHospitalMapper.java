

package com.code.ape.codeape.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.ape.codeape.admin.api.entity.SysHospital;
import org.apache.ibatis.annotations.Mapper;

/**
 * 医院表
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-17 17:19:22
 */
@Mapper
public interface SysHospitalMapper extends BaseMapper<SysHospital> {

}
