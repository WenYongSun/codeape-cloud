

package com.code.ape.codeape.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.admin.api.dto.SysWarnDTO;
import com.code.ape.codeape.admin.api.vo.SysWarnVO;
import com.code.ape.codeape.admin.service.SysWarnService;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.log.annotation.SysLog;
import com.code.ape.codeape.common.security.annotation.InjectAuth;
import com.code.ape.codeape.common.security.annotation.Inner;
import com.code.ape.codeape.common.security.annotation.RepeatSubmit;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 检测预警
 *
 * @author pig code generator
 * @date 2023-06-11 14:21:13
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/warn" )
@Tag(name = "检测预警管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class SysWarnController {

    private final SysWarnService sysWarnService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param dto 检测预警
     * @return
     */
    @Operation(summary = "分页查询", description = "分页查询")
    @GetMapping("/page" )
	@InjectAuth
    @PreAuthorize("@pms.hasPermission('api_syswarn_get')" )
    public R<Page<SysWarnVO>> getSysWarnPage(Page<SysWarnVO> page, SysWarnDTO dto) {
        return R.ok(sysWarnService.listPage(page,dto));
    }

	@Operation(summary = "根据医院Id查询预警配置", description = "根据医院Id查询预警配置")
	@GetMapping("/hos/{hosId}" )
	@Inner
	public R<List<SysWarnVO>> getSysWarnByHosId(@PathVariable("hosId") Long hosId) {
		return R.ok(sysWarnService.listByHosId(hosId));
	}


    /**
     * 通过id查询检测预警
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id查询", description = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('api_syswarn_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(sysWarnService.getById(id));
    }

    /**
     * 新增检测预警
     * @param dto 检测预警
     * @return R
     */
    @Operation(summary = "新增检测预警", description = "新增检测预警")
    @SysLog("新增检测预警" )
    @PostMapping
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('api_syswarn_add')" )
    public R save(@RequestBody @Validated(value = SysWarnDTO.SysWarnDTOADD.class) SysWarnDTO dto) {
        return R.ok(sysWarnService.saveSysWarn(dto));
    }

    /**
     * 修改检测预警
     * @param dto 检测预警
     * @return R
     */
    @Operation(summary = "修改检测预警", description = "修改检测预警")
    @SysLog("修改检测预警" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('api_syswarn_edit')" )
    public R updateById(@RequestBody @Validated(value = SysWarnDTO.SysWarnDTOUpdate.class) SysWarnDTO dto) {
        return R.ok(sysWarnService.updateSysWarn(dto));
    }

    /**
     * 通过id删除检测预警
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id删除检测预警", description = "通过id删除检测预警")
    @SysLog("通过id删除检测预警" )
    @DeleteMapping("/{id}" )
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('api_syswarn_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(sysWarnService.removeById(id));
    }

}
