

package com.code.ape.codeape.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.ape.codeape.admin.api.entity.SysUserPost;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户岗位 Mapper 接口
 * </p>
 *
 * @author fxz
 * @since 2022/3/19
 */
@Mapper
public interface SysUserPostMapper extends BaseMapper<SysUserPost> {

}
