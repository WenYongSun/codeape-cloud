

package com.code.ape.codeape.admin.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.admin.api.entity.SysDept;
import com.code.ape.codeape.common.core.entity.BaseParam;

import java.util.List;

/**
 * <p>
 * 部门管理 服务类
 * </p>
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @since 2019/2/1
 */
public interface SysDeptService extends IService<SysDept> {

	/**
	 * 查询部门树菜单
	 * @return 树
	 */
	List<Tree<Long>> listDeptTrees();

	/**
	 * 查询用户部门树
	 * @return
	 */
	List<Tree<Long>> listCurrentUserDeptTrees();

	/**
	 * 查询科室列表
	 * @return
	 */
	List<SysDept> listDept(BaseParam param);

	/**
	 * 添加信息部门
	 * @param sysDept
	 * @return
	 */
	Boolean saveDept(SysDept sysDept);

	/**
	 * 删除部门
	 * @param id 部门 ID
	 * @return 成功、失败
	 */
	Boolean removeDeptById(Long id);

	/**
	 * 更新部门
	 * @param sysDept 部门信息
	 * @return 成功、失败
	 */
	Boolean updateDeptById(SysDept sysDept);

	/**
	 * 查找指定部门的子部门id列表
	 * @param deptId 部门id
	 * @return List<Long>
	 */
	List<Long> listChildDeptId(Long deptId);

}
