

package com.code.ape.codeape.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.admin.api.dto.SysWarnDTO;
import com.code.ape.codeape.admin.api.entity.SysWarn;
import com.code.ape.codeape.admin.api.vo.SysWarnVO;
import com.code.ape.codeape.device.api.dto.QcRecordDTO;
import com.code.ape.codeape.device.api.vo.QcRecordVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 检测预警
 *
 * @author pig code generator
 * @date 2023-06-11 14:21:13
 */
@Mapper
public interface SysWarnMapper extends BaseMapper<SysWarn> {
	Page<SysWarnVO> selectSysWarnPage(Page<SysWarnVO> page, @Param("dto") SysWarnDTO dto);
}
