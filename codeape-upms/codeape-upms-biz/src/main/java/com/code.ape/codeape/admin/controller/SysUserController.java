

package com.code.ape.codeape.admin.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.admin.api.dto.UserDTO;
import com.code.ape.codeape.admin.api.dto.UserInfo;
import com.code.ape.codeape.admin.api.entity.SysUser;
import com.code.ape.codeape.admin.api.vo.UserExcelVO;
import com.code.ape.codeape.admin.api.vo.UserInfoVO;
import com.code.ape.codeape.admin.api.vo.UserVO;
import com.code.ape.codeape.admin.mapper.SysUserMapper;
import com.code.ape.codeape.admin.mapper.SysUserRoleMapper;
import com.code.ape.codeape.admin.service.SysUserService;
import com.code.ape.codeape.common.core.constant.CommonConstants;
import com.code.ape.codeape.common.core.constant.SecurityConstants;
import com.code.ape.codeape.common.core.exception.ErrorCodes;
import com.code.ape.codeape.common.core.util.MsgUtils;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.log.annotation.SysLog;
import com.code.ape.codeape.common.security.annotation.IgnoreAuth;
import com.code.ape.codeape.common.security.annotation.InjectAuth;
import com.code.ape.codeape.common.security.annotation.Inner;
import com.code.ape.codeape.common.security.annotation.RepeatSubmit;
import com.code.ape.codeape.common.security.service.CodeapeUser;
import com.code.ape.codeape.common.security.util.SecurityUtils;
import com.code.ape.codeape.common.xss.core.XssCleanIgnore;
import com.pig4cloud.plugin.excel.annotation.RequestExcel;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.RequiresNonNull;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2019/2/1
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
@Tag(name = "用户管理模块")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class SysUserController {

	private final SysUserService userService;

	/**
	 * 获取当前用户全部信息
	 * @return 用户信息
	 */
	@GetMapping(value = { "/info" })
	public R<UserInfoVO> info() {
		CodeapeUser codeapeUser = Objects.requireNonNull(SecurityUtils.getUser());
		SysUser user = userService.getOne(Wrappers.<SysUser>query().lambda()
				.eq(SysUser::getUsername, codeapeUser.getUsername())
				.eq(!StrUtil.equals(SecurityConstants.SYSTEM_ADMIN_USERNAME,codeapeUser.getUsername()),SysUser::getHosId,codeapeUser.getHosId()));
		if (user == null) {
			return R.failed(MsgUtils.getMessage(ErrorCodes.SYS_USER_QUERY_ERROR));
		}
		UserInfo userInfo = userService.getUserInfo(user);
		UserInfoVO vo = new UserInfoVO();
		vo.setSysUser(userInfo.getSysUser());
		vo.setRoles(userInfo.getRoles());
		vo.setPermissions(userInfo.getPermissions());
		return R.ok(vo);
	}

	/**
	 * 获取指定用户全部信息
	 * @return 用户信息
	 */
	@IgnoreAuth
	@GetMapping("/info/{username}")
	public R<UserInfo> info(@PathVariable String username) {
		SysUser user = userService.getOne(Wrappers.<SysUser>query().lambda()
				.eq(SysUser::getUsername, username)
				.eq(SysUser::getHosId,Objects.requireNonNull(SecurityUtils.getUser()).getHosId()));
		if (user == null) {
			return R.failed(MsgUtils.getMessage(ErrorCodes.SYS_USER_USERINFO_EMPTY, username));
		}
		return R.ok(userService.getUserInfo(user));
	}

	@IgnoreAuth
	@GetMapping("/info/hos")
	public R<UserInfo> loadInfoByUserNameAndHosId(String username,Long hosId) {
		//此处特殊处理：系统管理员可以登录任意医院 admin
		SysUser user = userService.getOne(Wrappers.<SysUser>query().lambda()
				.eq(SysUser::getUsername, username)
				.eq(!StrUtil.equals(SecurityConstants.SYSTEM_ADMIN_USERNAME,username),SysUser::getHosId,hosId));
		if (user == null) {
			return R.failed(MsgUtils.getMessage(ErrorCodes.SYS_USER_USERINFO_EMPTY, username));
		}
		return R.ok(userService.getUserInfo(user));
	}


	/**
	 * 根据部门id，查询对应的用户 id 集合
	 * @param deptIds 部门id 集合
	 * @return 用户 id 集合
	 */
	@Inner
	@GetMapping("/ids")
	public R<List<Long>> listUserIdByDeptIds(@RequestParam("deptIds") Set<Long> deptIds) {
		return R.ok(userService.listUserIdByDeptIds(deptIds));
	}

	/**
	 * 根据用户ID查询集合查询用户信息
	 * @param ids 用户ID集合
	 * @return 用户 id 集合
	 */
	@Inner
	@GetMapping("/userIds")
	@Operation(summary = "根据用户ID查询集合查询用户信息", description = "根据用户ID查询集合查询用户信息")
	public R<List<SysUser>> listUserInfoByIds(@RequestParam("ids") Set<Long> ids) {
		return R.ok(userService.list(Wrappers.<SysUser>lambdaQuery()
				.in(SysUser::getUserId,ids)
		));
	}

	@GetMapping("/list/{code}")
	@Operation(summary = "根据岗位编码查询人员信息", description = "根据岗位编码查询人员信息")
	public R<List<UserVO>> listByPostCode(@PathVariable("code")String postCode) {
		return R.ok(userService.listByPostCode(postCode));
	}


	/**
	 * 通过ID查询用户信息
	 * @param id ID
	 * @return 用户信息
	 */
	@GetMapping("/{id:\\d+}")
	public R<UserVO> user(@PathVariable Long id) {
		return R.ok(userService.getUserVoById(id));
	}

	/**
	 * 判断用户是否存在
	 * @param userDTO 查询条件
	 * @return
	 */
	@Inner(false)
	@GetMapping("/check/exist")
	public R<Boolean> isExist(UserDTO userDTO) {
		userDTO.setHosId(Objects.requireNonNull(SecurityUtils.getUser()).getHosId());
		SysUser sysUser = new SysUser();
		BeanUtil.copyProperties(userDTO,sysUser);
		List<SysUser> sysUserList = userService.list(new QueryWrapper<>(sysUser));
		if (CollUtil.isNotEmpty(sysUserList)) {
			return R.ok(Boolean.TRUE, MsgUtils.getMessage(ErrorCodes.SYS_USER_EXISTING));
		}
		return R.ok(Boolean.FALSE);
	}

	/**
	 * 删除用户信息
	 * @param id ID
	 * @return R
	 */
	@SysLog("删除用户信息")
	@DeleteMapping("/{id:\\d+}")
	@RepeatSubmit
	@PreAuthorize("@pms.hasPermission('sys_user_del')")
	public R<Boolean> userDel(@PathVariable Long id) {
		SysUser sysUser = userService.getById(id);
		return R.ok(userService.removeUserById(sysUser));
	}

	/**
	 * 添加用户
	 * @param userDto 用户信息
	 * @return success/false
	 */
	@SysLog("添加用户")
	@PostMapping
	@XssCleanIgnore({ "password" })
	@PreAuthorize("@pms.hasPermission('sys_user_add')")
	@RepeatSubmit
	public R<Boolean> user(@RequestBody UserDTO userDto) {
		//直接设置当前医院id
		userDto.setHosId(Objects.requireNonNull(SecurityUtils.getUser()).getHosId());
		return R.ok(userService.saveUser(userDto));
	}

	/**
	 * 管理员更新用户信息
	 * @param userDto 用户信息
	 * @return R
	 */
	@SysLog("更新用户信息")
	@PutMapping
	@XssCleanIgnore({ "password" })
	@PreAuthorize("@pms.hasPermission('sys_user_edit')")
	@RepeatSubmit
	public R<Boolean> updateUser(@Valid @RequestBody UserDTO userDto) {
		return userService.updateUser(userDto);
	}

	/**
	 * 分页查询用户
	 * @param page 参数集
	 * @param userDTO 查询参数列表
	 * @return 用户集合
	 */
	@InjectAuth
	@GetMapping("/page")
	public R<IPage<UserVO>> getUserPage(Page page, UserDTO userDTO) {
		userDTO.setHosId(Objects.requireNonNull(SecurityUtils.getUser().getHosId()));
		return R.ok(userService.getUserWithRolePage(page, userDTO));
	}

	/**
	 * 个人修改个人信息
	 * @param userDto userDto
	 * @return success/false
	 */
	@SysLog("修改个人信息")
	@PutMapping("/edit")
	@XssCleanIgnore({ "password", "newpassword1" })
	@RepeatSubmit
	public R<Boolean> updateUserInfo(@Valid @RequestBody UserDTO userDto) {
		userDto.setUsername(SecurityUtils.getUser().getUsername());
		userDto.setHosId(SecurityUtils.getUser().getHosId());
		return userService.updateUserInfo(userDto);
	}

	/**
	 * @param username 用户名称
	 * @return 上级部门用户列表
	 */
	@GetMapping("/ancestor/{username}")
	public R<List<SysUser>> listAncestorUsers(@PathVariable String username) {
		return R.ok(userService.listAncestorUsersByUsername(username));
	}

	/**
	 * 导出excel 表格
	 * @param userDTO 查询条件
	 * @return
	 */
	@ResponseExcel
	@GetMapping("/export")
	@PreAuthorize("@pms.hasPermission('sys_user_import_export')")
	@RepeatSubmit
	public List<UserExcelVO> export(UserDTO userDTO) {
		userDTO.setHosId(Objects.requireNonNull(SecurityUtils.getUser().getHosId()));
		return userService.listUser(userDTO);
	}

	/**
	 * 导入用户
	 * @param excelVOList 用户列表
	 * @param bindingResult 错误信息列表
	 * @return R
	 */
	@PostMapping("/import")
	@PreAuthorize("@pms.hasPermission('sys_user_import_export')")
	@RepeatSubmit
	public R importUser(@RequestExcel List<UserExcelVO> excelVOList, BindingResult bindingResult) {
		return userService.importUser(excelVOList, bindingResult);
	}

}
