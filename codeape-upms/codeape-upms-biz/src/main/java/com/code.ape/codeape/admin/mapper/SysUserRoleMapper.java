

package com.code.ape.codeape.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.ape.codeape.admin.api.entity.SysRole;
import com.code.ape.codeape.admin.api.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @since 2019/2/1
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

	/**
	 * 根据用户Id删除该用户的角色关系
	 * @param userId 用户ID
	 * @return boolean
	 * @author 寻欢·李
	 * @date 2017年12月7日 16:31:38
	 */
	Boolean deleteByUserId(@Param("userId") Long userId);

	/**
	 * 根据username获取这个用户的全部角色
	 * @param username
	 */
	List<SysRole> listByUserName(String username);

}
