

package com.code.ape.codeape.admin;

import com.code.ape.codeape.common.feign.annotation.EnableCodeapeFeignClients;
import com.code.ape.codeape.common.security.annotation.EnableCodeapeResourceServer;
import com.code.ape.codeape.common.swagger.annotation.EnableCodeapeDoc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2018年06月21日 用户统一管理系统
 */
@EnableCodeapeDoc
@EnableCodeapeResourceServer
@EnableCodeapeFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class CodeapeAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeapeAdminApplication.class, args);
	}

}
