

package com.code.ape.codeape.admin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.admin.api.dto.SysTimeTypeDTO;
import com.code.ape.codeape.admin.api.entity.SysTimeType;
import com.code.ape.codeape.admin.service.SysTimeTypeService;
import com.code.ape.codeape.common.core.constant.CacheConstants;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.log.annotation.SysLog;
import com.code.ape.codeape.common.security.annotation.Inner;
import com.code.ape.codeape.common.security.annotation.RepeatSubmit;
import com.code.ape.codeape.common.security.util.SecurityUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;


/**
 * 检测时段
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-22 11:14:25
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/systimetype" )
@Tag(name = "检测时段管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class SysTimeTypeController {

    private final SysTimeTypeService sysTimeTypeService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param sysTimeType 检测时段
     * @return
     */
    @Operation(summary = "分页查询", description = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('admin_systimetype_get')" )
    public R getSysTimeTypePage(Page page, SysTimeType sysTimeType) {
        return R.ok(sysTimeTypeService.page(page, Wrappers.lambdaQuery(sysTimeType).eq(SysTimeType::getHosId, Objects.requireNonNull(SecurityUtils.getUser()).getHosId()).orderByAsc(SysTimeType::getPriority)));
    }

    @Cacheable(value = CacheConstants.SYS_TIME_TYPE,key = "#model+'_'+#hosId",unless = "#result == null")
	@Operation(summary = "根据模块查询", description = "根据模块查询")
	@GetMapping("/by/model" )
	@Inner
	public R<List<SysTimeType>> getSysTimeTypePage(@Parameter(description = "模块") String model,@Parameter(description = "医院ID")Long hosId) {
    	return R.ok(sysTimeTypeService.list(Wrappers.<SysTimeType>lambdaQuery()
				.eq(SysTimeType::getInHos,model)
				.eq(SysTimeType::getHosId,hosId)));
	}


    /**
     * 通过id查询检测时段
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id查询", description = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('admin_systimetype_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(sysTimeTypeService.getById(id));
    }

    /**
     * 新增检测时段
     * @param sysTimeTypeDTO 检测时段
     * @return R
     */
    @Operation(summary = "新增检测时段", description = "新增检测时段")
    @SysLog("新增检测时段" )
    @PostMapping
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('admin_systimetype_add')" )
    public R save(@Validated(value = {SysTimeTypeDTO.SysTimeTypeADD.class}) @RequestBody SysTimeTypeDTO sysTimeTypeDTO) {
        return R.ok(sysTimeTypeService.saveSysTime(sysTimeTypeDTO));
    }

    /**
     * 修改检测时段
     * @param sysTimeType 检测时段
     * @return R
     */
    @Operation(summary = "修改检测时段", description = "修改检测时段")
    @SysLog("修改检测时段" )
    @PutMapping
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('admin_systimetype_edit')" )
    public R updateById(@Validated(value = {SysTimeTypeDTO.SysTimeTypeUpdate.class}) @RequestBody SysTimeTypeDTO sysTimeType) {
        return R.ok(sysTimeTypeService.updateSysTime(sysTimeType));
    }

    /**
     * 通过id删除检测时段
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id删除检测时段", description = "通过id删除检测时段")
    @SysLog("通过id删除检测时段" )
    @DeleteMapping("/{id}" )
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('admin_systimetype_del')" )
	@CacheEvict(value = CacheConstants.SYS_TIME_TYPE, allEntries = true)
    public R removeById(@PathVariable Long id) {
        return R.ok(sysTimeTypeService.removeById(id));
    }

	@Operation(summary = "清除检测时段缓存", description = "清除检测时段缓存")
	@SysLog("清除检测时段缓存" )
	@DeleteMapping("/clear" )
	@RepeatSubmit
	@PreAuthorize("@pms.hasPermission('admin_systimetype_del')" )
	public R clearCache() {
		return R.ok(sysTimeTypeService.clearCache());
	}
}
