
package com.code.ape.codeape.admin.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.ape.codeape.admin.api.dto.SysTimeTypeDTO;
import com.code.ape.codeape.admin.api.entity.SysTimeType;
import com.code.ape.codeape.admin.mapper.SysTimeTypeMapper;
import com.code.ape.codeape.admin.service.SysTimeTypeService;
import com.code.ape.codeape.common.core.constant.CacheConstants;
import com.code.ape.codeape.common.core.exception.ErrorCodes;
import com.code.ape.codeape.common.core.util.MsgUtils;
import com.code.ape.codeape.common.security.util.SecurityUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.text.MessageFormat;
import java.util.Objects;

/**
 * 检测时段
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-22 11:14:25
 */
@Service
public class SysTimeTypeServiceImpl extends ServiceImpl<SysTimeTypeMapper, SysTimeType> implements SysTimeTypeService {

	@CacheEvict(value = CacheConstants.SYS_TIME_TYPE, allEntries = true)
	@Transactional
	@Override
	public Boolean saveSysTime(SysTimeTypeDTO sysTimeTypeDTO) {
		//step1：普通时段：startTime.endTime，valueDown，valueUp不能为空
		Assert.isTrue(!StrUtil.equals(sysTimeTypeDTO.getType(),"1") || StrUtil.isAllNotBlank(sysTimeTypeDTO.getStartTime(), sysTimeTypeDTO.getEndTime()) && ObjectUtil.isAllNotEmpty(sysTimeTypeDTO.getValueDown(), sysTimeTypeDTO.getValueUp()), MsgUtils.getMessage(ErrorCodes.SYS_TIME_TYPE_ADD));

		SysTimeType sysTimeType = new SysTimeType();
		BeanUtil.copyProperties(sysTimeTypeDTO,sysTimeType);
		//step2 组装timeScope
		sysTimeType.setTimeScope(StrUtil.equals(sysTimeTypeDTO.getType(),"1")? MessageFormat.format("{0}-{1}",sysTimeTypeDTO.getStartTime(),sysTimeTypeDTO.getEndTime()):null);
		sysTimeType.setHosId(Objects.requireNonNull(SecurityUtils.getUser()).getHosId());
		int total = baseMapper.insert(sysTimeType);
		return total==1;
	}


	@CacheEvict(value = CacheConstants.SYS_TIME_TYPE, allEntries = true)
	@Transactional
	@Override
	public Boolean updateSysTime(SysTimeTypeDTO sysTimeTypeDTO) {
		//step1：普通时段：startTime.endTime，valueDown，valueUp不能为空
		Assert.isTrue(!StrUtil.equals(sysTimeTypeDTO.getType(),"1") || StrUtil.isAllNotBlank(sysTimeTypeDTO.getStartTime(), sysTimeTypeDTO.getEndTime()) && ObjectUtil.isAllNotEmpty(sysTimeTypeDTO.getValueDown(), sysTimeTypeDTO.getValueUp()), MsgUtils.getMessage(ErrorCodes.SYS_TIME_TYPE_ADD));

		SysTimeType sysTimeType = new SysTimeType();
		BeanUtil.copyProperties(sysTimeTypeDTO,sysTimeType);
		//step2 组装timeScope
		sysTimeType.setTimeScope(StrUtil.equals(sysTimeTypeDTO.getType(),"1")? MessageFormat.format("{0}-{1}",sysTimeTypeDTO.getStartTime(),sysTimeTypeDTO.getEndTime()):null);
		int total = baseMapper.updateById(sysTimeType);
		return total==1;
	}

	@CacheEvict(value = CacheConstants.SYS_TIME_TYPE,allEntries = true)
	@Override
	public Boolean clearCache() {
		return Boolean.TRUE;
	}
}
