

package com.code.ape.codeape.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.admin.api.entity.SysPost;
import com.code.ape.codeape.admin.api.vo.PostExcelVO;
import com.code.ape.codeape.common.core.util.R;
import org.springframework.validation.BindingResult;

import java.util.List;

/**
 * 岗位管理 服务类
 *
 * @author fxz
 * @date 2022-03-15 17:18:40
 */
public interface SysPostService extends IService<SysPost> {

	/**
	 * 导入岗位
	 * @param excelVOList 岗位列表
	 * @param bindingResult 错误信息列表
	 * @return ok fail
	 */
	R importPost(List<PostExcelVO> excelVOList, BindingResult bindingResult);

	/**
	 * 导出excel 表格
	 * @return
	 */
	List<PostExcelVO> listPost();

	Boolean removePostById(Long id);

	Boolean updatePostById(SysPost sysPost);
}
