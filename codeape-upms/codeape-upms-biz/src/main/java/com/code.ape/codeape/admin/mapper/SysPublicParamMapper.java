
package com.code.ape.codeape.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.ape.codeape.admin.api.entity.SysPublicParam;
import org.apache.ibatis.annotations.Mapper;

/**
 * 公共参数配置
 *
 * @author Lucky
 * @date 2019-04-29
 */
@Mapper
public interface SysPublicParamMapper extends BaseMapper<SysPublicParam> {

}
