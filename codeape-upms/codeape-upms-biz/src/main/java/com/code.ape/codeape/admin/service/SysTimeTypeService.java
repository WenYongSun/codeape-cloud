

package com.code.ape.codeape.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.admin.api.dto.SysTimeTypeDTO;
import com.code.ape.codeape.admin.api.entity.SysTimeType;

/**
 * 检测时段
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-22 11:14:25
 */
public interface SysTimeTypeService extends IService<SysTimeType> {

	Boolean saveSysTime(SysTimeTypeDTO sysTimeTypeDTO);

	Boolean updateSysTime(SysTimeTypeDTO sysTimeTypeDTO);

	Boolean clearCache();

}
