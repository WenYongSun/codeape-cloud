

package com.code.ape.codeape.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.admin.api.dto.SysHospitalDTO;
import com.code.ape.codeape.admin.api.entity.SysHospital;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.List;

/**
 * 医院表
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-17 17:19:22
 */
public interface SysHospitalService extends IService<SysHospital> {

	List<SysHospital> listByUserName(String username);

	Boolean saveHos(SysHospitalDTO dto);

}
