

package com.code.ape.codeape.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.ape.codeape.admin.api.entity.SysOauthClientDetails;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @since 2019/2/1
 */
@Mapper
public interface SysOauthClientDetailsMapper extends BaseMapper<SysOauthClientDetails> {

}
