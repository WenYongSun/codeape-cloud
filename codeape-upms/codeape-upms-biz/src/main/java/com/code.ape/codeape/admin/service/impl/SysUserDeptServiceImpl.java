package com.code.ape.codeape.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.ape.codeape.admin.api.entity.SysUserDept;
import com.code.ape.codeape.admin.mapper.SysUserDeptMapper;
import com.code.ape.codeape.admin.service.SysUserDeptService;
import org.springframework.stereotype.Service;

/**
 * 用户、科室关系
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-19 20:32:39
 */
@Service
public class SysUserDeptServiceImpl extends ServiceImpl<SysUserDeptMapper, SysUserDept> implements SysUserDeptService {

}
