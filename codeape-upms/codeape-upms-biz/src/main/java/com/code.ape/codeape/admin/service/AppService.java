

package com.code.ape.codeape.admin.service;

import com.code.ape.codeape.admin.api.dto.AppSmsDTO;
import com.code.ape.codeape.common.core.util.R;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2018/11/14
 */
public interface AppService {

	/**
	 * 发送手机验证码
	 * @param sms phone
	 * @return code
	 */
	R<Boolean> sendSmsCode(AppSmsDTO sms);

	/**
	 * 校验验证码
	 * @param phone 手机号
	 * @param code 验证码
	 * @return
	 */
	boolean check(String phone, String code);

}
