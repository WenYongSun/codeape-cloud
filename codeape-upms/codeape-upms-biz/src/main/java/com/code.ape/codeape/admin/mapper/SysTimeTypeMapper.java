

package com.code.ape.codeape.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.ape.codeape.admin.api.entity.SysTimeType;
import org.apache.ibatis.annotations.Mapper;

/**
 * 检测时段
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-22 11:14:25
 */
@Mapper
public interface SysTimeTypeMapper extends BaseMapper<SysTimeType> {

}
