
package com.code.ape.codeape.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.ape.codeape.admin.api.entity.SysUserDept;
import com.code.ape.codeape.admin.api.vo.SysUserDeptVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 用户、科室关系
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-19 20:32:39
 */
@Mapper
public interface SysUserDeptMapper extends BaseMapper<SysUserDept> {
	List<SysUserDeptVO> listByUserId(Long userId);
}
