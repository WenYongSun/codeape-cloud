
package com.code.ape.codeape.admin.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.ape.codeape.admin.api.dto.SysWarnDTO;
import com.code.ape.codeape.admin.api.entity.SysWarn;
import com.code.ape.codeape.admin.api.entity.constant.SysWarnModelStatusEnum;
import com.code.ape.codeape.admin.api.vo.SysWarnVO;
import com.code.ape.codeape.admin.mapper.SysWarnMapper;
import com.code.ape.codeape.admin.service.SysWarnService;
import com.code.ape.codeape.common.core.constant.CacheConstants;
import com.code.ape.codeape.common.core.util.AssertUtil;
import com.code.ape.codeape.common.security.util.SecurityUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * 检测预警
 *
 * @author pig code generator
 * @date 2023-06-11 14:21:13
 */
@Service
public class SysWarnServiceImpl extends ServiceImpl<SysWarnMapper, SysWarn> implements SysWarnService {

	@CacheEvict(value = CacheConstants.SYS_WARN_HOS, allEntries = true)
	@Transactional
	@Override
	public boolean saveSysWarn(SysWarnDTO dto) {
		//step1 校验参数
		if (dto.getModel().equals(SysWarnModelStatusEnum.URIC_ACID.getValue()))
			AssertUtil.isTrue(Objects.nonNull(dto.getGender()),"尿酸预警的性别不能为空");

		//step2 校验当前的配置是否存在
		Long count = baseMapper.selectCount(Wrappers.<SysWarn>lambdaQuery()
				.eq(SysWarn::getInHos, dto.getInHos())
				.eq(SysWarn::getHosId, Objects.requireNonNull(SecurityUtils.getUser()).getHosId())
				.eq(SysWarn::getModel, dto.getModel())
				.eq(dto.getModel().equals(SysWarnModelStatusEnum.URIC_ACID.getValue()), SysWarn::getGender, dto.getGender()));
		AssertUtil.isTrue(count==0,"该预警配置已存在！");

		//step3 入库
		SysWarn sysWarn = BeanUtil.toBean(dto, SysWarn.class);
		sysWarn.setHosId(Objects.requireNonNull(SecurityUtils.getUser()).getHosId());
		int sysWarnRows = baseMapper.insert(sysWarn);
		AssertUtil.isTrue(sysWarnRows==1,"添加失败！");
		return true;
	}

	@CacheEvict(value = CacheConstants.SYS_WARN_HOS, allEntries = true)
	@Transactional
	@Override
	public boolean updateSysWarn(SysWarnDTO dto) {
		//step1 查询
		SysWarn sysWarn = baseMapper.selectById(dto.getId());
		AssertUtil.isTrue(Objects.nonNull(sysWarn),"该条配置不存在！");
		//step2 系统配置，因此只允许修改值
		int sysWarnRows = baseMapper.update(null, Wrappers.<SysWarn>lambdaUpdate()
				.set(Objects.nonNull(dto.getValueDown()), SysWarn::getValueDown, dto.getValueDown())
				.set(Objects.nonNull(dto.getValueUp()), SysWarn::getValueUp, dto.getValueUp())
				.eq(SysWarn::getId, dto.getId()));
		AssertUtil.isTrue(sysWarnRows==1,"修改失败！");
		return true;
	}

	@Override
	public Page<SysWarnVO> listPage(Page<SysWarnVO> page, SysWarnDTO dto) {
		return baseMapper.selectSysWarnPage(page,dto);
	}

	@Cacheable(value = CacheConstants.SYS_WARN_HOS, key = "#hosId", unless = "#result == null")
	@Override
	public List<SysWarnVO> listByHosId(Long hosId) {
		List<SysWarn> list = baseMapper.selectList(Wrappers.<SysWarn>lambdaQuery()
				.eq(SysWarn::getHosId, hosId));
		return BeanUtil.copyToList(list,SysWarnVO.class);
	}
}
