

package com.code.ape.codeape.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.admin.api.dto.SysWarnDTO;
import com.code.ape.codeape.admin.api.entity.SysWarn;
import com.code.ape.codeape.admin.api.vo.SysWarnVO;

import java.util.List;

/**
 * 检测预警
 *
 * @author pig code generator
 * @date 2023-06-11 14:21:13
 */
public interface SysWarnService extends IService<SysWarn> {
	boolean saveSysWarn(SysWarnDTO dto);

	boolean updateSysWarn(SysWarnDTO dto);

	/**
	 * 分页查询
	 * @param page
	 * @param dto
	 * @return
	 */
	Page<SysWarnVO> listPage(Page<SysWarnVO> page, SysWarnDTO dto);

	List<SysWarnVO> listByHosId(Long hosId);
}
