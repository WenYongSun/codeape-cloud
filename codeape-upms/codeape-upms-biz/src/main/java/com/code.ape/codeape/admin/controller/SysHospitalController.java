

package com.code.ape.codeape.admin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.admin.api.dto.SysHospitalDTO;
import com.code.ape.codeape.admin.service.SysHospitalService;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.log.annotation.SysLog;
import com.code.ape.codeape.admin.api.entity.SysHospital;
import com.code.ape.codeape.common.security.annotation.IgnoreAuth;
import com.code.ape.codeape.common.security.annotation.Inner;
import com.code.ape.codeape.common.security.annotation.RepeatSubmit;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * 医院
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-17 17:19:22
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/syshospital" )
@Tag(name = "医院管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class SysHospitalController {

    private final SysHospitalService sysHospitalService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param sysHospital 医院
     * @return
     */
    @Operation(summary = "分页查询", description = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('admin_syshospital_get')" )
    public R getSysHospitalPage(Page page, SysHospital sysHospital) {
        return R.ok(sysHospitalService.page(page, Wrappers.query(sysHospital)));
    }


	@Operation(summary = "查询所有的医院信息-内部接口", description = "查询所有的医院信息-内部接口")
	@GetMapping("/hos/all")
	@IgnoreAuth
	@Inner
	public R<List<SysHospital>> getAllSysHospital() {
    	return R.ok(sysHospitalService.list(Wrappers.<SysHospital>lambdaQuery()
				.select(SysHospital::getId,SysHospital::getName)));
	}


	/**
	 * 根据用户名查询所在医院
	 * @IgnoreAuth->接口不需要token即可访问
	 * @param username  用户名
	 */
	@IgnoreAuth
	@Operation(summary = "根据用户名查询所在医院", description = "根据用户名查询所在医院")
	@GetMapping("/listByUserName/{username}" )
	public R<List<SysHospital>> listByUserName(@PathVariable String username) {
		return R.ok(sysHospitalService.listByUserName(username));
	}


    /**
     * 通过id查询医院
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id查询", description = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('admin_syshospital_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(sysHospitalService.getById(id));
    }

    /**
     * 新增医院
     * @param sysHospital 医院
     * @return R
     */
    @Operation(summary = "新增医院", description = "新增医院")
    @SysLog("新增医院" )
    @PostMapping
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('admin_syshospital_add')" )
    public R save(@Valid @RequestBody SysHospitalDTO sysHospital) {
        return R.ok(sysHospitalService.saveHos(sysHospital));
    }

    /**
     * 修改医院
     * @param sysHospital 医院
     * @return R
     */
    @Operation(summary = "修改医院", description = "修改医院")
    @SysLog("修改医院" )
    @PutMapping
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('admin_syshospital_edit')" )
    public R updateById(@RequestBody SysHospital sysHospital) {
        return R.ok(sysHospitalService.updateById(sysHospital));
    }

    /**
     * 通过id删除医院
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id删除医院", description = "通过id删除医院")
    @SysLog("通过id删除医院" )
	@RepeatSubmit
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('admin_syshospital_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(sysHospitalService.removeById(id));
    }

}
