package com.code.ape.codeape.admin.api.entity.constant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description  检测预警模块枚举
 */
@Getter
@RequiredArgsConstructor
public enum SysWarnModelStatusEnum {
	BLOOD_SUGAR(1,"血糖"),

	URIC_ACID(2,"尿酸"),

	BLOOD_KETONE(3,"血酮");

	private final Integer value;

	private final String description;
}
