

package com.code.ape.codeape.admin.api.vo;

import com.code.ape.codeape.admin.api.entity.SysLog;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2019/2/1
 */
@Data
public class LogVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private SysLog sysLog;

	private String username;

}
