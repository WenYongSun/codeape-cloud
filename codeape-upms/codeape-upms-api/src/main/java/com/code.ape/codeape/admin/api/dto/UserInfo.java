

package com.code.ape.codeape.admin.api.dto;

import com.code.ape.codeape.admin.api.entity.SysPost;
import com.code.ape.codeape.admin.api.entity.SysRole;
import com.code.ape.codeape.admin.api.entity.SysUser;
import com.code.ape.codeape.admin.api.vo.SysUserDeptVO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2019/2/1
 * <p>
 * commit('SET_ROLES', data) commit('SET_NAME', data) commit('SET_AVATAR', data)
 * commit('SET_INTRODUCTION', data) commit('SET_PERMISSIONS', data)
 */
@Data
public class UserInfo implements Serializable {

	/**
	 * 用户基本信息
	 */
	private SysUser sysUser;

	/**
	 * 权限标识集合
	 */
	private String[] permissions;

	/**
	 * 角色集合
	 */
	private Long[] roles;

	/**
	 * 角色集合
	 */
	private List<SysRole> roleList;

	/**
	 * 岗位集合
	 */
	private Long[] posts;

	/**
	 * 岗位集合
	 */
	private List<SysPost> postList;

	/**
	 * 当前用户所在的医院ID
	 */
	private Long hosId;

	/**
	 * 科室集合
	 */
	private Long[] deptAuths;
}
