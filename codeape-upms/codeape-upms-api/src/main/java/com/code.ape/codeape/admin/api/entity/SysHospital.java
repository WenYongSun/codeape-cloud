
package com.code.ape.codeape.admin.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 医院表
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-17 17:19:22
 */
@Data
@TableName("sys_hospital")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "医院表")
public class SysHospital extends BaseEntity {

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="id")
    private Long id;

    /**
     * 医院名称
     */
    @Schema(description ="医院名称")
    private String name;

    @Schema(description = "管理员账号")
    private String username;

    /**
     * 医院地址
     */
    @Schema(description ="医院地址")
    private String address;

    /**
     * 医院等级
     */
    @Schema(description ="医院等级")
    private String level;

    /**
     * 联系人
     */
    @Schema(description ="联系人")
    private String contacts;

    /**
     * 联系人电话
     */
    @Schema(description ="联系人电话")
    private String contactsPhone;

    /**
     * 删除标记
     */
    @TableLogic
    @Schema(description ="删除标记")
    private String delFlag;


}
