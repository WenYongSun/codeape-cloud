package com.code.ape.codeape.admin.api.entity.constant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description 岗位枚举，系统内置的岗位，不能修改
 */
@Getter
@RequiredArgsConstructor
public enum SysPostEnum {

	/**
	 * 系统内置岗位：住院护士
	 */
	IN_HOS_NURSE("IN_HOS_NURSE", "住院护士"),

	/**
	 * 系统内置岗位：住院医生
	 */
	IN_HOS_DOC("IN_HOS_DOC", "住院医生"),

	/**
	 * 系统内置岗位：体检医生
	 */
	PHYSICAL_DOC("PHYSICAL_DOC", "体检医生"),

	/**
	 * 系统内置岗位：院外医生
	 */
	OUT_HOS_DOC("OUT_HOS_DOC", "院外医生"),

	/**
	 * 系统内置岗位：管理员
	 */
	ADMIN("ADMIN", "管理员");


	/**
	 * 类型
	 */
	private final String postCode;

	/**
	 * 描述
	 */
	private final String postName;

}
