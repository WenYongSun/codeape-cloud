
package com.code.ape.codeape.admin.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 检测时段
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-22 11:14:25
 */
@Data
@Schema(description = "检测时段")
public class SysTimeTypeDTO  {

    /**
     * id
     */
    @Schema(description ="id")
	@NotNull(message = "id不能为空",groups = {SysTimeTypeUpdate.class})
    private Long id;

    /**
     * 医院id
     */
    @Schema(description ="医院id")
    private Long hosId;

    /**
     * 时间段名称
     */
    @Schema(description ="时间段名称")
	@NotBlank(message = "时段名称不能为空",groups = {SysTimeTypeADD.class,SysTimeTypeUpdate.class})
    private String name;

    /**
     * 开始时间04:30表示4点30
     */
    @Schema(description ="开始时间")
//	@NotBlank(message = "开始时间不能为空",groups = {SysTimeTypeADD.class,SysTimeTypeUpdate.class})
    private String startTime;

    /**
     * 结束时间06:00表示6点00
     */
    @Schema(description ="结束时间")
//	@NotBlank(message = "结束时间不能为空",groups = {SysTimeTypeADD.class,SysTimeTypeUpdate.class})
    private String endTime;

    /**
     * 血糖目标最低
     */
    @Schema(description ="血糖目标最低")
//	@NotNull(message = "最低值不能为空",groups = {SysTimeTypeADD.class,SysTimeTypeUpdate.class})
    private Float valueDown;

    /**
     * 血糖目标最高
     */
    @Schema(description ="血糖目标最高")
//	@NotNull(message = "最高值不能为空",groups = {SysTimeTypeADD.class,SysTimeTypeUpdate.class})
    private Float valueUp;

    /**
     * 排序号
     */
    @Schema(description ="排序号")
	@NotNull(message = "排序号不能为空",groups = {SysTimeTypeADD.class,SysTimeTypeUpdate.class})
    private Integer priority;

    /**
     * 1普通时段,0强化时段
     */
    @Schema(description ="1普通时段,0强化时段")
	@NotNull(message = "类型不能为空",groups = {SysTimeTypeADD.class,SysTimeTypeUpdate.class})
    private String type;

    /**
     * 0 院外,1 院内, 2 门诊 ,3 体检
     */
    @Schema(description ="0 院外,1 院内, 2 门诊 ,3 体检")
	@NotNull(message = "类型不能为空",groups = {SysTimeTypeADD.class,SysTimeTypeUpdate.class})
    private String inHos;

    @Schema(description = "删除状态")
    private String delFlag;

    public static interface SysTimeTypeADD{}

    public static interface SysTimeTypeUpdate{}

    public static interface SysTimeTypeDelete{}


}
