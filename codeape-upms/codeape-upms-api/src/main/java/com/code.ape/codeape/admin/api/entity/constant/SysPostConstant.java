package com.code.ape.codeape.admin.api.entity.constant;

import com.code.ape.codeape.admin.api.entity.SysPost;
import com.code.ape.codeape.admin.api.entity.constant.SysPostEnum;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * 系统内置的岗位编码，不允许修改
 */
public class SysPostConstant {

	/**
	 * 内置岗位编码集合
	 */
	public final static List<String> POST_CODE_LIST= Arrays.stream(SysPostEnum.values()).map(SysPostEnum::getPostCode).collect(Collectors.toList());

	/**
	 * 构建系统内置的SysPost
	 * @param hosId  医院Id
	 * @return
	 */
	public static List<SysPost> buildSysPost(Long hosId){
		return Arrays.stream(SysPostEnum.values())
				.map(data->{
					return SysPost.builder()
							.postName(data.getPostName())
							.postCode(data.getPostCode())
							.hosId(hosId)
							.postSort(0)
							.build();
				}).collect(Collectors.toList());
	}

}
