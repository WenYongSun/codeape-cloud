package com.code.ape.codeape.admin.api.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 客户端请求验证码
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2022/10/13
 */
@Data
public class AppSmsDTO {

	/**
	 * 手机号
	 */
	@NotBlank(message = "手机号不能为空")
	private String phone;

	/**
	 * 手机号是否存在数据库
	 */
	private Boolean exist;

}
