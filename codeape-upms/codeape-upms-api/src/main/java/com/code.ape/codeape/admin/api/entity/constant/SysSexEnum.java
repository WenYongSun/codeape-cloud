package com.code.ape.codeape.admin.api.entity.constant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description 岗位枚举，系统内置的岗位，不能修改
 */
@Getter
@RequiredArgsConstructor
public enum SysSexEnum {

	SEX_MAN("1", "男"),

	SEX_FEMALE("2", "女"),

	SEX_OTHER("3", "其他");

	/**
	 * 类型
	 */
	private final String value;

	/**
	 * 描述
	 */
	private final String description;

}
