package com.code.ape.codeape.admin.api.vo;

import com.code.ape.codeape.admin.api.entity.SysUserDept;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
@Data
public class SysUserDeptVO extends SysUserDept implements Serializable {
	private String deptName;
}
