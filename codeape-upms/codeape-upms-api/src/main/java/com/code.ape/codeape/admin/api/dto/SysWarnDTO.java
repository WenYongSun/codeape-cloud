
package com.code.ape.codeape.admin.api.dto;

import cn.hutool.core.collection.ListUtil;
import com.code.ape.codeape.admin.api.entity.SysWarn;
import com.code.ape.codeape.admin.api.entity.constant.SysSexEnum;
import com.code.ape.codeape.admin.api.entity.constant.SysWarnInHosEnum;
import com.code.ape.codeape.admin.api.entity.constant.SysWarnModelStatusEnum;
import com.code.ape.codeape.common.core.entity.BaseParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 检测预警
 *
 * @author pig code generator
 * @date 2023-06-11 14:21:13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "检测预警")
public class SysWarnDTO extends BaseParam {

    /**
     * id
     */
    @Schema(description ="id")
	@NotNull(message = "ID不能为空",groups = SysWarnDTOUpdate.class)
    private Long id;

    /**
     * 0 院外,1 院内, 2 门诊 ,3 体检
     */
    @Schema(description ="0 院外,1 院内, 2 门诊 ,3 体检")
	@NotNull(message = "使用方不能为空！",groups = {SysWarnDTOADD.class,SysWarnDTOUpdate.class})
    private Integer inHos;

    /**
     * 预警高值
     */
    @Schema(description ="预警高值")
	@NotNull(message = "预警高值不能为空！",groups = {SysWarnDTOADD.class,SysWarnDTOUpdate.class})
    private Float valueUp;

    @Schema(description = "医院ID")
    private Long hosId;

    /**
     * 预警低值
     */
    @Schema(description ="预警低值")
	@NotNull(message = "预警低值不能为空！",groups = {SysWarnDTOADD.class,SysWarnDTOUpdate.class})
    private Float valueDown;

    /**
     * 1 血糖 2 尿酸 3 血酮
     */
    @Schema(description ="1 血糖 2 尿酸 3 血酮")
    private Integer model;

    /**
     * 性别，来源字典 用于尿酸
     */
    @Schema(description ="性别，来源字典 用于尿酸")
    private String gender;

    public interface SysWarnDTOADD{}

	public interface SysWarnDTOUpdate{}

	public interface SysWarnDTOGet{}


	public final static Map<SysWarnModelStatusEnum, Function<SysWarnModelStatusEnum,List<SysWarn>>> SYS_WARN_MODEL_STATUS_ENUM=new HashMap<>(3);

    static{
		SYS_WARN_MODEL_STATUS_ENUM.put(SysWarnModelStatusEnum.BLOOD_SUGAR, (model)-> ListUtil.of(SysWarn.builder().model(model.getValue()).valueDown(3.9f).valueUp(22.1f).build()));

		SYS_WARN_MODEL_STATUS_ENUM.put(SysWarnModelStatusEnum.BLOOD_KETONE, (model)-> ListUtil.of(SysWarn.builder().model(model.getValue()).valueDown(0.3f).valueUp(0.4f).build()));

		SYS_WARN_MODEL_STATUS_ENUM.put(SysWarnModelStatusEnum.URIC_ACID, (model)-> ListUtil.of(SysWarn.builder().model(model.getValue()).valueDown(208f).valueUp(428f).gender(SysSexEnum.SEX_MAN.getValue()).build(),SysWarn.builder().model(model.getValue()).valueDown(208f).valueUp(428f).gender(SysSexEnum.SEX_FEMALE.getValue()).build()));
	}

	/**
	 * 构建系统内置的预警
	 * @return
	 */
	public static List<SysWarn> buildInnerSysWarn(Long hosId){
		List<SysWarn> list=new ArrayList<>();
		for (SysWarnInHosEnum inHosEnum : SysWarnInHosEnum.values()) {
			for (SysWarnModelStatusEnum modelStatusEnum : SysWarnModelStatusEnum.values()) {
				List<SysWarn> sysWarns = SYS_WARN_MODEL_STATUS_ENUM.get(modelStatusEnum).apply(modelStatusEnum).stream().peek(data->{
					data.setHosId(hosId);
					data.setInHos(inHosEnum.getValue());
				}).collect(Collectors.toList());
				list.addAll(sysWarns);
			}
		}
    	return list;
	}
}
