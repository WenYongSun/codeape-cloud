package com.code.ape.codeape.admin.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * 用户、科室关系
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-19 20:32:04
 */
@Data
@TableName("sys_user_dept")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "用户、科室关系")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SysUserDept extends BaseEntity {

    /**
     * userId
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="userId")
    private Long userId;

    /**
     * deptId
     */
    @Schema(description ="deptId")
    private Long deptId;

    /**
     * 0-正常，1-删除
     */
    @Schema(description ="0-正常，1-删除")
    private String delFlag;

}
