
package com.code.ape.codeape.admin.api.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 检测预警
 *
 * @author pig code generator
 * @date 2023-06-11 14:21:13
 */
@Data
@Schema(description = "检测预警")
public class SysWarnVO{

    /**
     * id
     */
    @Schema(description ="id")
    private Long id;

    /**
     * 0 院外,1 院内, 2 门诊 ,3 体检
     */
    @Schema(description ="0 院外,1 院内, 2 门诊 ,3 体检")
    private Integer inHos;

    /**
     * 预警高值
     */
    @Schema(description ="预警高值")
    private Float valueUp;

    /**
     * 预警低值
     */
    @Schema(description ="预警低值")
    private Float valueDown;

    /**
     * 1 血糖 2 尿酸 3 血酮
     */
    @Schema(description ="1 血糖 2 尿酸 3 血酮")
    private Integer model;

    /**
     * 性别，来源字典 用于尿酸
     */
    @Schema(description ="性别，来源字典 用于尿酸")
    private String gender;
}
