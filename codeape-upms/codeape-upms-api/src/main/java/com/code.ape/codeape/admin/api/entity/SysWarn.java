
package com.code.ape.codeape.admin.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 检测预警
 *
 * @author pig code generator
 * @date 2023-06-11 14:21:13
 */
@Data
@TableName("sys_warn")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "检测预警")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SysWarn extends BaseEntity {

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="id")
    private Long id;

    /**
     * 医院Id
     */
    @Schema(description ="医院Id")
    private Long hosId;

    /**
     * 0 院内,1 院外, 2 门诊 ,3 体检
     */
    @Schema(description ="0 院内,1 院外, 2 门诊 ,3 体检")
    private Integer inHos;

    /**
     * 预警高值
     */
    @Schema(description ="预警高值")
    private Float valueUp;

    /**
     * 预警低值
     */
    @Schema(description ="预警低值")
    private Float valueDown;

    /**
     * 1 血糖 2 尿酸 3 血酮
     */
    @Schema(description ="1 血糖 2 尿酸 3 血酮")
    private Integer model;

    /**
     * 性别，来源字典 用于尿酸
     */
    @Schema(description ="性别，来源字典 用于尿酸")
    private String gender;

    /**
     * 0-正常，1-删除
     */
    @Schema(description ="0-正常，1-删除")
	@TableLogic
    private String delFlag;


}
