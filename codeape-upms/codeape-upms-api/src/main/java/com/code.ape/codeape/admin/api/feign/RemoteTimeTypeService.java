package com.code.ape.codeape.admin.api.feign;

import com.code.ape.codeape.admin.api.entity.SysTimeType;
import com.code.ape.codeape.common.core.constant.SecurityConstants;
import com.code.ape.codeape.common.core.constant.ServiceNameConstants;
import com.code.ape.codeape.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description  检测时间段feign接口
 */
@FeignClient(contextId = "remoteSysTimeService", value = ServiceNameConstants.UMPS_SERVICE)
public interface RemoteTimeTypeService {

	@GetMapping(value = "/systimetype/by/model" ,headers = SecurityConstants.HEADER_FROM_IN)
	R<List<SysTimeType>> getSysTimeTypePage(@RequestParam(value = "model") String model, @RequestParam(value = "hosId")Long hosId);
}
