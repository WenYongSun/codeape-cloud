package com.code.ape.codeape.admin.api.entity.constant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description  检测预警使用方枚举
 */
@Getter
@RequiredArgsConstructor
public enum SysWarnInHosEnum {
	IN_HOS(0,"院内"),

	OUT_HOS(1,"院外"),

	MZ(2,"门诊"),

	PHYSICAL(3,"体检");

	private final Integer value;

	private final String description;
}
