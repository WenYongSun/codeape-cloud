

package com.code.ape.codeape.admin.api.feign;

import com.code.ape.codeape.admin.api.vo.SysWarnVO;
import com.code.ape.codeape.common.core.constant.SecurityConstants;
import com.code.ape.codeape.common.core.constant.ServiceNameConstants;
import com.code.ape.codeape.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author hccake
 */
@FeignClient(contextId = "remoteSysWarnService", value = ServiceNameConstants.UMPS_SERVICE)
public interface RemoteSysWarnService {

	/**
	 * 根据医院Id查询预警配置
	 * @param hosId 医院ID
	 * @return {@link SysWarnVO}
	 */
	@GetMapping(value = "/warn/hos/{hosId}", headers = SecurityConstants.HEADER_FROM_IN)
	R<List<SysWarnVO>> getSysWarnByHosId(@PathVariable("hosId") Long hosId);
}
