package com.code.ape.codeape.admin.api.feign;

import com.code.ape.codeape.admin.api.entity.SysHospital;
import com.code.ape.codeape.common.core.constant.SecurityConstants;
import com.code.ape.codeape.common.core.constant.ServiceNameConstants;
import com.code.ape.codeape.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description  医院的的feign接口
 */
@FeignClient(contextId = "remoteSysHospitalService", value = ServiceNameConstants.UMPS_SERVICE)
public interface RemoteSysHospitalService {

	@GetMapping(value = "/syshospital/hos/all",headers = SecurityConstants.HEADER_FROM_IN)
	R<List<SysHospital>> getAllSysHospital();
}
