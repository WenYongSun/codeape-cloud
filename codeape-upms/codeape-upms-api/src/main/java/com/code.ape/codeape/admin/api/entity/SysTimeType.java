
package com.code.ape.codeape.admin.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 检测时段
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023-05-22 11:14:25
 */
@Data
@TableName("sys_time_type")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "检测时段")
public class SysTimeType extends BaseEntity {

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="id")
    private Long id;

    /**
     * 医院id
     */
    @Schema(description ="医院id")
    private Long hosId;

    /**
     * 时间段名称
     */
    @Schema(description ="时间段名称")
    private String name;

    /**
     * 时间段范围文字描述02:00-04:00。强化字段及随机检测为空
     */
    @Schema(description ="时间段范围文字描述02:00-04:00。强化字段及随机检测为空")
    private String timeScope;

    /**
     * 开始时间04:30表示4点30
     */
    @Schema(description ="开始时间04:30表示4点30")
    private String startTime;

    /**
     * 结束时间06:00表示6点00
     */
    @Schema(description ="结束时间06:00表示6点00")
    private String endTime;

    /**
     * 血糖目标最低
     */
    @Schema(description ="血糖目标最低")
    private Float valueDown;

    /**
     * 血糖目标最高
     */
    @Schema(description ="血糖目标最高")
    private Float valueUp;

    /**
     * 排序号
     */
    @Schema(description ="排序号")
    private Integer priority;

    /**
     * 备注
     */
    @Schema(description ="备注")
    private String remark;

    /**
     * 1普通时段,0强化时段
     */
    @Schema(description ="1普通时段,0强化时段")
    private String type;

    /**
     * 1餐前,0 餐后
     */
    @Schema(description ="1餐前,0 餐后")
    private Integer mealTimeType;

    /**
     * 0 院外,1 院内, 2 门诊 ,3 体检
     */
    @Schema(description ="0 院外,1 院内, 2 门诊 ,3 体检")
    private String inHos;

    @Schema(description = "删除状态")
	@TableLogic
    private String delFlag;


}
