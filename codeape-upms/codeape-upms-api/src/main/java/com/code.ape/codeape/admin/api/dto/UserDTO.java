

package com.code.ape.codeape.admin.api.dto;

import com.code.ape.codeape.common.core.entity.BaseParam;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2019/2/1
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UserDTO extends BaseParam {

	/**
	 * 角色ID
	 */
	private List<Long> role;

	private Long deptId;

	/**
	 * 岗位ID
	 */
	private List<Long> post;

	/**
	 * 新密码
	 */
	private String newpassword1;


	/**
	 * 验证码
	 */
	private String code;

	/**
	 * 科室权限
	 */
	private List<Long> auth;

	/**
	 * 主键ID
	 */
	private Long userId;

	@Schema(title = "医院Id")
	private Long hosId;


	/**
	 * 用户名
	 */
	private String username;

	private String gender;

	private String name;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * 随机盐
	 */
	@JsonIgnore
	private String salt;

	/**
	 * 锁定标记
	 */
	private String lockFlag;

	/**
	 * 手机号
	 */
	private String phone;

	/**
	 * 头像
	 */
	private String avatar;


	/**
	 * 0-正常，1-删除
	 */
	private String delFlag;
}
