

package com.code.ape.codeape.admin.api.feign;

import com.code.ape.codeape.common.core.constant.SecurityConstants;
import com.code.ape.codeape.common.core.constant.ServiceNameConstants;
import com.code.ape.codeape.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2019/2/1
 */
@FeignClient(contextId = "remoteTokenService", value = ServiceNameConstants.AUTH_SERVICE)
public interface RemoteTokenService {

	/**
	 * 分页查询token 信息
	 * @param params 分页参数
	 * @return page
	 */
	@PostMapping(value = "/token/page", headers = SecurityConstants.HEADER_FROM_IN)
	R getTokenPage(@RequestBody Map<String, Object> params);

	/**
	 * 删除token
	 * @param token token
	 * @return
	 */
	@DeleteMapping(value = "/token/{token}", headers = SecurityConstants.HEADER_FROM_IN)
	R<Boolean> removeToken(@PathVariable("token") String token);

}
