

package com.code.ape.codeape.admin.api.dto;

import com.code.ape.codeape.admin.api.entity.SysHospital;
import com.code.ape.codeape.common.core.entity.BaseParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2019/2/1
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysHospitalDTO extends BaseParam {

	/**
	 * id
	 */
	@NotNull(message = "医院Id不能为空",groups = {SysHospitalUpdate.class,SysHospitalDelete.class})
	@Schema(description ="ID")
	private Long id;

	/**
	 * 医院名称
	 */
	@NotBlank(message = "医院名称不能为空",groups = {SysHospitalADD.class})
	@Schema(description ="医院名称")
	private String name;


	@NotBlank(message = "管理员账号不能为空",groups = {SysHospitalADD.class})
	@Schema(description = "管理员账号")
	private String username;

	/**
	 * 医院地址
	 */
	@NotBlank(message = "医院地址不能为空",groups = {SysHospitalADD.class})
	@Schema(description ="医院地址")
	private String address;

	/**
	 * 医院等级
	 */
	@NotBlank(message = "医院等级不能为空",groups = {SysHospitalADD.class})
	@Schema(description ="医院等级")
	private String level;

	/**
	 * 联系人
	 */
	@NotBlank(message = "联系人不能为空",groups = {SysHospitalADD.class})
	@Schema(description ="联系人")
	private String contacts;

	/**
	 * 联系人电话
	 */
	@NotBlank(message = "联系人电话不能为空",groups = {SysHospitalADD.class})
	@Schema(description ="联系人电话")
	private String contactsPhone;


	public static interface SysHospitalADD{}

	public static interface SysHospitalUpdate{}

	public static interface SysHospitalDelete{}

}
