/*
 Navicat Premium Data Transfer

 Source Server         : codeape-mysql
 Source Server Type    : MySQL
 Source Server Version : 50742
 Source Host           : codeape-mysql:3306
 Source Schema         : codeape

 Target Server Type    : MySQL
 Target Server Version : 50742
 File Encoding         : 65001

 Date: 11/06/2023 11:16:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for flyway_history
-- ----------------------------
DROP TABLE IF EXISTS `flyway_history`;
CREATE TABLE `flyway_history`  (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `script` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `checksum` int(11) NULL DEFAULT NULL,
  `installed_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `installed_on` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`) USING BTREE,
  INDEX `flyway_history_s_idx`(`success`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flyway_history
-- ----------------------------
INSERT INTO `flyway_history` VALUES (1, '1.0.0', 'init codeape', 'SQL', 'V1.0.0__init_codeape.sql', -457850757, 'root', '2023-06-11 10:48:46', 5667, 1);

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '部门名称',
  `his_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'his系统中的ID',
  `hos_id` bigint(20) NOT NULL COMMENT '医院的唯一ID',
  `sort_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序值',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标记  -1：已删除  0：正常',
  `parent_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '父部门ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '部门管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1659386149638709250, '内分泌科室', NULL, 1659018792143663105, 999, '1', '0', '2023-05-19 10:31:03', 'admin', '2023-05-20 13:06:43', 'admin');
INSERT INTO `sys_dept` VALUES (1659386264763965441, '神经内科', NULL, 1659018792143663105, 999, '0', '0', '2023-05-19 10:31:30', 'admin', '2023-05-19 10:31:49', 'admin');
INSERT INTO `sys_dept` VALUES (1659386542775017473, '内分泌科', NULL, 1659018792143663105, 999, '0', '0', '2023-05-19 10:32:36', 'admin', '2023-05-19 10:32:36', 'admin');
INSERT INTO `sys_dept` VALUES (1659787907582017538, '内分泌护理单元', NULL, 1659018792143663105, 999, '1', '1659386542775017473', '2023-05-20 13:07:29', 'admin', '2023-05-20 13:15:01', 'admin');
INSERT INTO `sys_dept` VALUES (1659789309465214978, '心内科', NULL, 1659018792143663105, 999, '0', '0', '2023-05-20 13:13:03', 'admin', '2023-05-20 13:13:03', 'admin');
INSERT INTO `sys_dept` VALUES (1659789383939276802, '急诊科', NULL, 1659018792143663105, 999, '0', '0', '2023-05-20 13:13:21', 'admin', '2023-05-20 13:13:21', 'admin');
INSERT INTO `sys_dept` VALUES (1659790789526044673, '内分泌护理单元', NULL, 1659018792143663105, 999, '0', '1659386542775017473', '2023-05-19 10:31:30', 'admin', '2023-05-19 10:31:49', 'admin');
INSERT INTO `sys_dept` VALUES (1659799651851362305, '心内科护理单元', NULL, 1659018792143663105, 999, '0', '1659789309465214978', '2023-05-20 13:54:09', 'admin', '2023-05-20 13:54:09', 'admin');
INSERT INTO `sys_dept` VALUES (1660162973545213954, '内分泌', NULL, 1660133100613181442, 999, '0', '0', '2023-05-21 13:57:52', 'admin', '2023-05-21 13:57:52', 'admin');
INSERT INTO `sys_dept` VALUES (1660163024992546818, '心血管内科', NULL, 1660133100613181442, 999, '0', '0', '2023-05-21 13:58:04', 'admin', '2023-05-21 13:58:04', 'admin');
INSERT INTO `sys_dept` VALUES (1660167857690566657, '神经内科', NULL, 1660133100613181442, 999, '0', '0', '2023-05-21 14:17:16', 'zjszyy', '2023-05-21 14:17:16', 'zjszyy');
INSERT INTO `sys_dept` VALUES (1660167897876193281, '肿瘤科', NULL, 1660133100613181442, 999, '0', '0', '2023-05-21 14:17:26', 'zjszyy', '2023-05-21 14:17:26', 'zjszyy');

-- ----------------------------
-- Table structure for sys_dept_relation
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept_relation`;
CREATE TABLE `sys_dept_relation`  (
  `ancestor` bigint(20) NOT NULL COMMENT '祖先节点',
  `descendant` bigint(20) NOT NULL COMMENT '后代节点',
  PRIMARY KEY (`ancestor`, `descendant`) USING BTREE,
  INDEX `idx1`(`ancestor`) USING BTREE,
  INDEX `idx2`(`descendant`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '部门关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept_relation
-- ----------------------------
INSERT INTO `sys_dept_relation` VALUES (1659790789526044673, 1659790789526044673);
INSERT INTO `sys_dept_relation` VALUES (1659799651851362305, 1659799651851362305);
INSERT INTO `sys_dept_relation` VALUES (1660162973545213954, 1660162973545213954);
INSERT INTO `sys_dept_relation` VALUES (1660163024992546818, 1660163024992546818);
INSERT INTO `sys_dept_relation` VALUES (1660167857690566657, 1660167857690566657);
INSERT INTO `sys_dept_relation` VALUES (1660167897876193281, 1660167897876193281);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` bigint(20) NOT NULL,
  `dict_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标识',
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '描述',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `system_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '是否是系统内置',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标记',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sys_dict_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '字典表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, 'dict_type', '字典类型', NULL, '0', '0', '2019-05-16 14:16:20', '', 'admin', '2021-12-29 12:29:18');
INSERT INTO `sys_dict` VALUES (2, 'log_type', '日志类型', NULL, '0', '0', '2020-03-13 14:21:01', '', 'admin', '2021-12-29 12:30:14');
INSERT INTO `sys_dict` VALUES (3, 'ds_type', '驱动类型', NULL, '0', '0', '2021-10-15 16:24:35', '', 'admin', '2021-12-29 12:30:18');
INSERT INTO `sys_dict` VALUES (4, 'param_type', '参数配置', '检索、原文、报表、安全、文档、消息、其他', '1', '0', '2022-03-25 20:51:26', 'admin', 'admin', '2022-03-25 20:51:26');
INSERT INTO `sys_dict` VALUES (5, 'status_type', '租户状态', '租户状态', '1', '0', '2022-03-25 20:56:51', 'admin', 'admin', '2022-03-25 20:56:51');
INSERT INTO `sys_dict` VALUES (6, 'menu_type_status', '菜单类型', NULL, '1', '0', '2022-09-18 17:12:05', 'admin', 'admin', '2022-09-18 17:12:05');
INSERT INTO `sys_dict` VALUES (7, 'dict_css_type', '字典项展示样式', NULL, '1', '0', '2022-09-28 21:37:23', 'admin', 'admin', '2022-09-28 21:37:23');
INSERT INTO `sys_dict` VALUES (8, 'keepalive_status', '菜单是否开启缓冲', NULL, '1', '0', '2022-09-28 21:46:12', 'admin', 'admin', '2022-09-28 21:46:12');
INSERT INTO `sys_dict` VALUES (9, 'user_lock_flag', '用户锁定标记', NULL, '1', '0', '2022-09-28 21:51:39', 'admin', 'admin', '2022-09-28 21:51:39');
INSERT INTO `sys_dict` VALUES (1659810149162504194, 'sex_type', '性别字典', '', '0', '0', '2023-05-20 14:35:52', 'admin', 'admin', '2023-05-20 14:35:52');
INSERT INTO `sys_dict` VALUES (1660489191679275010, 'blood_time_type', '时段类型', '', '0', '0', '2023-05-22 11:34:08', 'admin', 'admin', '2023-05-22 11:34:08');
INSERT INTO `sys_dict` VALUES (1660490998769987586, 'blood_time_model', '时段模块', '院内、院外、门诊、体检', '0', '0', '2023-05-22 11:41:19', 'admin', 'admin', '2023-05-22 11:41:44');
INSERT INTO `sys_dict` VALUES (1661610864541384705, 'blood_report_check', '血糖报告默认勾选', '血糖报告默认勾选：常规血糖  强化血糖  检测时间  操作人', '0', '0', '2023-05-25 13:51:16', 'admin', 'admin', '2023-05-25 13:52:22');
INSERT INTO `sys_dict` VALUES (1661612225085521921, 'blood_report_sort', '血糖报告显示排序', '血糖报告显示排序', '0', '0', '2023-05-25 13:56:40', 'admin', 'admin', '2023-05-25 13:56:40');
INSERT INTO `sys_dict` VALUES (1664969466052313089, 'diabetes_type', '糖尿病类型', '糖尿病类型', '0', '0', '2023-06-03 20:17:09', 'admin', 'admin', '2023-06-03 20:17:09');

-- ----------------------------
-- Table structure for sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item`  (
  `id` bigint(20) NOT NULL,
  `dict_id` bigint(20) NOT NULL COMMENT '字典ID',
  `dict_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '字典标识',
  `value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '值',
  `label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标签',
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '字典类型',
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '描述',
  `sort_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序（升序）',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT ' ' COMMENT '备注',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标记',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sys_dict_value`(`value`) USING BTREE,
  INDEX `sys_dict_label`(`label`) USING BTREE,
  INDEX `sys_dict_del_flag`(`del_flag`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '字典项' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_item
-- ----------------------------
INSERT INTO `sys_dict_item` VALUES (1, 1, 'dict_type', '1', '系统类', NULL, '系统类字典', 0, ' ', '0', '2019-05-16 14:20:40', NULL, NULL, '2019-05-16 14:20:40');
INSERT INTO `sys_dict_item` VALUES (2, 1, 'dict_type', '0', '业务类', NULL, '业务类字典', 0, ' ', '0', '2019-05-16 14:20:59', NULL, NULL, '2019-05-16 14:20:59');
INSERT INTO `sys_dict_item` VALUES (3, 2, 'log_type', '0', '正常', NULL, '正常', 0, ' ', '0', '2020-03-13 14:23:22', NULL, NULL, '2020-03-13 14:23:22');
INSERT INTO `sys_dict_item` VALUES (4, 2, 'log_type', '9', '异常', NULL, '异常', 0, ' ', '0', '2020-03-13 14:23:35', NULL, NULL, '2020-03-13 14:23:35');
INSERT INTO `sys_dict_item` VALUES (5, 3, 'ds_type', 'com.mysql.cj.jdbc.Driver', 'MYSQL8', NULL, 'MYSQL8', 0, ' ', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (6, 3, 'ds_type', 'com.mysql.jdbc.Driver', 'MYSQL5', NULL, 'MYSQL5', 0, ' ', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (7, 3, 'ds_type', 'oracle.jdbc.OracleDriver', 'Oracle', NULL, 'Oracle', 0, ' ', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (8, 3, 'ds_type', 'org.mariadb.jdbc.Driver', 'mariadb', NULL, 'mariadb', 0, ' ', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (9, 3, 'ds_type', 'com.microsoft.sqlserver.jdbc.SQLServerDriver', 'sqlserver2005+', NULL, 'sqlserver2005+', 0, ' ', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (10, 3, 'ds_type', 'com.microsoft.jdbc.sqlserver.SQLServerDriver', 'sqlserver2000', NULL, 'sqlserver2000', 0, ' ', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (11, 3, 'ds_type', 'com.ibm.db2.jcc.DB2Driver', 'db2', NULL, 'db2', 0, ' ', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (12, 3, 'ds_type', 'org.postgresql.Driver', 'postgresql', NULL, 'postgresql', 0, ' ', '0', NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (13, 4, 'param_type', '1', '检索', NULL, '检索', 0, '检索', '0', '2022-03-25 20:51:51', 'admin', 'admin', '2022-03-25 20:51:51');
INSERT INTO `sys_dict_item` VALUES (14, 4, 'param_type', '2', '原文', NULL, '原文', 1, '原文', '0', '2022-03-25 20:52:06', 'admin', 'admin', '2022-03-25 20:52:06');
INSERT INTO `sys_dict_item` VALUES (15, 4, 'param_type', '3', '报表', NULL, '报表', 2, '报表', '0', '2022-03-25 20:52:16', 'admin', 'admin', '2022-03-25 20:52:16');
INSERT INTO `sys_dict_item` VALUES (16, 4, 'param_type', '4', '安全', NULL, '安全', 3, '安全', '0', '2022-03-25 20:52:32', 'admin', 'admin', '2022-03-25 20:52:32');
INSERT INTO `sys_dict_item` VALUES (17, 4, 'param_type', '5', '文档', NULL, '文档', 4, '文档', '0', '2022-03-25 20:52:52', 'admin', 'admin', '2022-03-25 20:52:52');
INSERT INTO `sys_dict_item` VALUES (18, 4, 'param_type', '6', '消息', NULL, '消息', 5, '消息', '0', '2022-03-25 20:53:07', 'admin', 'admin', '2022-03-25 20:53:07');
INSERT INTO `sys_dict_item` VALUES (19, 4, 'param_type', '9', '其他', NULL, '其他', 6, '其他', '0', '2022-03-25 20:54:50', 'admin', 'admin', '2022-03-25 20:54:50');
INSERT INTO `sys_dict_item` VALUES (20, 4, 'param_type', '0', '默认', NULL, '默认', 7, '默认', '0', '2022-03-25 20:55:23', 'admin', 'admin', '2022-03-25 20:55:23');
INSERT INTO `sys_dict_item` VALUES (21, 5, 'status_type', '0', '正常', NULL, '状态正常', 0, '状态正常', '0', '2022-03-25 20:57:12', 'admin', 'admin', '2022-03-25 20:57:12');
INSERT INTO `sys_dict_item` VALUES (22, 5, 'status_type', '9', '冻结', NULL, '状态冻结', 1, '状态冻结', '0', '2022-03-25 20:57:34', 'admin', 'admin', '2022-03-25 20:57:34');
INSERT INTO `sys_dict_item` VALUES (23, 6, 'menu_type_status', '0', '菜单', NULL, '菜单', 0, ' ', '0', '2022-09-18 17:15:52', 'admin', 'admin', '2022-09-18 17:15:52');
INSERT INTO `sys_dict_item` VALUES (24, 6, 'menu_type_status', '1', '按钮', 'success', '按钮', 1, ' ', '0', '2022-09-18 17:16:06', 'admin', 'admin', '2022-09-18 17:16:06');
INSERT INTO `sys_dict_item` VALUES (25, 7, 'dict_css_type', 'success', 'success', 'success', 'success', 2, ' ', '0', '2022-09-28 21:41:49', 'admin', 'admin', '2022-09-28 21:41:49');
INSERT INTO `sys_dict_item` VALUES (26, 7, 'dict_css_type', 'info', 'info', 'info', 'info', 3, ' ', '0', '2022-09-28 21:41:59', 'admin', 'admin', '2022-09-28 21:41:59');
INSERT INTO `sys_dict_item` VALUES (27, 7, 'dict_css_type', 'warning', 'warning', 'warning', 'warning', 4, ' ', '0', '2022-09-28 21:42:09', 'admin', 'admin', '2022-09-28 21:42:09');
INSERT INTO `sys_dict_item` VALUES (28, 7, 'dict_css_type', 'danger', 'danger', 'danger', 'danger', 5, ' ', '0', '2022-09-28 21:42:19', 'admin', 'admin', '2022-09-28 21:42:19');
INSERT INTO `sys_dict_item` VALUES (29, 8, 'keepalive_status', '0', '否', 'info', '不开启缓冲', 0, ' ', '0', '2022-09-28 21:46:32', 'admin', 'admin', '2022-09-28 21:46:32');
INSERT INTO `sys_dict_item` VALUES (30, 8, 'keepalive_status', '1', '是', NULL, '开启缓冲', 1, ' ', '0', '2022-09-28 21:46:54', 'admin', 'admin', '2022-09-28 21:46:54');
INSERT INTO `sys_dict_item` VALUES (31, 9, 'user_lock_flag', '0', '正常', NULL, '正常状态', 0, ' ', '0', '2022-09-28 21:51:55', 'admin', 'admin', '2022-09-28 21:51:55');
INSERT INTO `sys_dict_item` VALUES (32, 9, 'user_lock_flag', '9', '锁定', 'info', '已锁定', 9, ' ', '0', '2022-09-28 21:52:13', 'admin', 'admin', '2022-09-28 21:52:13');
INSERT INTO `sys_dict_item` VALUES (1659810661937139714, 1659810149162504194, 'sex_type', '1', '男', '', '性别：男', 1, '', '0', '2023-05-20 14:37:54', 'admin', 'admin', '2023-05-20 14:37:54');
INSERT INTO `sys_dict_item` VALUES (1659810737384280066, 1659810149162504194, 'sex_type', '2', '女', '', '性别：女', 2, '', '0', '2023-05-20 14:38:12', 'admin', 'admin', '2023-05-20 14:38:12');
INSERT INTO `sys_dict_item` VALUES (1659810812374241282, 1659810149162504194, 'sex_type', '3', '其他', '', '性别：未知', 3, '', '0', '2023-05-20 14:38:30', 'admin', 'admin', '2023-05-20 14:38:30');
INSERT INTO `sys_dict_item` VALUES (1660489368251084801, 1660489191679275010, 'blood_time_type', '1', '普通时段', '', '普通时段', 1, '', '0', '2023-05-22 11:34:50', 'admin', 'admin', '2023-05-22 11:39:40');
INSERT INTO `sys_dict_item` VALUES (1660489470369804290, 1660489191679275010, 'blood_time_type', '0', '强化时段', '', '强化时段', 2, '', '0', '2023-05-22 11:35:15', 'admin', 'admin', '2023-05-22 11:39:45');
INSERT INTO `sys_dict_item` VALUES (1660491190676172802, 1660490998769987586, 'blood_time_model', '0', '院外', '', '院外', 1, '', '0', '2023-05-22 11:42:05', 'admin', 'admin', '2023-05-22 11:42:05');
INSERT INTO `sys_dict_item` VALUES (1660491231285424130, 1660490998769987586, 'blood_time_model', '1', '院内', '', '院内', 2, '', '0', '2023-05-22 11:42:15', 'admin', 'admin', '2023-05-22 11:42:15');
INSERT INTO `sys_dict_item` VALUES (1660491290131509249, 1660490998769987586, 'blood_time_model', '2', '门诊', '', '门诊', 3, '', '0', '2023-05-22 11:42:29', 'admin', 'admin', '2023-05-22 11:42:29');
INSERT INTO `sys_dict_item` VALUES (1660491375288463361, 1660490998769987586, 'blood_time_model', '3', '体检', '', '体检', 4, '', '0', '2023-05-22 11:42:49', 'admin', 'admin', '2023-05-22 11:42:49');
INSERT INTO `sys_dict_item` VALUES (1661611219257868290, 1661610864541384705, 'blood_report_check', '1', '常规血糖', '', '常规血糖', 1, '', '0', '2023-05-25 13:52:41', 'admin', 'admin', '2023-05-25 13:52:41');
INSERT INTO `sys_dict_item` VALUES (1661611265214857218, 1661610864541384705, 'blood_report_check', '2', '强化血糖', '', '强化血糖', 2, '', '0', '2023-05-25 13:52:52', 'admin', 'admin', '2023-05-25 13:52:52');
INSERT INTO `sys_dict_item` VALUES (1661611326116151297, 1661610864541384705, 'blood_report_check', '3', '检测时间', '', '检测时间', 3, '', '0', '2023-05-25 13:53:06', 'admin', 'admin', '2023-05-25 13:53:06');
INSERT INTO `sys_dict_item` VALUES (1661611459289497602, 1661610864541384705, 'blood_report_check', '4', '操作人', '', '操作人', 4, '', '0', '2023-05-25 13:53:38', 'admin', 'admin', '2023-05-25 13:53:38');
INSERT INTO `sys_dict_item` VALUES (1661612307147079682, 1661612225085521921, 'blood_report_sort', '1', '正序', '', '正序', 1, '', '0', '2023-05-25 13:57:00', 'admin', 'admin', '2023-05-25 13:57:00');
INSERT INTO `sys_dict_item` VALUES (1661612365280133121, 1661612225085521921, 'blood_report_sort', '2', '倒序', '', '倒序', 2, '', '0', '2023-05-25 13:57:14', 'admin', 'admin', '2023-05-25 13:57:14');
INSERT INTO `sys_dict_item` VALUES (1664969694209867778, 1664969466052313089, 'diabetes_type', '1', 'I型', '', 'I型糖尿病', 1, '', '0', '2023-06-03 20:18:03', 'admin', 'admin', '2023-06-03 20:18:03');
INSERT INTO `sys_dict_item` VALUES (1664969789286350849, 1664969466052313089, 'diabetes_type', '2', 'II型', '', 'II型糖尿病', 2, '', '0', '2023-06-03 20:18:26', 'admin', 'admin', '2023-06-03 20:18:26');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`  (
  `id` bigint(20) NOT NULL COMMENT '文件ID',
  `file_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件名称',
  `bucket_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件存储桶名称',
  `original` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '原始文件名',
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件类型',
  `file_size` bigint(20) NULL DEFAULT NULL COMMENT '文件大小',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志：0-正常，1-删除',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '文件管理表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_hos_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_hos_config`;
CREATE TABLE `sys_hos_config`  (
  `id` bigint(20) NOT NULL,
  `hos_id` bigint(20) NULL DEFAULT NULL COMMENT '医院ID',
  `blood_report_check` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '血糖报告默认勾选',
  `blood_report_sort` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '血糖报告排序',
  `blood_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_hospital
-- ----------------------------
DROP TABLE IF EXISTS `sys_hospital`;
CREATE TABLE `sys_hospital`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '医院名称',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员账号',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医院地址',
  `level` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医院等级',
  `contacts` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `contacts_phone` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人电话',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标记',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '医院表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_hospital
-- ----------------------------
INSERT INTO `sys_hospital` VALUES (1659018792143663105, '浙江第一附属医院', 'zjdy', '浙江省杭州市', '三级甲等', '陈斌', '18796327106', '0', '2023-05-18 10:11:18', 'admin', 'admin', '2023-05-18 10:11:18');
INSERT INTO `sys_hospital` VALUES (1660133100613181442, '杭州市中医院', 'zjszyy', '浙江省杭州市上城区', '三级甲等', '陈斌', '18796327106', '0', '2023-05-21 11:59:10', 'admin', 'admin', '2023-05-21 11:59:10');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(20) NOT NULL,
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '1' COMMENT '日志类型',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '日志标题',
  `service_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '服务ID',
  `remote_addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '操作IP地址',
  `user_agent` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户代理',
  `request_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '请求URI',
  `method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '操作方式',
  `params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '操作提交的数据',
  `time` bigint(20) NULL DEFAULT NULL COMMENT '执行时间',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标记',
  `exception` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sys_log_create_by`(`create_by`) USING BTREE,
  INDEX `sys_log_request_uri`(`request_uri`) USING BTREE,
  INDEX `sys_log_type`(`type`) USING BTREE,
  INDEX `sys_log_create_date`(`create_time`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单名称',
  `permission` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '菜单权限标识',
  `path` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '前端URL',
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父菜单ID',
  `icon` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '图标',
  `sort_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序值',
  `keep_alive` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '0-开启，1- 关闭',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '菜单类型 （0菜单 1按钮）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '逻辑删除标记(0--正常 1--删除)',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1000, '基础设置', NULL, '/admin', -1, 'icon-quanxianguanli', 1, '0', '0', '0', '', '2018-09-28 08:29:53', 'admin', '2023-05-31 13:08:20');
INSERT INTO `sys_menu` VALUES (1100, '用户管理', NULL, '/admin/user/index', 1000, 'icon-yonghuguanli', 0, '0', '0', '0', ' ', '2017-11-02 22:24:37', ' ', '2020-03-12 00:12:57');
INSERT INTO `sys_menu` VALUES (1101, '用户新增', 'sys_user_add', NULL, 1100, NULL, 0, '0', '1', '0', ' ', '2017-11-08 09:52:09', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (1102, '用户修改', 'sys_user_edit', NULL, 1100, NULL, 0, '0', '1', '0', ' ', '2017-11-08 09:52:48', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (1103, '用户删除', 'sys_user_del', NULL, 1100, NULL, 0, '0', '1', '0', ' ', '2017-11-08 09:54:01', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (1104, '导入导出', 'sys_user_import_export', NULL, 1100, NULL, 0, '0', '1', '0', ' ', '2017-11-08 09:54:01', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (1200, '菜单管理', NULL, '/admin/menu/index', 1000, 'icon-caidanguanli', 1, '0', '0', '0', ' ', '2017-11-08 09:57:27', ' ', '2020-03-12 00:13:52');
INSERT INTO `sys_menu` VALUES (1201, '菜单新增', 'sys_menu_add', NULL, 1200, NULL, 0, '0', '1', '0', ' ', '2017-11-08 10:15:53', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (1202, '菜单修改', 'sys_menu_edit', NULL, 1200, NULL, 0, '0', '1', '0', ' ', '2017-11-08 10:16:23', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (1203, '菜单删除', 'sys_menu_del', NULL, 1200, NULL, 0, '0', '1', '0', ' ', '2017-11-08 10:16:43', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (1300, '角色管理', NULL, '/admin/role/index', 1000, 'icon-jiaoseguanli', 2, '0', '0', '0', ' ', '2017-11-08 10:13:37', ' ', '2020-03-12 00:15:40');
INSERT INTO `sys_menu` VALUES (1301, '角色新增', 'sys_role_add', NULL, 1300, NULL, 0, '0', '1', '0', ' ', '2017-11-08 10:14:18', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (1302, '角色修改', 'sys_role_edit', NULL, 1300, NULL, 0, '0', '1', '0', ' ', '2017-11-08 10:14:41', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (1303, '角色删除', 'sys_role_del', NULL, 1300, NULL, 0, '0', '1', '0', ' ', '2017-11-08 10:14:59', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (1304, '分配权限', 'sys_role_perm', NULL, 1300, NULL, 0, '0', '1', '0', ' ', '2018-04-20 07:22:55', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (1305, '导入导出', 'sys_role_import_export', NULL, 1300, NULL, 0, '0', '1', '0', 'admin', '2022-03-21 11:14:52', 'admin', '2022-03-21 11:15:07');
INSERT INTO `sys_menu` VALUES (1400, '科室管理', NULL, '/admin/dept/index', 1000, 'icon-web-icon-', 3, '0', '0', '0', '', '2018-01-20 13:17:19', 'admin', '2023-05-20 13:15:59');
INSERT INTO `sys_menu` VALUES (1401, '科室新增', 'sys_dept_add', NULL, 1400, NULL, 0, '0', '1', '0', '', '2018-01-20 14:56:16', 'admin', '2023-05-20 13:16:08');
INSERT INTO `sys_menu` VALUES (1402, '科室修改', 'sys_dept_edit', NULL, 1400, NULL, 0, '0', '1', '0', '', '2018-01-20 14:56:59', 'admin', '2023-05-20 13:16:18');
INSERT INTO `sys_menu` VALUES (1403, '科室删除', 'sys_dept_del', NULL, 1400, NULL, 0, '0', '1', '0', '', '2018-01-20 14:57:28', 'admin', '2023-05-20 13:16:24');
INSERT INTO `sys_menu` VALUES (1500, '岗位管理', '', '/admin/post/index', 1000, 'icon-gangweiguanli', 4, '0', '0', '0', NULL, '2018-01-20 13:17:19', 'admin', '2022-11-10 21:35:55');
INSERT INTO `sys_menu` VALUES (1501, '岗位查看', 'sys_post_get', NULL, 1500, '1', 0, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2022-03-15 17:32:54');
INSERT INTO `sys_menu` VALUES (1502, '岗位新增', 'sys_post_add', NULL, 1500, '1', 1, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2022-03-15 17:32:48');
INSERT INTO `sys_menu` VALUES (1503, '岗位修改', 'sys_post_edit', NULL, 1500, '1', 2, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2022-03-15 17:33:10');
INSERT INTO `sys_menu` VALUES (1504, '岗位删除', 'sys_post_del', NULL, 1500, '1', 3, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2022-03-15 17:33:27');
INSERT INTO `sys_menu` VALUES (1505, '导入导出', 'sys_post_import_export', NULL, 1500, NULL, 4, '0', '1', '0', 'admin', '2022-03-21 12:53:05', 'admin', '2022-03-21 12:53:05');
INSERT INTO `sys_menu` VALUES (2000, '系统管理', NULL, '/setting', -1, 'icon-wxbgongju1', 2, '0', '0', '0', '', '2017-11-07 20:56:00', 'admin', '2022-11-10 21:42:39');
INSERT INTO `sys_menu` VALUES (2100, '日志管理', NULL, '/admin/log/index', 2000, 'icon-rizhiguanli', 3, '0', '0', '0', ' ', '2017-11-20 14:06:22', ' ', '2020-03-12 00:15:49');
INSERT INTO `sys_menu` VALUES (2101, '日志删除', 'sys_log_del', NULL, 2100, NULL, 0, '0', '1', '0', ' ', '2017-11-20 20:37:37', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (2102, '导入导出', 'sys_log_import_export', NULL, 2100, NULL, 0, '0', '1', '0', ' ', '2017-11-08 09:54:01', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (2200, '字典管理', NULL, '/admin/dict/index', 2000, 'icon-tubiaozhizuomoban-27', 2, '0', '0', '0', '', '2017-11-29 11:30:52', 'admin', '2022-11-10 21:38:09');
INSERT INTO `sys_menu` VALUES (2201, '字典删除', 'sys_dict_del', NULL, 2200, NULL, 0, '0', '1', '0', ' ', '2017-11-29 11:30:11', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (2202, '字典新增', 'sys_dict_add', NULL, 2200, NULL, 0, '0', '1', '0', ' ', '2018-05-11 22:34:55', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (2203, '字典修改', 'sys_dict_edit', NULL, 2200, NULL, 0, '0', '1', '0', ' ', '2018-05-11 22:36:03', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (2300, '令牌管理', NULL, '/admin/token/index', 2000, 'icon-lingpaiguanli', 4, '0', '0', '0', '', '2018-09-04 05:58:41', 'admin', '2022-11-10 21:38:33');
INSERT INTO `sys_menu` VALUES (2301, '令牌删除', 'sys_token_del', NULL, 2300, NULL, 0, '0', '1', '0', ' ', '2018-09-04 05:59:50', ' ', '2020-03-13 12:57:34');
INSERT INTO `sys_menu` VALUES (2400, '终端管理', '', '/admin/client/index', 2000, 'icon-shouji', 0, '0', '0', '0', ' ', '2018-01-20 13:17:19', ' ', '2020-03-12 00:15:54');
INSERT INTO `sys_menu` VALUES (2401, '客户端新增', 'sys_client_add', NULL, 2400, '1', 0, '0', '1', '0', ' ', '2018-05-15 21:35:18', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (2402, '客户端修改', 'sys_client_edit', NULL, 2400, NULL, 0, '0', '1', '0', ' ', '2018-05-15 21:37:06', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (2403, '客户端删除', 'sys_client_del', NULL, 2400, NULL, 0, '0', '1', '0', ' ', '2018-05-15 21:39:16', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (2600, '文件管理', NULL, '/admin/file/index', 2000, 'icon-wenjianjiawenjianguanli', 1, '0', '0', '0', '', '2018-06-26 10:50:32', 'admin', '2022-11-10 21:35:40');
INSERT INTO `sys_menu` VALUES (2601, '文件删除', 'sys_file_del', NULL, 2600, NULL, 0, '0', '1', '0', ' ', '2017-11-29 11:30:11', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (2602, '文件新增', 'sys_file_add', NULL, 2600, NULL, 0, '0', '1', '0', ' ', '2018-05-11 22:34:55', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (2603, '文件修改', 'sys_file_edit', NULL, 2600, NULL, 0, '0', '1', '0', ' ', '2018-05-11 22:36:03', ' ', '2021-05-25 06:48:34');
INSERT INTO `sys_menu` VALUES (2700, '参数管理', NULL, '/admin/param/index', 2000, 'icon-canshu', 5, '0', '0', '0', 'admin', '2022-03-25 20:40:27', 'admin', '2022-11-10 21:38:50');
INSERT INTO `sys_menu` VALUES (2701, '参数新增', 'sys_publicparam_add', NULL, 2700, NULL, 0, '0', '1', '0', 'admin', '2022-03-25 20:45:05', 'admin', '2022-03-25 20:45:05');
INSERT INTO `sys_menu` VALUES (2702, '参数删除', 'sys_publicparam_del', NULL, 2700, NULL, 1, '0', '1', '0', 'admin', '2022-03-25 20:45:43', 'admin', '2022-03-25 20:45:43');
INSERT INTO `sys_menu` VALUES (2703, '参数修改', 'sys_publicparam_edit', NULL, 2700, NULL, 3, '0', '1', '0', 'admin', '2022-03-25 20:46:04', 'admin', '2022-03-25 20:46:04');
INSERT INTO `sys_menu` VALUES (3000, '开发平台', NULL, '/gen', -1, 'icon-keshihuapingtaiicon_zujian', 3, '1', '0', '0', '', '2020-03-11 22:15:40', 'admin', '2022-11-10 21:39:25');
INSERT INTO `sys_menu` VALUES (3100, '数据源管理', NULL, '/gen/datasource', 3000, 'icon-shujuyuan', 3, '1', '0', '0', '', '2020-03-11 22:17:05', 'admin', '2022-11-10 21:40:27');
INSERT INTO `sys_menu` VALUES (3200, '代码生成', NULL, '/gen/index', 3000, 'icon-didaima', 0, '0', '0', '0', '', '2020-03-11 22:23:42', 'admin', '2022-11-10 21:39:54');
INSERT INTO `sys_menu` VALUES (3300, '表单管理', NULL, '/gen/form', 3000, 'icon-biaodanguanli', 1, '1', '0', '0', '', '2020-03-11 22:19:32', 'admin', '2022-11-10 21:40:06');
INSERT INTO `sys_menu` VALUES (3301, '表单新增', 'gen_form_add', NULL, 3300, '', 0, '0', '1', '0', ' ', '2018-05-15 21:35:18', ' ', '2020-03-11 22:39:08');
INSERT INTO `sys_menu` VALUES (3302, '表单修改', 'gen_form_edit', NULL, 3300, '', 0, '0', '1', '0', ' ', '2018-05-15 21:35:18', ' ', '2020-03-11 22:39:09');
INSERT INTO `sys_menu` VALUES (3303, '表单删除', 'gen_form_del', NULL, 3300, '', 0, '0', '1', '0', ' ', '2018-05-15 21:35:18', ' ', '2020-03-11 22:39:11');
INSERT INTO `sys_menu` VALUES (3400, '表单设计', NULL, '/gen/design', 3000, 'icon-sheji', 2, '1', '0', '0', '', '2020-03-11 22:18:05', 'admin', '2022-11-10 21:40:16');
INSERT INTO `sys_menu` VALUES (4000, '服务监控', NULL, 'http://localhost:5001/login', -1, 'icon-iconset0265', 4, '0', '0', '0', 'admin', '2022-03-21 09:44:50', 'admin', '2022-11-10 21:42:17');
INSERT INTO `sys_menu` VALUES (9999, '系统官网', NULL, 'https://codeape4cloud.com/#/', -1, 'icon-web-icon-', 999, '0', '0', '1', '', '2019-01-17 17:05:19', 'admin', '2023-06-10 16:41:21');
INSERT INTO `sys_menu` VALUES (1684374329728, '医院管理', '', '/admin/syshospital/index', -1, 'icon-shujuzhanshi2', 5, '0', '0', '0', NULL, '2018-01-20 13:17:19', 'admin', '2023-05-25 19:26:31');
INSERT INTO `sys_menu` VALUES (1684374329729, '医院查看', 'admin_syshospital_get', NULL, 1684374329728, '1', 0, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1684374329730, '医院新增', 'admin_syshospital_add', NULL, 1684374329728, '1', 1, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1684374329731, '医院修改', 'admin_syshospital_edit', NULL, 1684374329728, '1', 2, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1684374329732, '医院删除', 'admin_syshospital_del', NULL, 1684374329728, '1', 3, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1684725265770, '目标时段管理', '', '/admin/systimetype/index', 1000, 'icon-iframe', 8, '0', '0', '0', NULL, '2018-01-20 13:17:19', 'admin', '2023-05-22 15:57:23');
INSERT INTO `sys_menu` VALUES (1684725265771, '检测时段查看', 'admin_systimetype_get', NULL, 1684725265770, '1', 0, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1684725265772, '检测时段新增', 'admin_systimetype_add', NULL, 1684725265770, '1', 1, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1684725265773, '检测时段修改', 'admin_systimetype_edit', NULL, 1684725265770, '1', 2, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1684725265774, '检测时段删除', 'admin_systimetype_del', NULL, 1684725265770, '1', 3, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1685002829989, '设备管理', '', '/device', -1, 'icon-jiankong', 6, '0', '0', '0', NULL, '2018-01-20 13:17:19', 'admin', '2023-05-25 19:46:24');
INSERT INTO `sys_menu` VALUES (1685002829990, '设备查看', 'device_deviceinfo_get', NULL, 1685002829989, '1', 2, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-25 19:25:39');
INSERT INTO `sys_menu` VALUES (1685002829991, '设备新增', 'device_deviceinfo_add', NULL, 1685002829989, '1', 1, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1685002829992, '设备修改', 'device_deviceinfo_edit', NULL, 1685002829989, '1', 2, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1685002829993, '设备删除', 'device_deviceinfo_del', NULL, 1685002829989, '1', 3, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1685178890515, '试纸列表', '', '/device/paper/index', 1685002829989, 'icon-send', 5, '0', '0', '0', NULL, '2018-01-20 13:17:19', 'admin', '2023-05-27 17:26:42');
INSERT INTO `sys_menu` VALUES (1685178890516, '试纸查看', 'device_paperinfo_get', NULL, 1685178890515, '1', 6, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-27 17:23:22');
INSERT INTO `sys_menu` VALUES (1685178890517, '试纸新增', 'device_paperinfo_add', NULL, 1685178890515, '1', 7, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-27 17:23:33');
INSERT INTO `sys_menu` VALUES (1685178890518, '试纸修改', 'device_paperinfo_edit', NULL, 1685178890515, '1', 8, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-27 17:23:43');
INSERT INTO `sys_menu` VALUES (1685178890519, '试纸删除', 'device_paperinfo_del', NULL, 1685178890515, '1', 9, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-27 17:23:47');
INSERT INTO `sys_menu` VALUES (1685191950869, '质控液列表', '', '/device/liquid/index', 1685002829989, 'icon-tubiaozhizuosvg-13', 10, '0', '0', '0', NULL, '2018-01-20 13:17:19', 'admin', '2023-05-27 21:01:17');
INSERT INTO `sys_menu` VALUES (1685191950870, '质控液查看', 'device_qcliquidinfo_get', NULL, 1685191950869, '1', 11, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-27 21:00:08');
INSERT INTO `sys_menu` VALUES (1685191950871, '质控液新增', 'device_qcliquidinfo_add', NULL, 1685191950869, '1', 12, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-27 21:00:13');
INSERT INTO `sys_menu` VALUES (1685191950872, '质控液修改', 'device_qcliquidinfo_edit', NULL, 1685191950869, '1', 13, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-27 21:00:18');
INSERT INTO `sys_menu` VALUES (1685191950873, '质控液删除', 'device_qcliquidinfo_del', NULL, 1685191950869, '1', 14, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-27 21:00:22');
INSERT INTO `sys_menu` VALUES (1685247343294, '质控记录', '', '/device/record/index', 1685002829989, 'icon-caidan1', 15, '0', '0', '0', NULL, '2018-01-20 13:17:19', 'admin', '2023-05-28 18:17:39');
INSERT INTO `sys_menu` VALUES (1685247343295, '质控记录查看', 'device_qcrecord_get', NULL, 1685247343294, '1', 16, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-28 18:16:28');
INSERT INTO `sys_menu` VALUES (1685247343296, '质控记录新增', 'device_qcrecord_add', NULL, 1685247343294, '1', 17, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-28 18:16:33');
INSERT INTO `sys_menu` VALUES (1685247343297, '质控记录修改', 'device_qcrecord_edit', NULL, 1685247343294, '1', 18, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-28 18:16:37');
INSERT INTO `sys_menu` VALUES (1685247343298, '质控记录删除', 'device_qcrecord_del', NULL, 1685247343294, '1', 19, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-28 18:16:41');
INSERT INTO `sys_menu` VALUES (1685350289870, '质控统计管理', '', '/device/count/index', 1685002829989, 'icon-biaoge1', 20, '0', '0', '0', NULL, '2018-01-20 13:17:19', 'admin', '2023-05-30 10:19:52');
INSERT INTO `sys_menu` VALUES (1685350289871, '质控统计查看', 'device_qccount_get', NULL, 1685350289870, '1', 21, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-30 10:20:02');
INSERT INTO `sys_menu` VALUES (1685350289872, '质控统计新增', 'device_qccount_add', NULL, 1685350289870, '1', 22, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-30 10:20:09');
INSERT INTO `sys_menu` VALUES (1685350289873, '质控统计修改', 'device_qccount_edit', NULL, 1685350289870, '1', 23, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-30 10:20:14');
INSERT INTO `sys_menu` VALUES (1685350289874, '质控统计删除', 'device_qccount_del', NULL, 1685350289870, '1', 24, '0', '1', '0', NULL, '2018-05-15 21:35:18', 'admin', '2023-05-30 10:20:18');
INSERT INTO `sys_menu` VALUES (1685676844730, '住院患者', '', '/inhos/pat/index', 1665170110243573762, 'icon-jiaoseguanli', 1, '0', '0', '0', NULL, '2018-01-20 13:17:19', 'admin', '2023-06-04 09:35:04');
INSERT INTO `sys_menu` VALUES (1685676844731, '住院患者查看', 'inhos_patinfohot_get', NULL, 1685676844730, '1', 0, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1685676844732, '住院患者新增', 'inhos_patinfohot_add', NULL, 1685676844730, '1', 1, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1685676844733, '住院患者修改', 'inhos_patinfohot_edit', NULL, 1685676844730, '1', 2, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1685676844734, '住院患者删除', 'inhos_patinfohot_del', NULL, 1685676844730, '1', 3, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1685844206511, '转科管理', '', '/inhos/transfer/index', 1665170110243573762, 'icon-component24', 3, '0', '0', '0', NULL, '2018-01-20 13:17:19', 'admin', '2023-06-10 15:38:46');
INSERT INTO `sys_menu` VALUES (1685844206512, '转科管理查看', 'api_pattransfer_get', NULL, 1685844206511, '1', 0, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1685844206513, '转科管理新增', 'api_pattransfer_add', NULL, 1685844206511, '1', 1, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1685844206514, '转科管理修改', 'api_pattransfer_edit', NULL, 1685844206511, '1', 2, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1685844206515, '转科管理删除', 'api_pattransfer_del', NULL, 1685844206511, '1', 3, '0', '1', '0', NULL, '2018-05-15 21:35:18', NULL, '2018-07-29 13:38:59');
INSERT INTO `sys_menu` VALUES (1661663113640628226, '设备列表', '', '/device/deviceinfo/index', 1685002829989, 'icon-keshihuapingtaiicon_zujian', 0, '0', '0', '1', 'admin', '2018-01-20 13:17:19', 'admin', '2023-05-25 19:41:32');
INSERT INTO `sys_menu` VALUES (1661700003911647234, '设备列表', '', '/device/device/index', 1685002829989, 'icon-sheji', 0, '0', '0', '0', 'admin', '2018-01-20 13:17:19', 'admin', '2023-05-25 19:44:15');
INSERT INTO `sys_menu` VALUES (1665170110243573762, '住院管理', NULL, '/inhos', -1, 'icon-rizhi', 999, '0', '0', '0', 'admin', '2023-06-04 09:34:26', 'admin', '2023-06-04 09:34:26');
INSERT INTO `sys_menu` VALUES (1665235323353546753, '住院患者出院', 'inhos_patinfo_leave', NULL, 1685676844730, NULL, 4, '0', '1', '0', 'admin', '2023-06-04 13:53:34', 'admin', '2023-06-04 13:53:34');
INSERT INTO `sys_menu` VALUES (1666316939865780225, '出院患者', NULL, '/inhos/outpat/index', 1665170110243573762, 'icon-wangzhan', 2, '0', '0', '0', 'admin', '2023-06-07 13:31:32', 'admin', '2023-06-07 13:31:32');
INSERT INTO `sys_menu` VALUES (1666317100683784194, '查看出院患者', 'inhos_out_pat_get', NULL, 1666316939865780225, NULL, 1, '0', '1', '0', 'admin', '2023-06-07 13:32:10', 'admin', '2023-06-07 13:32:10');
INSERT INTO `sys_menu` VALUES (1666344841332289538, '办理住院', 'inhos_out_pat_in', NULL, 1666316939865780225, NULL, 2, '0', '1', '0', 'admin', '2023-06-07 15:22:24', 'admin', '2023-06-07 15:22:24');

-- ----------------------------
-- Table structure for sys_oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `sys_oauth_client_details`;
CREATE TABLE `sys_oauth_client_details`  (
  `client_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '客户端ID',
  `resource_ids` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '资源列表',
  `client_secret` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '客户端密钥',
  `scope` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '域',
  `authorized_grant_types` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '认证类型',
  `web_server_redirect_uri` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '重定向地址',
  `authorities` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '角色列表',
  `access_token_validity` int(11) NULL DEFAULT NULL COMMENT 'token 有效期',
  `refresh_token_validity` int(11) NULL DEFAULT NULL COMMENT '刷新令牌有效期',
  `additional_information` varchar(4096) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '令牌扩展字段JSON',
  `autoapprove` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否自动放行',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '终端信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oauth_client_details
-- ----------------------------
INSERT INTO `sys_oauth_client_details` VALUES ('app', NULL, 'app', 'server', 'app,refresh_token', NULL, NULL, NULL, NULL, NULL, 'true', NULL, NULL, NULL, NULL);
INSERT INTO `sys_oauth_client_details` VALUES ('client', NULL, 'client', 'server', 'client_credentials', NULL, NULL, NULL, NULL, NULL, 'true', NULL, NULL, NULL, NULL);
INSERT INTO `sys_oauth_client_details` VALUES ('pda', '', 'pda', 'server', 'password,refresh_token', '', '', NULL, NULL, '', 'true', '2023-05-22 16:03:27', '2023-05-22 16:03:27', 'admin', 'admin');
INSERT INTO `sys_oauth_client_details` VALUES ('test', NULL, 'test', 'server', 'password,app,refresh_token', NULL, NULL, NULL, NULL, NULL, 'true', NULL, NULL, NULL, NULL);
INSERT INTO `sys_oauth_client_details` VALUES ('web', NULL, 'web', 'server', 'password,app,refresh_token,authorization_code,client_credentials', 'https://codeapex.vip', NULL, NULL, NULL, NULL, 'true', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '岗位名称',
  `post_sort` int(11) NOT NULL COMMENT '岗位排序',
  `hos_id` bigint(20) NOT NULL COMMENT '医院Id',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '是否删除  -1：已删除  0：正常',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新人',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`post_id`) USING BTREE,
  UNIQUE INDEX `hos_code_index`(`post_code`, `hos_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ADMIN', '管理员', 1, 1659018792143663105, '0', '2022-03-19 10:05:15', 'admin', '2023-05-19 12:27:16', 'admin', '系统管理员');
INSERT INTO `sys_post` VALUES (2, 'IN_HOS_DOC', '住院医生', 0, 1659018792143663105, '0', '2022-03-19 10:06:20', 'admin', '2023-06-02 12:24:17', 'admin', '住院医生');
INSERT INTO `sys_post` VALUES (3, 'BOSS', '院长', -1, 1659018792143663105, '0', '2022-03-19 10:06:35', 'admin', '2023-06-02 12:27:44', 'admin', '大boss');
INSERT INTO `sys_post` VALUES (1659437092614897665, 'IN_HOS_NURSE', '住院护士', 0, 1659018792143663105, '0', '2023-05-19 13:53:28', 'admin', '2023-06-10 16:18:09', 'admin', '住院护士');
INSERT INTO `sys_post` VALUES (1660165110450806786, 'ADMIN', '管理员', 0, 1660133100613181442, '0', '2023-05-21 14:06:21', 'admin', '2023-05-21 14:06:21', 'admin', '');
INSERT INTO `sys_post` VALUES (1660165152096051201, 'IN_HOS_DOC', '住院医生', 0, 1660133100613181442, '0', '2023-05-21 14:06:31', 'admin', '2023-05-21 14:06:31', 'admin', '');
INSERT INTO `sys_post` VALUES (1660165212405948418, 'IN_HOS_NURSE', '住院护士', 0, 1660133100613181442, '0', '2023-05-21 14:06:46', 'admin', '2023-05-21 14:06:46', 'admin', '');
INSERT INTO `sys_post` VALUES (1664487956488572929, 'OUT_HOS_DOC', '院外医生', 5, 1659018792143663105, '0', '2023-06-02 12:23:48', 'admin', '2023-06-02 12:23:48', 'admin', '院外医生');
INSERT INTO `sys_post` VALUES (1664488275129847809, 'MZ_INHOS_DOC', '门诊医生', 0, 1659018792143663105, '0', '2023-06-02 12:25:04', 'admin', '2023-06-02 12:25:04', 'admin', '门诊医生');
INSERT INTO `sys_post` VALUES (1664488426309341185, 'PHYSICAL_DOC', '体检医生', 0, 1659018792143663105, '0', '2023-06-02 12:25:40', 'admin', '2023-06-02 12:26:18', 'admin', '体检医生');

-- ----------------------------
-- Table structure for sys_public_param
-- ----------------------------
DROP TABLE IF EXISTS `sys_public_param`;
CREATE TABLE `sys_public_param`  (
  `public_id` bigint(20) NOT NULL COMMENT '编号',
  `public_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '参数名称',
  `public_key` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '参数键名',
  `public_value` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '参数键值',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态，1-启用，0-禁用',
  `validate_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '校验码',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT ' ' COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT ' ' COMMENT '修改人',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `public_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '参数类型，1-系统参数，2-业务参数',
  `system_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '是否为系统内置参数，1-是，0-否',
  PRIMARY KEY (`public_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '公共参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_public_param
-- ----------------------------
INSERT INTO `sys_public_param` VALUES (1, '接口文档不显示的字段', 'GEN_HIDDEN_COLUMNS', 'tenant_id', '0', '', ' ', ' ', '2020-05-12 04:25:19', NULL, '9', '1');
INSERT INTO `sys_public_param` VALUES (2, '注册用户默认角色', 'USER_DEFAULT_ROLE', 'GENERAL_USER', '0', '', 'admin', 'admin', '2022-03-30 10:00:57', '2022-03-30 02:05:59', '2', '1');
INSERT INTO `sys_public_param` VALUES (1660126345338482689, '医院管理员的角色编码', 'ADMIN_CODE', 'ADMIN', '0', '', 'admin', 'admin', '2023-05-21 11:32:19', '2023-05-21 12:37:38', '9', '1');
INSERT INTO `sys_public_param` VALUES (1660128138793512962, '医院管理员默认密码', 'HOS_ADMIN_PASSWORD', '123456', '0', '', 'admin', 'admin', '2023-05-21 11:39:27', '2023-05-21 11:39:27', '2', '1');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `role_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色名称',
  `role_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色代码',
  `role_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '角色描述',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标识：0-正常，1-删除',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '修改人',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`role_id`) USING BTREE,
  UNIQUE INDEX `role_idx1_role_code`(`role_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '系统角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', 'ADMIN', '管理员', '0', '2017-10-29 15:45:51', '2018-12-26 14:09:11', 'admin', 'admin');
INSERT INTO `sys_role` VALUES (1658730544372613122, '医生', 'DOC', '医生', '0', '2023-05-17 15:05:54', '2023-05-17 15:05:54', 'admin', 'admin');
INSERT INTO `sys_role` VALUES (1658730625385594882, '护士', 'NURSE', '护士', '0', '2023-05-17 15:06:13', '2023-05-17 15:06:13', 'admin', 'admin');
INSERT INTO `sys_role` VALUES (1660134954269057025, '系统管理员', 'SYSTEM_ADMIN', '系统管理员，拥有全部权限', '0', '2023-05-21 12:06:32', '2023-05-21 12:06:32', 'admin', 'admin');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 1000);
INSERT INTO `sys_role_menu` VALUES (1, 1100);
INSERT INTO `sys_role_menu` VALUES (1, 1101);
INSERT INTO `sys_role_menu` VALUES (1, 1102);
INSERT INTO `sys_role_menu` VALUES (1, 1103);
INSERT INTO `sys_role_menu` VALUES (1, 1104);
INSERT INTO `sys_role_menu` VALUES (1, 1200);
INSERT INTO `sys_role_menu` VALUES (1, 1201);
INSERT INTO `sys_role_menu` VALUES (1, 1202);
INSERT INTO `sys_role_menu` VALUES (1, 1203);
INSERT INTO `sys_role_menu` VALUES (1, 1300);
INSERT INTO `sys_role_menu` VALUES (1, 1301);
INSERT INTO `sys_role_menu` VALUES (1, 1302);
INSERT INTO `sys_role_menu` VALUES (1, 1303);
INSERT INTO `sys_role_menu` VALUES (1, 1304);
INSERT INTO `sys_role_menu` VALUES (1, 1305);
INSERT INTO `sys_role_menu` VALUES (1, 1400);
INSERT INTO `sys_role_menu` VALUES (1, 1401);
INSERT INTO `sys_role_menu` VALUES (1, 1402);
INSERT INTO `sys_role_menu` VALUES (1, 1403);
INSERT INTO `sys_role_menu` VALUES (1, 1500);
INSERT INTO `sys_role_menu` VALUES (1, 1501);
INSERT INTO `sys_role_menu` VALUES (1, 1502);
INSERT INTO `sys_role_menu` VALUES (1, 1503);
INSERT INTO `sys_role_menu` VALUES (1, 1504);
INSERT INTO `sys_role_menu` VALUES (1, 1505);
INSERT INTO `sys_role_menu` VALUES (1, 1684725265770);
INSERT INTO `sys_role_menu` VALUES (1, 1684725265771);
INSERT INTO `sys_role_menu` VALUES (1, 1684725265772);
INSERT INTO `sys_role_menu` VALUES (1, 1684725265773);
INSERT INTO `sys_role_menu` VALUES (1, 1684725265774);
INSERT INTO `sys_role_menu` VALUES (1, 1685002829989);
INSERT INTO `sys_role_menu` VALUES (1, 1685002829990);
INSERT INTO `sys_role_menu` VALUES (1, 1685002829991);
INSERT INTO `sys_role_menu` VALUES (1, 1685002829992);
INSERT INTO `sys_role_menu` VALUES (1, 1685002829993);
INSERT INTO `sys_role_menu` VALUES (1, 1685178890515);
INSERT INTO `sys_role_menu` VALUES (1, 1685178890516);
INSERT INTO `sys_role_menu` VALUES (1, 1685178890517);
INSERT INTO `sys_role_menu` VALUES (1, 1685178890518);
INSERT INTO `sys_role_menu` VALUES (1, 1685178890519);
INSERT INTO `sys_role_menu` VALUES (1, 1685191950869);
INSERT INTO `sys_role_menu` VALUES (1, 1685191950870);
INSERT INTO `sys_role_menu` VALUES (1, 1685191950871);
INSERT INTO `sys_role_menu` VALUES (1, 1685191950872);
INSERT INTO `sys_role_menu` VALUES (1, 1685191950873);
INSERT INTO `sys_role_menu` VALUES (1, 1685247343294);
INSERT INTO `sys_role_menu` VALUES (1, 1685247343295);
INSERT INTO `sys_role_menu` VALUES (1, 1685247343296);
INSERT INTO `sys_role_menu` VALUES (1, 1685247343297);
INSERT INTO `sys_role_menu` VALUES (1, 1685247343298);
INSERT INTO `sys_role_menu` VALUES (1, 1661700003911647234);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685002829989);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685002829990);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685002829991);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685002829992);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685002829993);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685178890515);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685178890516);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685178890517);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685178890518);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685178890519);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685191950869);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685191950870);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685191950871);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685191950872);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685191950873);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685247343294);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685247343295);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1685247343296);
INSERT INTO `sys_role_menu` VALUES (1658730625385594882, 1661700003911647234);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1000);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1100);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1101);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1102);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1103);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1104);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1200);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1201);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1202);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1203);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1300);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1301);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1302);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1303);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1304);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1305);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1400);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1401);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1402);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1403);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1500);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1501);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1502);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1503);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1504);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1505);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2000);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2100);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2101);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2102);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2200);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2201);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2202);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2203);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2300);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2301);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2400);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2401);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2402);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2403);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2600);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2601);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2602);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2603);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2700);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2701);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2702);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 2703);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 3000);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 3100);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 3200);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 3300);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 3301);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 3302);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 3303);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 3400);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 4000);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1684374329728);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1684374329729);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1684374329730);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1684374329731);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1684374329732);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1684725265770);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1684725265771);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1684725265772);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1684725265773);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1684725265774);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685002829989);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685002829990);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685002829991);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685002829992);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685002829993);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685178890515);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685178890516);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685178890517);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685178890518);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685178890519);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685191950869);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685191950870);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685191950871);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685191950872);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685191950873);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685247343294);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685247343295);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685247343296);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685247343297);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685247343298);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685350289870);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685350289871);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685350289872);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685350289873);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685350289874);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685676844730);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685676844731);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685676844732);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685676844733);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685676844734);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685844206511);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685844206512);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685844206513);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685844206514);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1685844206515);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1661700003911647234);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1665170110243573762);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1665235323353546753);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1666316939865780225);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1666317100683784194);
INSERT INTO `sys_role_menu` VALUES (1660134954269057025, 1666344841332289538);

-- ----------------------------
-- Table structure for sys_time_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_time_type`;
CREATE TABLE `sys_time_type`  (
  `id` bigint(11) NOT NULL,
  `hos_id` bigint(32) NOT NULL COMMENT '医院id',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '时间段名称',
  `time_scope` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '时间段范围文字描述02:00-04:00。\r\n强化字段及随机检测为空',
  `start_time` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开始时间04:30表示4点30',
  `end_time` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结束时间06:00表示6点00\r\n',
  `value_down` float(3, 1) NULL DEFAULT NULL COMMENT '血糖目标最低',
  `value_up` float(3, 1) NULL DEFAULT NULL COMMENT '血糖目标最高',
  `priority` int(2) NULL DEFAULT NULL COMMENT '排序号',
  `remark` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `type` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '1普通时段,0强化时段',
  `meal_time_type` int(1) NULL DEFAULT NULL COMMENT '1餐前,0 餐后',
  `in_hos` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '0 院外,1 院内, 2 门诊 ,3 体检',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '0-正常，1-删除',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '检测时段' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_time_type
-- ----------------------------
INSERT INTO `sys_time_type` VALUES (1660531690154209282, 1659018792143663105, '早餐前', '05:00-08:00', '05:00', '08:00', 1.0, 2.0, 1, NULL, '1', NULL, '1', '0', 'admin', 'admin', '2023-05-22 15:25:40', '2023-05-22 14:23:01');
INSERT INTO `sys_time_type` VALUES (1660531832907345921, 1659018792143663105, '随机', NULL, '', '', NULL, NULL, 2, NULL, '0', NULL, '1', '1', 'admin', 'admin', '2023-05-22 14:55:31', '2023-05-22 14:23:35');
INSERT INTO `sys_time_type` VALUES (1660547577087197186, 1659018792143663105, '早餐后', '08:00-11:00', '08:00', '11:00', 1.0, 2.0, 2, NULL, '1', NULL, '1', '0', 'admin', 'admin', '2023-05-22 15:26:09', '2023-05-22 15:26:09');
INSERT INTO `sys_time_type` VALUES (1660550091048157186, 1659018792143663105, '午餐前', '11:00-12:30', '11:00', '12:30', 12.0, 13.0, 3, NULL, '1', NULL, '1', '0', 'admin', 'admin', '2023-05-22 15:37:02', '2023-05-22 15:36:08');
INSERT INTO `sys_time_type` VALUES (1660551636724678657, 1659018792143663105, '午餐后', '12:30-14:00', '12:30', '14:00', 1.0, 2.0, 4, NULL, '1', NULL, '1', '0', 'admin', 'admin', '2023-05-22 15:42:16', '2023-05-22 15:42:16');
INSERT INTO `sys_time_type` VALUES (1660551785421144066, 1659018792143663105, '晚餐前', '14:00-16:00', '14:00', '16:00', 1.0, 2.5, 5, NULL, '1', NULL, '1', '0', 'admin', 'admin', '2023-05-22 15:42:52', '2023-05-22 15:42:52');
INSERT INTO `sys_time_type` VALUES (1660551903620825090, 1659018792143663105, '晚餐后', '16:00-20:00', '16:00', '20:00', 2.0, 12.0, 6, NULL, '1', NULL, '1', '0', 'admin', 'admin', '2023-05-22 15:43:48', '2023-05-22 15:43:20');
INSERT INTO `sys_time_type` VALUES (1660552109498236930, 1659018792143663105, '睡前', '20:00-00:00', '20:00', '00:00', 1.0, 2.0, 7, NULL, '1', NULL, '1', '0', 'admin', 'admin', '2023-05-22 15:45:00', '2023-05-22 15:44:09');
INSERT INTO `sys_time_type` VALUES (1660554319175024642, 1659018792143663105, '随机', NULL, NULL, '', NULL, NULL, 8, NULL, '0', NULL, '1', '0', 'admin', 'admin', '2023-05-22 15:52:56', '2023-05-22 15:52:56');
INSERT INTO `sys_time_type` VALUES (1660554411521015809, 1659018792143663105, 'q1h', NULL, NULL, '15:53', NULL, NULL, 9, NULL, '0', NULL, '1', '0', 'admin', 'admin', '2023-05-22 15:53:24', '2023-05-22 15:53:18');
INSERT INTO `sys_time_type` VALUES (1660554482903875586, 1659018792143663105, 'q4h', NULL, NULL, '', NULL, NULL, 10, NULL, '0', NULL, '1', '0', 'admin', 'admin', '2023-05-22 15:53:35', '2023-05-22 15:53:35');
INSERT INTO `sys_time_type` VALUES (1660554576998891522, 1659018792143663105, 'q6h', NULL, NULL, '', NULL, NULL, 11, NULL, '0', NULL, '1', '0', 'admin', 'admin', '2023-05-22 20:10:14', '2023-05-22 15:53:57');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '姓名',
  `gender` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '性别，1男二女',
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '密码',
  `dept_id` bigint(20) NULL DEFAULT NULL,
  `hos_id` bigint(20) NOT NULL COMMENT '医院ID',
  `salt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '随机盐',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '简介',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '头像',
  `lock_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '0-正常，9-锁定',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '0-正常，1-删除',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `user_hos_index`(`username`, `hos_id`, `del_flag`) USING BTREE,
  INDEX `user_idx1_username`(`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, '管理员', '1', 'admin', '$2a$10$RpFJjxYiXdEsAGnWp/8fsOetMuOON96Ntk/Ym2M/RKRyU0GZseaDC', 1659386542775017473, 1659018792143663105, NULL, '17034642999', '', '0', '0', '2018-04-20 07:15:18', '2023-05-21 12:14:24', NULL, 'admin');
INSERT INTO `sys_user` VALUES (1659441176889720833, '张三', '1', '2022123', '$2a$10$5iVzfQtacMSXHNl9I27RQ.2jaq/.eR7ckV7lCEu4TiLdnQUf4waJq', 1659386264763965441, 1659018792143663105, NULL, '18796327106', NULL, '0', '0', '2023-05-19 14:09:42', '2023-05-19 22:25:42', 'admin', 'admin');
INSERT INTO `sys_user` VALUES (1659458202844733441, '李四', '1', '202234', '$2a$10$bOx0Q/MylAi8vUJKmSSbkuB0PrHuNV3q683JLSEXIpF7Tp.4xpIEy', 1659386542775017473, 1659018792143663105, NULL, '18765732617', NULL, '0', '0', '2023-05-19 15:17:21', '2023-05-19 22:25:29', 'admin', 'admin');
INSERT INTO `sys_user` VALUES (1659818471869579266, '李倩文', '1', '12312', '$2a$10$qKYubw/t/VGkZxfDgME/m.u76xOUTvTaWKBYZj4rXRcj.Ws.WdShS', 1659790789526044673, 1659018792143663105, NULL, '', NULL, '0', '1', '2023-05-20 15:08:56', '2023-05-20 15:58:38', 'admin', 'admin');
INSERT INTO `sys_user` VALUES (1659824797278474241, NULL, NULL, '123874', '$2a$10$b0YcFvMYZ/iTTed8sa3rmedOVaZhhousGS/Cm9zSVvFzrLFurlRku', 1659386264763965441, 1659018792143663105, NULL, '18796329208', NULL, '0', '1', '2023-05-20 15:34:04', '2023-05-20 15:34:09', 'admin', 'admin');
INSERT INTO `sys_user` VALUES (1659825528257581058, '李四', NULL, '12934', '$2a$10$53K.fwPvvuIclxr01mMkHew7Hw8fML52jxA.GsrTDFOo5Cms4W77q', 1659386542775017473, 1659018792143663105, NULL, '18796327108', NULL, '0', '1', '2023-05-20 15:36:59', '2023-05-20 15:58:35', 'admin', 'admin');
INSERT INTO `sys_user` VALUES (1659826056786022402, '李伟', '1', '192883', '$2a$10$ZRaqB9aApC7J.fVOm8MNOeqv.VfWSV2ShbAsxp1B/GMd2GCSULbMG', 1659386542775017473, 1659018792143663105, NULL, '18796327198', NULL, '0', '1', '2023-05-20 15:39:05', '2023-05-20 15:56:37', 'admin', 'admin');
INSERT INTO `sys_user` VALUES (1659826749299167234, '张思思', '2', '193844', '$2a$10$qre1lAgGodKemPLroPToLuK9JZ.2x7qeRdCTI62Jsw4lAz622N/iC', 1659386542775017473, 1659018792143663105, NULL, '18796327107', NULL, '0', '1', '2023-05-20 15:41:50', '2023-05-20 15:58:33', 'admin', 'admin');
INSERT INTO `sys_user` VALUES (1659828235563372545, '成长', '1', '198384', '$2a$10$paTQoNqWWrjtXk.AHzR/HuYYaPeWGgQzKdDFLqpmZj.qIh0wOp1zW', 1659386542775017473, 1659018792143663105, NULL, '', NULL, '0', '1', '2023-05-20 15:47:44', '2023-05-20 15:56:17', 'admin', 'admin');
INSERT INTO `sys_user` VALUES (1659829855353245698, '立思辰', '1', '918283', '$2a$10$dCyClnSmNssJR3OfMJEW5u.TNfrONeod/H1UNC9VvOe0zouC.P/x2', 1659386264763965441, 1659018792143663105, NULL, '', NULL, '0', '1', '2023-05-20 15:54:10', '2023-05-20 15:54:54', 'admin', 'admin');
INSERT INTO `sys_user` VALUES (1659887681463345154, '王玮', '1', '201123', '$2a$10$IsCNlOhwgLtPapZR4cuMf.4mevf0WZAQoW5MUPhdniox65w7VJV6q', 1659386264763965441, 1659018792143663105, NULL, '', NULL, '0', '0', '2023-05-20 19:43:57', '2023-05-20 19:43:57', 'admin', 'admin');
INSERT INTO `sys_user` VALUES (1659889190586507265, '陈鑫', '2', '12934', '$2a$10$9fNKlOHAYAT/y.eXo2ZXnu8qaiakWOYk7.VGLDNZ6jbP/oex9fl.G', 1659386264763965441, 1659018792143663105, NULL, '', NULL, '0', '0', '2023-05-20 19:49:57', '2023-05-22 15:06:44', 'admin', 'admin');
INSERT INTO `sys_user` VALUES (1660133103368839170, '陈斌', '1', 'zjszyy', '$2a$10$q9XgvxAyNtufFzvwlUDHT.fPGYYvNum47MyaaAmPJP5nqk.lMnyGC', 1660162973545213954, 1660133100613181442, NULL, NULL, NULL, '0', '0', '2023-05-21 11:59:10', '2023-05-21 14:07:08', 'admin', 'admin');
INSERT INTO `sys_user` VALUES (1660166686271791105, '张三', '1', '123452', '$2a$10$KaCPIL0dlrYiDRw.Vd3ZMu7qPBiT95t53mQGRR4JdIdlDPEwyUWGm', 1660162973545213954, 1660133100613181442, NULL, '', NULL, '0', '0', '2023-05-21 14:12:37', '2023-05-21 14:12:37', 'zjszyy', 'zjszyy');
INSERT INTO `sys_user` VALUES (1662003819232063489, '张三', '1', '1234333', '$2a$10$kYe6Iu1EULUQhdvLlx2tNuBuPUXfaCV7vyGasqYAU.mdM7leDaxYG', 1659386264763965441, 1659018792143663105, NULL, '', NULL, '0', '0', '2023-05-26 15:52:44', '2023-05-26 15:52:44', 'admin', 'admin');
INSERT INTO `sys_user` VALUES (1662369503690608642, '王慧明', '2', 'whm', '$2a$10$mGYs.8aV.GafqfhoihzMrOcWVyprXn9muKzflm5ZWg5SJ1gnrMb22', 1660162973545213954, 1660133100613181442, NULL, '', NULL, '0', '0', '2023-05-27 16:05:50', '2023-05-27 16:05:50', 'admin', 'admin');
INSERT INTO `sys_user` VALUES (1662432715555459074, '白鹿', '2', 'bailu', '$2a$10$qvnYq.m7.XQIDlrzGSqqsOgh2mgg9.orBkYVvmY7fnw16G5rOkh96', 1659386542775017473, 1659018792143663105, NULL, '', NULL, '0', '0', '2023-05-27 20:17:01', '2023-05-28 22:26:48', 'admin', 'admin');

-- ----------------------------
-- Table structure for sys_user_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_dept`;
CREATE TABLE `sys_user_dept`  (
  `user_id` bigint(20) NOT NULL,
  `dept_id` bigint(20) NOT NULL,
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '0-正常，1-删除',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  UNIQUE INDEX `user_dept_index`(`user_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户、科室关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_dept
-- ----------------------------
INSERT INTO `sys_user_dept` VALUES (1, 1659386149638709250, '0', '2023-05-21 12:14:25', '2023-05-21 12:14:25', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1, 1659386542775017473, '0', '2023-05-21 12:14:25', '2023-05-21 12:14:25', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659441176889720833, 1659386264763965441, '0', '2023-05-19 22:25:42', '2023-05-19 22:25:42', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659441176889720833, 1659386542775017473, '0', '2023-05-19 22:25:42', '2023-05-19 22:25:42', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659458202844733441, 1659386264763965441, '0', '2023-05-19 22:25:29', '2023-05-19 22:25:29', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659458202844733441, 1659386542775017473, '0', '2023-05-19 22:25:29', '2023-05-19 22:25:29', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659818471869579266, 1659790789526044673, '0', '2023-05-20 15:26:22', '2023-05-20 15:26:22', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659824797278474241, 1659789309465214978, '0', '2023-05-20 15:34:04', '2023-05-20 15:34:04', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659825528257581058, 1659386264763965441, '0', '2023-05-20 15:36:59', '2023-05-20 15:36:59', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659825528257581058, 1659386542775017473, '0', '2023-05-20 15:36:59', '2023-05-20 15:36:59', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659825528257581058, 1659789309465214978, '0', '2023-05-20 15:36:59', '2023-05-20 15:36:59', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659825528257581058, 1659789383939276802, '0', '2023-05-20 15:36:59', '2023-05-20 15:36:59', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659825528257581058, 1659790789526044673, '0', '2023-05-20 15:36:59', '2023-05-20 15:36:59', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659825528257581058, 1659799651851362305, '0', '2023-05-20 15:36:59', '2023-05-20 15:36:59', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659826056786022402, 1659789383939276802, '0', '2023-05-20 15:39:05', '2023-05-20 15:39:05', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659826056786022402, 1659799651851362305, '0', '2023-05-20 15:39:05', '2023-05-20 15:39:05', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659826749299167234, 1659790789526044673, '0', '2023-05-20 15:41:50', '2023-05-20 15:41:50', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659828235563372545, 1659789309465214978, '0', '2023-05-20 15:47:44', '2023-05-20 15:47:44', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659828235563372545, 1659789383939276802, '0', '2023-05-20 15:47:44', '2023-05-20 15:47:44', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659829855353245698, 1659789383939276802, '0', '2023-05-20 15:54:10', '2023-05-20 15:54:10', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659887681463345154, 1659789309465214978, '0', '2023-05-20 19:43:57', '2023-05-20 19:43:57', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659889190586507265, 1659386264763965441, '0', '2023-05-22 15:06:45', '2023-05-22 15:06:45', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659889190586507265, 1659386542775017473, '0', '2023-05-22 15:06:45', '2023-05-22 15:06:45', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659889190586507265, 1659789309465214978, '0', '2023-05-22 15:06:45', '2023-05-22 15:06:45', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1659889190586507265, 1659789383939276802, '0', '2023-05-22 15:06:45', '2023-05-22 15:06:45', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1660133103368839170, 1660162973545213954, '0', '2023-05-21 14:07:08', '2023-05-21 14:07:08', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1660133103368839170, 1660163024992546818, '0', '2023-05-21 14:07:08', '2023-05-21 14:07:08', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1660166686271791105, 1660162973545213954, '0', '2023-05-21 14:12:37', '2023-05-21 14:12:37', 'zjszyy', 'zjszyy');
INSERT INTO `sys_user_dept` VALUES (1662003819232063489, 1659789309465214978, '0', '2023-05-26 15:52:44', '2023-05-26 15:52:44', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1662003819232063489, 1659789383939276802, '0', '2023-05-26 15:52:44', '2023-05-26 15:52:44', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1662369503690608642, 1660162973545213954, '0', '2023-05-27 16:05:50', '2023-05-27 16:05:50', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1662369503690608642, 1660163024992546818, '0', '2023-05-27 16:05:50', '2023-05-27 16:05:50', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1662369503690608642, 1660167857690566657, '0', '2023-05-27 16:05:50', '2023-05-27 16:05:50', 'admin', 'admin');
INSERT INTO `sys_user_dept` VALUES (1662432715555459074, 1659386542775017473, '0', '2023-05-28 22:26:48', '2023-05-28 22:26:48', 'admin', 'admin');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (1659441176889720833, 2);
INSERT INTO `sys_user_post` VALUES (1659458202844733441, 2);
INSERT INTO `sys_user_post` VALUES (1659887681463345154, 2);
INSERT INTO `sys_user_post` VALUES (1659889190586507265, 2);
INSERT INTO `sys_user_post` VALUES (1660133103368839170, 1660165152096051201);
INSERT INTO `sys_user_post` VALUES (1660166686271791105, 1660165152096051201);
INSERT INTO `sys_user_post` VALUES (1662003819232063489, 2);
INSERT INTO `sys_user_post` VALUES (1662369503690608642, 1660165212405948418);
INSERT INTO `sys_user_post` VALUES (1662432715555459074, 1659437092614897665);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1660134954269057025);
INSERT INTO `sys_user_role` VALUES (1659441176889720833, 1658730544372613122);
INSERT INTO `sys_user_role` VALUES (1659458202844733441, 1);
INSERT INTO `sys_user_role` VALUES (1659887681463345154, 1658730544372613122);
INSERT INTO `sys_user_role` VALUES (1659889190586507265, 1658730544372613122);
INSERT INTO `sys_user_role` VALUES (1660133103368839170, 1);
INSERT INTO `sys_user_role` VALUES (1660166686271791105, 1658730544372613122);
INSERT INTO `sys_user_role` VALUES (1662003819232063489, 1658730544372613122);
INSERT INTO `sys_user_role` VALUES (1662369503690608642, 1658730625385594882);
INSERT INTO `sys_user_role` VALUES (1662432715555459074, 1658730625385594882);

SET FOREIGN_KEY_CHECKS = 1;
