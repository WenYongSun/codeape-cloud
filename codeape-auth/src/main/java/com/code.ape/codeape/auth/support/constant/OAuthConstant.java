package com.code.ape.codeape.auth.support.constant;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
public class OAuthConstant {
	public final static String OAUTH_PARAMETER_ORG_ID="hosId";

	public final static String OAUTH_PARAMETER_SN_ID="sn";

}
