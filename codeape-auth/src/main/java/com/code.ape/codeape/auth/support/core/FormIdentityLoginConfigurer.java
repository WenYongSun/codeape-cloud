package com.code.ape.codeape.auth.support.core;

import com.code.ape.codeape.auth.support.handler.FormAuthenticationFailureHandler;
import com.code.ape.codeape.auth.support.handler.SsoLogoutSuccessHandler;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @data 2022-06-04
 *
 * 基于授权码模式 统一认证登录 spring security & sas 都可以使用 所以抽取成 HttpConfigurer
 */
public final class FormIdentityLoginConfigurer
		extends AbstractHttpConfigurer<FormIdentityLoginConfigurer, HttpSecurity> {

	@Override
	public void init(HttpSecurity http) throws Exception {
		http.formLogin(formLogin -> {
			formLogin.loginPage("/token/login");
			formLogin.loginProcessingUrl("/token/form");
			formLogin.failureHandler(new FormAuthenticationFailureHandler());

		})
			.logout() // SSO登出成功处理
			.logoutSuccessHandler(new SsoLogoutSuccessHandler())
			.deleteCookies("JSESSIONID")
			.invalidateHttpSession(true)
			.and()
			.csrf()
			.disable();
	}

}
