

package com.code.ape.codeape.auth;

import com.code.ape.codeape.common.feign.annotation.EnableCodeapeFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2018年06月21日 认证授权中心
 */
@EnableCodeapeFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class CodeapeAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeapeAuthApplication.class, args);
	}

}
