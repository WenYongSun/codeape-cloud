

package com.code.ape.codeape.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2018年06月21日 监控中心
 */
@EnableAdminServer
@EnableDiscoveryClient
@SpringBootApplication
public class CodeapeMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeapeMonitorApplication.class, args);
	}

}
