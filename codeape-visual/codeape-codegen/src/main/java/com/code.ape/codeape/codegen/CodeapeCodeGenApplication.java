

package com.code.ape.codeape.codegen;

import com.code.ape.codeape.common.datasource.annotation.EnableDynamicDataSource;
import com.code.ape.codeape.common.feign.annotation.EnableCodeapeFeignClients;
import com.code.ape.codeape.common.security.annotation.EnableCodeapeResourceServer;
import com.code.ape.codeape.common.swagger.annotation.EnableCodeapeDoc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2020/03/11 代码生成模块
 */
@EnableCodeapeDoc
@EnableDynamicDataSource
@EnableCodeapeFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@EnableCodeapeResourceServer
public class CodeapeCodeGenApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeapeCodeGenApplication.class, args);
	}

}
