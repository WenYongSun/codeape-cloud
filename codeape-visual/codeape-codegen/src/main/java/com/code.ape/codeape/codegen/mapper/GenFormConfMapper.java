

package com.code.ape.codeape.codegen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.ape.codeape.codegen.entity.GenFormConf;
import org.apache.ibatis.annotations.Mapper;

/**
 * 生成记录
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2019-08-12 15:55:35
 */
@Mapper
public interface GenFormConfMapper extends BaseMapper<GenFormConf> {

}
