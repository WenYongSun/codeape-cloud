

package com.code.ape.codeape.codegen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.codegen.entity.GenFormConf;

/**
 * 表单管理
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2019-08-12 15:55:35
 */
public interface GenFormConfService extends IService<GenFormConf> {

	/**
	 * 获取表单信息
	 * @param dsName 数据源ID
	 * @param tableName 表名称
	 * @return
	 */
	String getForm(String dsName, String tableName);

}
