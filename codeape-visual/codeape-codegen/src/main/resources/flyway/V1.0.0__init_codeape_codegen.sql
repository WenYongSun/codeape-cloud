/*
 Navicat Premium Data Transfer

 Source Server         : codeape-mysql
 Source Server Type    : MySQL
 Source Server Version : 50742
 Source Host           : codeape-mysql:3306
 Source Schema         : codeape_codegen

 Target Server Type    : MySQL
 Target Server Version : 50742
 File Encoding         : 65001

 Date: 11/06/2023 10:34:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_datasource_conf
-- ----------------------------
DROP TABLE IF EXISTS `gen_datasource_conf`;
CREATE TABLE `gen_datasource_conf`  (
  `id` bigint(20) NOT NULL COMMENT '数据源ID',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '数据源名称',
  `url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'jdbc-url',
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '密码',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标记',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '数据源表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_datasource_conf
-- ----------------------------
INSERT INTO `gen_datasource_conf` VALUES (1658762275280646145, 'codeape_codeape', 'jdbc:mysql://codeape-mysql:3306/codeape?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Asia/Shanghai&nullCatalogMeansCurrent=true&allowPublicKeyRetrieval=true', 'root', 'IPO9qWcNgIyX5pqvZpj/Gg==', '0', '2023-05-17 17:11:59', 'admin', '2023-05-17 17:11:59', 'admin');
INSERT INTO `gen_datasource_conf` VALUES (1661639980464435201, 'codeape_device', 'jdbc:mysql://codeape-mysql:3306/codeape_device?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&allowMultiQueries=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Asia/Shanghai&nullCatalogMeansCurrent=true&allowPublicKeyRetrieval=true', 'root', 'nQmBVNsWUltaybdhCW1dOA==', '0', '2023-05-25 15:46:58', 'admin', '2023-05-25 15:46:58', 'admin');
INSERT INTO `gen_datasource_conf` VALUES (1664473928039018498, 'codeape_inhos', 'jdbc:mysql://codeape-mysql:3306/codeape_inhos?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Asia/Shanghai&nullCatalogMeansCurrent=true&allowPublicKeyRetrieval=true', 'root', 'LIYjhcBM9zBAivSsZvu+9w==', '0', '2023-06-02 11:28:04', 'admin', '2023-06-02 11:28:04', 'admin');

-- ----------------------------
-- Table structure for gen_form_conf
-- ----------------------------
DROP TABLE IF EXISTS `gen_form_conf`;
CREATE TABLE `gen_form_conf`  (
  `id` bigint(20) NOT NULL COMMENT '表单配置ID',
  `table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '表名',
  `form_info` json NOT NULL COMMENT '表单信息',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标记',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `table_name`(`table_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '表单配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_form_conf
-- ----------------------------
INSERT INTO `gen_form_conf` VALUES (1661647625590108161, 'gen_form_conf', '\"{\\n  column: [\\n    {\\n      type: \'checkbox\',\\n      label: \'多选框组\',\\n      dicData: [\\n        {\\n          label: \'选项一\',\\n          value:  0\\n        },\\n        {\\n          label: \'选项二\',\\n          value: 1\\n        },\\n        {\\n          label: \'选项三\',\\n          value: 2\\n        }\\n      ],\\n      span: 24,\\n      display: true,\\n      props: {\\n        label: \'label\',\\n        value: \'value\',\\n        desc: \'desc\'\\n      },\\n      prop: \'a168500261761623899\'\\n    },\\n    {\\n      type: \'input\',\\n      label: \'表单配置ID\',\\n      prop: \'id\',\\n      addDisplay: false,\\n      editDisabled: true\\n    },\\n    {\\n      type: \'input\',\\n      label: \'表名\',\\n      prop: \'tableName\',\\n      addDisplay: false,\\n      editDisabled: true\\n    },\\n    {\\n      type: \'input\',\\n      label: \'表单信息\',\\n      prop: \'formInfo\',\\n      addDisplay: false,\\n      editDisabled: true\\n    },\\n    {\\n      type: \'input\',\\n      label: \'删除标记\',\\n      prop: \'delFlag\',\\n      addDisplay: false,\\n      editDisabled: true\\n    },\\n    {\\n      type: \'input\',\\n      label: \'创建时间\',\\n      prop: \'createTime\',\\n      addDisplay: false,\\n      editDisabled: true\\n    },\\n    {\\n      type: \'input\',\\n      label: \'创建人\',\\n      prop: \'createBy\',\\n      addDisplay: false,\\n      editDisabled: true\\n    },\\n    {\\n      type: \'input\',\\n      label: \'修改时间\',\\n      prop: \'updateTime\',\\n      addDisplay: false,\\n      editDisabled: true\\n    },\\n    {\\n      type: \'input\',\\n      label: \'更新人\',\\n      prop: \'updateBy\',\\n      addDisplay: false,\\n      editDisabled: true\\n    }\\n  ],\\n  labelPosition: \'left\',\\n  labelSuffix: \'：\',\\n  labelWidth: 120,\\n  gutter: 0,\\n  menuBtn: true,\\n  submitBtn: true,\\n  submitText: \'提交\',\\n  emptyBtn: true,\\n  emptyText: \'清空\',\\n  menuPosition: \'center\',\\n  border: true,\\n  index: true,\\n  indexLabel: \'序号\',\\n  stripe: true,\\n  menuAlign: \'center\',\\n  align: \'center\',\\n  searchMenuSpan: 6\\n}\"', '0', '2023-05-25 16:17:21', 'admin', '2023-05-25 16:17:21', 'admin');

SET FOREIGN_KEY_CHECKS = 1;
