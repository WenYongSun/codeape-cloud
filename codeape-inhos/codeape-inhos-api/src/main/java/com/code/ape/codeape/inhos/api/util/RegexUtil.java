package com.code.ape.codeape.inhos.api.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description 正则工具类
 */
public class RegexUtil {
	/**
	 * 提取数字
	 * @param str 字符串
	 * @return 正则提取之后
	 */
	public static String getNumber(String str,String regex) {
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(str);
		return m.replaceAll("").trim();
	}
}
