
package com.code.ape.codeape.inhos.api.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 住院患者
 *
 * @author pig code generator
 * @date 2023-06-02 11:34:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "住院患者")
public class PatInfoVO  {

    /**
     * 主键id
     */
    @Schema(description ="主键id")
    private Long id;

    /**
     * 患者住院号，每次住院唯一
     */
    @Schema(description ="患者住院号，每次住院唯一")
    private String iptNum;

    /**
     * 就诊流水号，终身唯一
     */
    @Schema(description ="就诊流水号，终身唯一")
    private String visitId;

    /**
     * 身份证号
     */
    @Schema(description ="身份证号")
    private String idCard;

    /**
     * 姓名
     */
    @Schema(description ="姓名")
    private String name;

    /**
     * 手机
     */
    @Schema(description ="手机")
    private String phone;

    /**
     * 性别 来源字典
     */
    @Schema(description ="性别 来源字典")
    private String gender;

    /**
     * 床号
     */
    @Schema(description ="床号")
    private String bedNum;

    @Schema(description = "床位排序号")
    private String bedSort;

    /**
     * 主治医生Id
     */
    @Schema(description ="主治医生Id")
    private Long docId;

    @Schema(description = "主治医生姓名")
    private String docName;

    /**
     * 责任护士
     */
    @Schema(description ="责任护士")
    private Long nurseId;

    @Schema(description = "责任护士姓名")
    private String nurseName;

    /**
     * deptId
     */
    @Schema(description ="deptId")
    private Long deptId;

    /**
     * 住院时间
     */
    @Schema(description ="住院时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime iptTime;

    /**
     * 出院时间
     */
    @Schema(description ="leaveTime")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime leaveTime;

    /**
     * 出生日期
     */
    @Schema(description ="出生日期")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private LocalDateTime birthday;

    /**
     * 身高
     */
    @Schema(description ="身高")
    private Integer height;

    /**
     * 体重
     */
    @Schema(description ="体重")
    private Float weight;

    /**
     * 糖尿病类型：I型，II型，来源字典
     */
    @Schema(description ="糖尿病类型：I型，II型，来源字典")
    private String diabetesType;

    /**
     * 入院诊断
     */
    @Schema(description ="入院诊断")
    private String diagnosis;

    /**
     * 患者腕带条码值
     */
    @Schema(description ="患者腕带条码值")
    private String scanCode;

    @Schema(description = "入院血糖")
    private Float inGluValue;

    @Schema(description = "出院血糖")
    private Float outGluValue;

    @Schema(description = "出入院状态")
    private Integer status;
}
