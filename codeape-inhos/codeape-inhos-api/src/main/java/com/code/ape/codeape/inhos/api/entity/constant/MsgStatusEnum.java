package com.code.ape.codeape.inhos.api.entity.constant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description  消息日志的状态
 */
@Getter
@RequiredArgsConstructor
public enum MsgStatusEnum {
	MSG_SEND(1,"已发送"),

	MSG_SUCCESS(2,"发送成功"),

	MSG_FAIL(3,"发送失败");

	private final Integer value;

	private final String description;
}
