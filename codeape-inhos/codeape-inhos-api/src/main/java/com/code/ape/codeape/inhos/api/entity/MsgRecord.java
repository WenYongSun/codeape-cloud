
package com.code.ape.codeape.inhos.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 消息日志
 *
 * @author pig code generator
 * @date 2023-06-05 19:27:14
 */
@Data
@TableName("msg_record")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "消息日志")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MsgRecord extends BaseEntity {

    /**
     * ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="ID")
    private Long id;

    /**
     * 消息内容
     */
    @Schema(description ="消息内容")
    private String content;

    /**
     * 消息ID
     */
    @Schema(description ="消息ID")
    private String msgId;

    /**
     * 主题
     */
    @Schema(description ="主题")
    private String topic;

    /**
     * 异常信息
     */
    @Schema(description ="异常信息")
    private String exceptionMsg;

    /**
     * 消息状态 1 已发送 2 发送成功 3 发送失败
     */
    @Schema(description ="消息状态 1 已发送 2 发送成功 3 发送失败")
    private Integer status;

    /**
     * 删除标记  -1：已删除  0：正常
     */
    @TableLogic
    @Schema(description ="删除标记  -1：已删除  0：正常")
    private String delFlag;

    @Schema(description = "发送失败的次数")
    private Integer failNum;


}
