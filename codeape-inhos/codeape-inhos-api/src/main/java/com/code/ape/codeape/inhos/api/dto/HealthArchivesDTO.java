
package com.code.ape.codeape.inhos.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.core.entity.BaseParam;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 控糖目标
 *
 * @author pig code generator
 * @date 2023-06-14 17:16:37
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "控糖目标")
public class HealthArchivesDTO extends BaseParam {

    /**
     * id
     */
    @Schema(description ="id")
	@NotNull(message = "ID不能为空",groups = {HealthArchivesDTODelete.class,HealthArchivesDTOUpdate.class})
    private Long id;

    /**
     * 患者ID
     */
    @Schema(description ="患者ID")
	@NotNull(message = "患者ID不能为空",groups = {HealthArchivesDTOAdd.class,HealthArchivesDTOPatId.class})
    private Long patId;

    /**
     * 时段名称
     */
    @Schema(description ="时段名称")
	@NotBlank(message = "时段名称不能为空",groups = {HealthArchivesDTOAdd.class})
    private String timeType;

    /**
     * 目标上限
     */
    @Schema(description ="目标上限")
	@NotNull(message = "上限值不能为空",groups = {HealthArchivesDTOAdd.class})
    private Float valueUp;

    /**
     * 目标下限
     */
    @Schema(description ="目标下限")
	@NotNull(message = "下限值不能为空",groups = {HealthArchivesDTOAdd.class})
    private Float valueDown;

    @Schema(description = "患者的科室Id")
    private Long deptId;

    public interface HealthArchivesDTOAdd{}
	public interface HealthArchivesDTOUpdate{}
	public interface HealthArchivesDTODelete{}
	public interface HealthArchivesDTOPatId{}
}
