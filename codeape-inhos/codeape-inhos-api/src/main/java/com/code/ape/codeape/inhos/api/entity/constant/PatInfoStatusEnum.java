package com.code.ape.codeape.inhos.api.entity.constant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description  出入院状态
 */
@Getter
@RequiredArgsConstructor
public enum PatInfoStatusEnum {
	IN_HOS(1,"在院"),

	OUT_HOS(0,"出院");

	private final Integer value;

	private final String description;
}
