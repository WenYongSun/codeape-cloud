package com.code.ape.codeape.inhos.api.entity.constant;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description 住院模块的QM配置
 */
public class MqConstant {
	/**
	 * 触发冷热分离的Topic
	 */
	public final static String PAT_TOPIC="pat_topic";

	/**
	 * 出院数据迁移到冷库
	 */
	public final static String TAG_COLD="TAG_COLD";

	/**
	 * 入院数据迁移到热库
	 */
	public final static String TAG_HOT="TAG_HOT";
}
