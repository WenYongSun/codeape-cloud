
package com.code.ape.codeape.inhos.api.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

/**
 * 转科管理
 *
 * @author pig code generator
 * @date 2023-06-04 10:03:26
 */
@Data
@Schema(description = "转科管理")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PatTransferVO  {

    /**
     * ID
     */
    @Schema(description ="ID")
    private Long id;

    /**
     * 患者id
     */
    @Schema(description ="患者id")
    private Long patId;

    @Schema(description = "患者姓名")
    private String patName;

    /**
     * 就诊流水号，终身唯一
     */
    @Schema(description ="就诊流水号，终身唯一")
    private String visitId;

    /**
     * 住院号
     */
    @Schema(description ="住院号")
    private String iptNum;

    /**
     * 床号
     */
    @Schema(description ="床号")
    private String bedNum;

    /**
     * 转科时间
     */
    @Schema(description ="转科时间")
    private LocalDateTime transferTime;

    /**
     * 原科室
     */
    @Schema(description ="原科室")
    private Long oldDept;

    /**
     * 当前科室
     */
    @Schema(description ="当前科室")
    private Long newDept;

    /**
     * 责任医生的id
     */
    @Schema(description ="责任医生的id")
    private Long docId;

    /**
     * 责任护士id
     */
    @Schema(description ="责任护士id")
    private Long nurseId;

    /**
     * 备注
     */
    @Schema(description ="备注")
    private String remark;
}
