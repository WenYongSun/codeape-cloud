package com.code.ape.codeape.inhos.api.entity.constant;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
public class PatInfoConstant {
	/**
	 * 床位的正则表达式，直接取出全部的数字
	 */
	public static final  String REGULAR_EXPRESSION = "[^0-9]";

	/**
	 * 存储在ElasticSearch中的index名称
	 */
	public final static String PAT_INFO_INDEX_NAME="pat_info";
}
