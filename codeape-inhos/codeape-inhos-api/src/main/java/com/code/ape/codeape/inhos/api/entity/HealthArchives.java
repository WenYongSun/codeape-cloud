
package com.code.ape.codeape.inhos.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 控糖目标
 *
 * @author pig code generator
 * @date 2023-06-14 17:16:37
 */
@Data
@TableName("health_archives")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "控糖目标")
public class HealthArchives extends BaseEntity {

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="id")
    private Long id;

    /**
     * 患者ID
     */
    @Schema(description ="患者ID")
    private Long patId;

    /**
     * 医院ID
     */
    @Schema(description ="医院ID")
    private Long hosId;

    /**
     * 时段名称
     */
    @Schema(description ="时段名称")
    private String timeType;

    /**
     * 目标上限
     */
    @Schema(description ="目标上限")
    private Float valueUp;

    /**
     * 目标下限
     */
    @Schema(description ="目标下限")
    private Float valueDown;

    /**
     * 0-正常，1-删除
     */
    @Schema(description ="0-正常，1-删除")
	@TableLogic
    private String delFlag;


}
