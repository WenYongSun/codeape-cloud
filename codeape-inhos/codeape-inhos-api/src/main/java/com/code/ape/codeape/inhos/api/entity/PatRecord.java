
package com.code.ape.codeape.inhos.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

/**
 * 患者出入院记录
 *
 * @author pig code generator
 * @date 2023-06-02 17:00:26
 */
@Data
@TableName("pat_record")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "患者出入院记录")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PatRecord extends BaseEntity {

    /**
     * ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="ID")
    private Long id;

    /**
     * 患者ID
     */
    @Schema(description ="患者ID")
    private Long patId;

    /**
     * 就诊流水号，终身唯一
     */
    @Schema(description ="就诊流水号，终身唯一")
    private String visitId;

    /**
     * 住院的医院id
     */
    @Schema(description ="住院的医院id")
    private Long hosId;

    /**
     * 住院号
     */
    @Schema(description ="住院号")
    private String iptNum;

    /**
     * 床位号
     */
    @Schema(description ="床位号")
    private String bedNum;

    /**
     * 科室ID
     */
    @Schema(description ="科室ID")
    private Long deptId;

    /**
     * 主治医生ID
     */
    @Schema(description ="主治医生ID")
    private Long docId;

    /**
     * 责任护士id
     */
    @Schema(description ="责任护士id")
    private Long nurseId;

    /**
     * 入院时间
     */
    @Schema(description ="入院时间")
    private LocalDateTime iptTime;

    /**
     * 出院时间
     */
    @Schema(description ="出院时间")
    private LocalDateTime leaveTime;

    /**
     * 备注
     */
    @Schema(description ="备注")
    private String comment;

    /**
     * 出入院类型：0：出院；1：入院
     */
    @Schema(description ="出入院类型：0：出院；1：入院")
    private Integer iptType;

    /**
     * 删除标记  -1：已删除  0：正常
     */
    @Schema(description ="删除标记  -1：已删除  0：正常")
	@TableLogic
    private String delFlag;


}
