package com.code.ape.codeape.inhos.api.entity.constant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description  入院记录状态
 */
@Getter
@RequiredArgsConstructor
public enum PatRecordTypeEnum {
	IN_HOS(1,"入院"),
	OUT_HOS(0,"出院");
	private final Integer value;

	private final String description;
}
