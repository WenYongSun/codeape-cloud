
package com.code.ape.codeape.inhos.api.dto;

import com.code.ape.codeape.common.core.entity.BaseParam;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 住院患者
 *
 * @author pig code generator
 * @date 2023-06-02 11:34:04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "住院患者")
public class PatInfoDTO extends BaseParam {

    /**
     * 主键id
     */
    @Schema(description ="主键id")
	@NotNull(message = "ID不能为空",groups = {PatInfoDTOUpdate.class,PatInfoDTODelete.class,PatInfoDTOLeave.class,PatInfoDTOIN.class})
    private Long id;

    /**
     * 患者住院号，每次住院唯一
     */
    @Schema(description ="患者住院号，每次住院唯一")
	@NotBlank(message = "住院号不能为空",groups = {PatInfoDTOADD.class,PatInfoDTOUpdate.class,PatInfoDTOIN.class})
    private String iptNum;

    /**
     * 就诊流水号，终身唯一
	 * 就这流水号是HIS系统识别指定的患者的唯一识别号，一般在门诊看诊就会生成，有很多HIS系统都会将这个就诊流水号作为唯一识别，如果其他his系统不一致，也填充在这字段中
	 * 1. 本系统添加，不需要填写就诊流水号，系统自动生成，因此这里就不是必填了
	 * 2. 同步HIS数据：需要让HIS系统这个流水号作为唯一识别
     */
    @Schema(description ="就诊流水号，终身唯一")
//	@NotBlank(message = "就诊流水号不能为空",groups = {PatInfoDTOADD.class})
    private String visitId;

    /**
     * 身份证号
     */
    @Schema(description ="身份证号")
    private String idCard;

    /**
     * 姓名
     */
    @Schema(description ="姓名")
	@NotBlank(message = "姓名不能为空",groups = {PatInfoDTOADD.class,PatInfoDTOUpdate.class,PatInfoDTOIN.class})
    private String name;

    /**
     * 手机
     */
    @Schema(description ="手机")
    private String phone;

    /**
     * 性别 来源字典
     */
    @Schema(description ="性别 来源字典")
    private String gender;

    /**
     * 床号
     */
    @Schema(description ="床号")
    private String bedNum;


    /**
     * 主治医生Id
     */
    @Schema(description ="主治医生Id")
    private Long docId;

    /**
     * 责任护士
     */
    @Schema(description ="责任护士")
    private Long nurseId;

    /**
     * 科室
     */
    @Schema(description ="deptId")
	@NotNull(message = "科室不能为空",groups = {PatInfoDTOADD.class,PatInfoDTOUpdate.class,PatInfoDTOIN.class})
    private Long deptId;

    /**
     * 住院时间
     */
    @Schema(description ="住院时间")
	@NotNull(message = "住院时间不能为空",groups = {PatInfoDTOADD.class,PatInfoDTOUpdate.class,PatInfoDTOIN.class})
	@Past(message = "住院时间不能为未来时间",groups = {PatInfoDTOADD.class,PatInfoDTOUpdate.class,PatInfoDTOIN.class})
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime iptTime;

    /**
     * 出院时间
     */
    @Schema(description ="leaveTime")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime leaveTime;

    /**
     * 出生日期
     */
    @Schema(description ="出生日期")
	@NotNull(message = "出生日期不能为空",groups = {PatInfoDTOADD.class,PatInfoDTOUpdate.class,PatInfoDTOIN.class})
	@Past(message = "出生日期不能为未来时间",groups = {PatInfoDTOADD.class,PatInfoDTOUpdate.class,PatInfoDTOIN.class})
	@DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;

    /**
     * 身高
     */
    @Schema(description ="身高")
    private Integer height;

    /**
     * 体重
     */
    @Schema(description ="体重")
    private Float weight;

    /**
     * 糖尿病类型：I型，II型，来源字典
     */
    @Schema(description ="糖尿病类型：I型，II型，来源字典")
    private String diabetesType;

    /**
     * 入院诊断
     */
    @Schema(description ="入院诊断")
    private String diagnosis;

    /**
     * 患者腕带条码值
     */
    @Schema(description ="患者腕带条码值")
    private String scanCode;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "入院开始时间")
    private LocalDateTime startTime;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "入院结束时间")
    private LocalDateTime endTime;

    public interface PatInfoDTOADD{}
	public interface PatInfoDTOUpdate{}
	public interface PatInfoDTODelete{}
	public interface PatInfoDTOLeave{}
	public interface PatInfoDTOIN{}

}
