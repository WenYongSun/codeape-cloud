
package com.code.ape.codeape.inhos.api.dto;

import com.code.ape.codeape.common.core.entity.BaseParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Schema(description = "转科管理")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PatTransferDTO extends BaseParam {

    /**
     * ID
     */
    @Schema(description ="ID")
    private Long id;

    /**
     * 患者id
     */
    @Schema(description ="患者id")
    private Long patId;

    /**
     * 就诊流水号，终身唯一
     */
    @Schema(description ="就诊流水号，终身唯一")
    private String visitId;

    @Schema(description = "患者姓名")
    private String patName;

    /**
     * 住院号
     */
    @Schema(description ="住院号")
    private String iptNum;

    @Schema(description = "原科室")
	private Long oldDept;

    @Schema(description = "新科室")
	private Long newDept;

    @Schema(description = "转科开始时间")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description="转科结束时间")
    private LocalDateTime endTime;

	public interface PatTransferDTOSearch{}

	public interface PatTransferDTOInfo{}


}
