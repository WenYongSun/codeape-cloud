package com.code.ape.codeape.inhos.api.entity.constant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description  出入院状态
 */
@Getter
@RequiredArgsConstructor
public enum PatInfoColdStatusEnum {
	COLD_YES(1,"已触发冷热分离"),

	COLD_NO(0,"未触发冷热分离");

	private final Integer value;

	private final String description;
}
