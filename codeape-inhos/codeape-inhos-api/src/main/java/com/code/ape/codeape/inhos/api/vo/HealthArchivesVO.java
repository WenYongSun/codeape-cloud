
package com.code.ape.codeape.inhos.api.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 控糖目标
 *
 * @author pig code generator
 * @date 2023-06-14 17:16:37
 */
@Data
@Schema(description = "控糖目标")
public class HealthArchivesVO {

    /**
     * id
     */
    @Schema(description ="id")
    private Long id;

    /**
     * 患者ID
     */
    @Schema(description ="患者ID")
    private Long patId;

    @Schema(description = "患者姓名")
    private String patName;

    @Schema(description = "患者住院号")
    private String iptNum;

    /**
     * 时段名称
     */
    @Schema(description ="时段名称")
    private String timeType;

    /**
     * 目标上限
     */
    @Schema(description ="目标上限")
    private Float valueUp;

    /**
     * 目标下限
     */
    @Schema(description ="目标下限")
    private Float valueDown;
}
