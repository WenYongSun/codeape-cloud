
package com.code.ape.codeape.inhos.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code.ape.codeape.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 住院患者
 *
 * @author pig code generator
 * @date 2023-06-02 11:34:04
 */
@Data
@TableName("pat_info_hot")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "住院患者")
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PatInfoHot extends BaseEntity {

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(description ="主键id")
    private Long id;

    /**
     * 患者住院号，每次住院唯一
     */
    @Schema(description ="患者住院号，每次住院唯一")
    private String iptNum;

    /**
     * 就诊流水号，终身唯一
     */
    @Schema(description ="就诊流水号，终身唯一")
    private String visitId;

    /**
     * 住院次数
     */
    @Schema(description ="住院次数")
    private Long visitTime;

    /**
     * 身份证号
     */
    @Schema(description ="身份证号")
    private String idCard;

    /**
     * 姓名
     */
    @Schema(description ="姓名")
    private String name;

    /**
     * 手机
     */
    @Schema(description ="手机")
    private String phone;

    /**
     * 性别 来源字典
     */
    @Schema(description ="性别 来源字典")
    private String gender;

    /**
     * 床号
     */
    @Schema(description ="床号")
    private String bedNum;

    /**
     * 正则后的床位，用于床位排序
     */
    @Schema(description ="正则后的床位，用于床位排序")
    private String bedSort;

    /**
     * 医院hos_id
     */
    @Schema(description ="医院hos_id")
    private Long hosId;

    /**
     * 主治医生Id
     */
    @Schema(description ="主治医生Id")
    private Long docId;

    /**
     * 责任护士
     */
    @Schema(description ="责任护士")
    private Long nurseId;

    /**
     * deptId
     */
    @Schema(description ="deptId")
    private Long deptId;

    /**
     * 住院时间
     */
    @Schema(description ="住院时间")
    private LocalDateTime iptTime;

    /**
     * 出院时间
     */
    @Schema(description ="leaveTime")
    private LocalDateTime leaveTime;

    /**
     * 出生日期
     */
    @Schema(description ="出生日期")
    private LocalDateTime birthday;

    /**
     * 身高
     */
    @Schema(description ="身高")
    private Integer height;

    /**
     * 体重
     */
    @Schema(description ="体重")
    private Float weight;

    /**
     * 糖尿病类型：I型，II型，来源字典
     */
    @Schema(description ="糖尿病类型：I型，II型，来源字典")
    private String diabetesType;

    /**
     * 入院诊断
     */
    @Schema(description ="入院诊断")
    private String diagnosis;

    /**
     * 患者腕带条码值
     */
    @Schema(description ="患者腕带条码值")
    private String scanCode;

    /**
     * 0 出院 1 在院
     */
    @Schema(description ="0 出院 1 在院")
    private Integer status;

    @Schema(description = "0 未触发冷热分离 1 已触发冷热分离")
    private Integer coldStatus;

    /**
     * 删除标记  -1：已删除  0：正常
     */
    @TableLogic
    @Schema(description ="删除标记  -1：已删除  0：正常")
    private String delFlag;
}
