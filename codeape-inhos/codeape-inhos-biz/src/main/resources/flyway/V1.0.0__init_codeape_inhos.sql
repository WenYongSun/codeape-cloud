/*
 Navicat Premium Data Transfer

 Source Server         : codeape-mysql
 Source Server Type    : MySQL
 Source Server Version : 50742
 Source Host           : codeape-mysql:3306
 Source Schema         : codeape_inhos

 Target Server Type    : MySQL
 Target Server Version : 50742
 File Encoding         : 65001

 Date: 11/06/2023 10:04:27
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for msg_record
-- ----------------------------
DROP TABLE IF EXISTS `msg_record`;
CREATE TABLE `msg_record`  (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '消息内容',
  `msg_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '消息ID',
  `topic` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主题',
  `exception_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '异常信息',
  `status` int(11) NOT NULL COMMENT '消息状态 1 已发送 2 发送成功 3 发送失败',
  `fail_num` int(1) NULL DEFAULT 0 COMMENT '发送失败的次数',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标记  -1：已删除  0：正常',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '消息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of msg_record
-- ----------------------------
INSERT INTO `msg_record` VALUES (1665959154133200898, '{\"bedNum\":\"23\",\"bedSort\":\"23\",\"birthday\":\"2010-06-07T00:00:00\",\"deptId\":1659386542775017473,\"diabetesType\":\"\",\"diagnosis\":\"\",\"docId\":1659441176889720833,\"gender\":\"1\",\"id\":1665959006283984897,\"idCard\":\"\",\"iptNum\":\"19238347\",\"iptTime\":\"2023-06-06T13:48:55\",\"leaveTime\":\"2023-06-06T13:49:48.952\",\"name\":\"张三丰\",\"nurseId\":1662432715555459074,\"phone\":\"\",\"scanCode\":\"\",\"status\":0,\"visitId\":\"1665959006229458945\"}', '7F0000014FB018B4AAC21CB787330000', 'pat_cold_topic', NULL, 2, 0, '0', '2023-06-06 13:49:49', 'admin', '2023-06-06 13:58:18', NULL);
INSERT INTO `msg_record` VALUES (1665975959799132162, '{\"bedNum\":\"12\",\"bedSort\":\"12\",\"birthday\":\"2019-06-10T00:00:00\",\"deptId\":1659789309465214978,\"diabetesType\":\"\",\"diagnosis\":\"\",\"docId\":1659441176889720833,\"gender\":\"1\",\"id\":1665975916706852866,\"idCard\":\"\",\"iptNum\":\"123455\",\"iptTime\":\"2019-06-17T00:00:00\",\"leaveTime\":\"2023-06-06T14:56:35.757\",\"name\":\"秦始皇\",\"nurseId\":1662432715555459074,\"phone\":\"\",\"scanCode\":\"\",\"status\":0,\"visitId\":\"1665975916539080705\"}', '7F000001618418B4AAC21D20C4D50001', 'pat_cold_topic', NULL, 2, 1, '0', '2023-06-06 14:56:36', 'admin', '2023-06-06 15:44:31', 'admin');
INSERT INTO `msg_record` VALUES (1665976518648197122, '{\"bedNum\":\"22\",\"bedSort\":\"22\",\"birthday\":\"2021-06-14T00:00:00\",\"deptId\":1659789309465214978,\"diabetesType\":\"\",\"diagnosis\":\"\",\"docId\":1659458202844733441,\"gender\":\"1\",\"id\":1665976484137463810,\"idCard\":\"\",\"iptNum\":\"192878\",\"iptTime\":\"2023-06-06T14:58:32\",\"leaveTime\":\"2023-06-06T14:58:49.037\",\"name\":\"小鬼子\",\"nurseId\":1662432715555459074,\"phone\":\"\",\"scanCode\":\"\",\"status\":0,\"visitId\":\"1665976484137463809\"}', '7F000001618418B4AAC21D20C4E60002', 'pat_cold_topic', NULL, 2, 1, '0', '2023-06-06 14:58:49', 'admin', '2023-06-06 15:44:31', 'admin');
INSERT INTO `msg_record` VALUES (1665981085297397762, '{\"bedNum\":\"11\",\"bedSort\":\"11\",\"birthday\":\"2020-06-15T00:00:00\",\"deptId\":1659386542775017473,\"diabetesType\":\"\",\"diagnosis\":\"\",\"docId\":1659441176889720833,\"gender\":\"1\",\"id\":1665981054884499457,\"idCard\":\"\",\"iptNum\":\"123444\",\"iptTime\":\"2022-06-06T00:00:00\",\"leaveTime\":\"2023-06-06T15:16:57.666\",\"name\":\"董天宝\",\"nurseId\":1662432715555459074,\"phone\":\"\",\"scanCode\":\"\",\"status\":0,\"visitId\":\"1665981054817390594\"}', '7F000001618418B4AAC21D20B2360000', 'pat_cold_topic', NULL, 2, 1, '0', '2023-06-06 15:16:58', 'admin', '2023-06-06 15:44:26', 'admin');
INSERT INTO `msg_record` VALUES (1665983364045934594, '{\"bedNum\":\"\",\"bedSort\":\"\",\"birthday\":\"2021-06-07T00:00:00\",\"deptId\":1659386542775017473,\"diabetesType\":\"\",\"diagnosis\":\"\",\"gender\":\"1\",\"id\":1665983308198776835,\"idCard\":\"\",\"iptNum\":\"19287834\",\"iptTime\":\"2020-06-07T00:00:00\",\"leaveTime\":\"2023-06-06T15:26:01.043\",\"name\":\"汉武帝\",\"phone\":\"\",\"scanCode\":\"\",\"status\":0,\"visitId\":\"1665983308198776834\"}', '7F000001354818B4AAC21D0F97C60000', 'pat_cold_topic', NULL, 2, 1, '0', '2023-06-06 15:26:01', 'admin', '2023-06-06 15:25:45', 'admin');
INSERT INTO `msg_record` VALUES (1666319836146028545, '{\"bedNum\":\"12\",\"bedSort\":\"12\",\"birthday\":\"2015-06-15T00:00:00\",\"deptId\":1659789309465214978,\"diabetesType\":\"\",\"diagnosis\":\"\",\"docId\":1659441176889720833,\"gender\":\"1\",\"id\":1666298635495378946,\"idCard\":\"\",\"iptNum\":\"123884\",\"iptTime\":\"2019-06-03T12:18:28\",\"leaveTime\":\"2023-06-07T13:43:02.125\",\"name\":\"李斯\",\"nurseId\":1662432715555459074,\"phone\":\"\",\"scanCode\":\"\",\"status\":0,\"visitId\":\"1666298635466018818\"}', '7F0000011B6818B4AAC221D7B0540000', 'pat_cold_topic', NULL, 2, 1, '0', '2023-06-07 13:43:02', 'admin', '2023-06-07 13:42:48', 'admin');
INSERT INTO `msg_record` VALUES (1666353593527791618, '1666298635495378946', '7F000001592418B4AAC22252828E0000', 'pat_hot_topic', NULL, 2, 1, '0', '2023-06-07 15:57:11', 'admin', '2023-06-07 15:56:57', 'admin');
INSERT INTO `msg_record` VALUES (1666354951823781889, '1665983308198776835', '7F000001592418B4AAC222576BD70001', 'pat_hot_topic', NULL, 2, 1, '0', '2023-06-07 16:02:35', 'admin', '2023-06-07 16:02:19', 'admin');
INSERT INTO `msg_record` VALUES (1666358984147955713, '1665981054884499457', '7F0000014FDC18B4AAC222661BCF0000', 'pat_hot_topic', NULL, 2, 1, '0', '2023-06-07 16:18:36', 'admin', '2023-06-07 16:18:21', 'admin');
INSERT INTO `msg_record` VALUES (1666362864281784321, '1665975916706852866', '7F00000162A818B4AAC222743B5D0000', 'pat_hot_topic', NULL, 2, 1, '0', '2023-06-07 16:34:01', 'admin', '2023-06-07 16:33:47', 'admin');
INSERT INTO `msg_record` VALUES (1666372395653091330, '1665959134235422723', '7F0000015C2418B4AAC22296E1D10000', 'pat_topic:TAG_HOT', NULL, 2, 1, '0', '2023-06-07 17:11:53', 'admin', '2023-06-07 17:11:38', 'admin');
INSERT INTO `msg_record` VALUES (1666374987837833217, '1665976484137463810', '7F0000014EB418B4AAC222A0547D0000', 'pat_topic:TAG_HOT', NULL, 2, 1, '0', '2023-06-07 17:22:12', 'admin', '2023-06-07 17:22:02', 'admin');
INSERT INTO `msg_record` VALUES (1666375089717477378, '1665973871627141122', '7F0000014EB418B4AAC222A0AEA10001', 'pat_topic:TAG_HOT', NULL, 2, 1, '0', '2023-06-07 17:22:36', 'admin', '2023-06-07 17:22:20', 'admin');
INSERT INTO `msg_record` VALUES (1666411547350233089, '1665972505156452354', '7F00000120F818B4AAC223255BF40000', 'pat_topic:TAG_HOT', NULL, 2, 1, '0', '2023-06-07 19:47:28', 'admin', '2023-06-07 19:47:15', 'admin');
INSERT INTO `msg_record` VALUES (1666411768704626689, '{\"bedNum\":\"\",\"bedSort\":\"\",\"birthday\":\"2021-06-07T00:00:00\",\"deptId\":1659386542775017473,\"diabetesType\":\"\",\"diagnosis\":\"\",\"gender\":\"1\",\"id\":1665983308198776835,\"idCard\":\"\",\"iptNum\":\"1928783412\",\"iptTime\":\"2020-06-07T02:02:00\",\"leaveTime\":\"2023-06-07T19:48:20.655\",\"name\":\"汉武帝\",\"phone\":\"\",\"scanCode\":\"\",\"status\":0,\"visitId\":\"1665983308198776834\"}', '7F00000120F818B4AAC223261EA10001', 'pat_topic:TAG_COLD', NULL, 2, 1, '0', '2023-06-07 19:48:21', 'admin', '2023-06-07 19:48:05', 'admin');
INSERT INTO `msg_record` VALUES (1666411977996201986, '{\"bedNum\":\"11\",\"bedSort\":\"11\",\"birthday\":\"2020-06-15T00:00:00\",\"deptId\":1659386542775017473,\"diabetesType\":\"\",\"diagnosis\":\"\",\"docId\":1659441176889720833,\"gender\":\"1\",\"id\":1665981054884499457,\"idCard\":\"\",\"iptNum\":\"123444\",\"iptTime\":\"2022-06-06T00:00:00\",\"leaveTime\":\"2023-06-07T19:49:10.604\",\"name\":\"董天宝\",\"nurseId\":1662432715555459074,\"phone\":\"\",\"scanCode\":\"\",\"status\":0,\"visitId\":\"1665981054817390594\"}', '7F00000120F818B4AAC22326E1970002', 'pat_topic:TAG_COLD', NULL, 2, 1, '0', '2023-06-07 19:49:11', 'admin', '2023-06-07 19:48:55', 'admin');
INSERT INTO `msg_record` VALUES (1666411988855255041, '{\"bedNum\":\"12\",\"bedSort\":\"12\",\"birthday\":\"2015-06-15T00:00:00\",\"deptId\":1659386542775017473,\"diabetesType\":\"\",\"diagnosis\":\"\",\"docId\":1659887681463345154,\"gender\":\"1\",\"id\":1666298635495378946,\"idCard\":\"\",\"iptNum\":\"123884233\",\"iptTime\":\"2019-06-03T12:18:28\",\"leaveTime\":\"2023-06-07T19:49:13.184\",\"name\":\"李斯\",\"nurseId\":1662432715555459074,\"phone\":\"\",\"scanCode\":\"\",\"status\":0,\"visitId\":\"1666298635466018818\"}', '7F00000120F818B4AAC22326EBB70003', 'pat_topic:TAG_COLD', NULL, 2, 1, '0', '2023-06-07 19:49:13', 'admin', '2023-06-07 19:48:57', 'admin');
INSERT INTO `msg_record` VALUES (1666411999143882753, '{\"bedNum\":\"12\",\"bedSort\":\"12\",\"birthday\":\"2019-06-10T00:00:00\",\"deptId\":1659789309465214978,\"diabetesType\":\"\",\"diagnosis\":\"\",\"docId\":1659441176889720833,\"gender\":\"1\",\"id\":1665975916706852866,\"idCard\":\"\",\"iptNum\":\"123455\",\"iptTime\":\"2019-06-17T00:00:00\",\"leaveTime\":\"2023-06-07T19:49:15.662\",\"name\":\"秦始皇\",\"nurseId\":1662432715555459074,\"phone\":\"\",\"scanCode\":\"\",\"status\":0,\"visitId\":\"1665975916539080705\"}', '7F00000120F818B4AAC22326F5410004', 'pat_topic:TAG_COLD', NULL, 2, 1, '0', '2023-06-07 19:49:16', 'admin', '2023-06-07 19:49:00', 'admin');
INSERT INTO `msg_record` VALUES (1666412009726111745, '{\"bedNum\":\"12\",\"bedSort\":\"12\",\"birthday\":\"2016-06-06T00:00:00\",\"deptId\":1659789309465214978,\"diabetesType\":\"1\",\"diagnosis\":\"\",\"docId\":1659441176889720833,\"gender\":\"1\",\"id\":1665959134235422723,\"idCard\":\"\",\"iptNum\":\"82736\",\"iptTime\":\"2023-06-06T13:49:32\",\"leaveTime\":\"2023-06-07T19:49:18.181\",\"name\":\"步惊云\",\"nurseId\":1662432715555459074,\"phone\":\"\",\"scanCode\":\"\",\"status\":0,\"visitId\":\"1665959134235422722\"}', '7F00000120F818B4AAC22326FF1E0005', 'pat_topic:TAG_COLD', NULL, 2, 1, '0', '2023-06-07 19:49:18', 'admin', '2023-06-07 19:49:02', 'admin');
INSERT INTO `msg_record` VALUES (1666412020346089473, '{\"bedNum\":\"22\",\"bedSort\":\"22\",\"birthday\":\"2019-06-18T00:00:00\",\"deptId\":1659386542775017473,\"diabetesType\":\"1\",\"diagnosis\":\"\",\"docId\":1659458202844733441,\"gender\":\"1\",\"id\":1665973871627141122,\"idCard\":\"\",\"iptNum\":\"192837\",\"iptTime\":\"2023-06-06T14:48:00\",\"leaveTime\":\"2023-06-07T19:49:20.713\",\"name\":\"刘伯温\",\"nurseId\":1662432715555459074,\"phone\":\"\",\"scanCode\":\"\",\"status\":0,\"visitId\":\"1665973871627141121\"}', '7F00000120F818B4AAC2232708FA0006', 'pat_topic:TAG_COLD', NULL, 2, 1, '0', '2023-06-07 19:49:21', 'admin', '2023-06-07 19:49:05', 'admin');
INSERT INTO `msg_record` VALUES (1666412030005571586, '{\"bedNum\":\"22\",\"bedSort\":\"22\",\"birthday\":\"2021-06-14T00:00:00\",\"deptId\":1659789309465214978,\"diabetesType\":\"\",\"diagnosis\":\"\",\"docId\":1659458202844733441,\"gender\":\"1\",\"id\":1665976484137463810,\"idCard\":\"\",\"iptNum\":\"192878\",\"iptTime\":\"2023-06-06T14:58:32\",\"leaveTime\":\"2023-06-07T19:49:23.028\",\"name\":\"小鬼子\",\"nurseId\":1662432715555459074,\"phone\":\"\",\"scanCode\":\"\",\"status\":0,\"visitId\":\"1665976484137463809\"}', '7F00000120F818B4AAC2232712010007', 'pat_topic:TAG_COLD', NULL, 2, 1, '0', '2023-06-07 19:49:23', 'admin', '2023-06-07 19:49:07', 'admin');
INSERT INTO `msg_record` VALUES (1666412039442755586, '{\"bedNum\":\"24\",\"bedSort\":\"24\",\"birthday\":\"2009-06-15T00:00:00\",\"deptId\":1659789309465214978,\"diabetesType\":\"1\",\"diagnosis\":\"\",\"docId\":1659441176889720833,\"gender\":\"1\",\"id\":1665972505156452354,\"idCard\":\"\",\"iptNum\":\"928347\",\"iptTime\":\"2023-04-03T00:00:00\",\"leaveTime\":\"2023-06-07T19:49:25.278\",\"name\":\"朱元璋\",\"nurseId\":1662432715555459074,\"phone\":\"\",\"scanCode\":\"\",\"status\":0,\"visitId\":\"1665972505106120706\"}', '7F00000120F818B4AAC223271AC60008', 'pat_topic:TAG_COLD', NULL, 2, 1, '0', '2023-06-07 19:49:25', 'admin', '2023-06-07 19:49:09', 'admin');
INSERT INTO `msg_record` VALUES (1666412524375601153, '1665972505156452354', '7F00000120F818B4AAC22328DE6C0009', 'pat_topic:TAG_HOT', NULL, 2, 1, '0', '2023-06-07 19:51:21', 'admin', '2023-06-07 19:51:05', 'admin');
INSERT INTO `msg_record` VALUES (1667162758366044161, '1665976484137463810', '7F00000132EC18B4AAC22DD238330000', 'pat_topic:TAG_HOT', NULL, 2, 1, '0', '2023-06-09 21:32:31', 'admin', '2023-06-09 21:32:17', 'admin');
INSERT INTO `msg_record` VALUES (1667162817518313474, '1665973871627141122', '7F00000132EC18B4AAC22DD26B620001', 'pat_topic:TAG_HOT', NULL, 2, 1, '0', '2023-06-09 21:32:45', 'admin', '2023-06-09 21:32:30', 'admin');
INSERT INTO `msg_record` VALUES (1667162868361666562, '1665959134235422723', '7F00000132EC18B4AAC22DD29AA90002', 'pat_topic:TAG_HOT', NULL, 2, 1, '0', '2023-06-09 21:32:57', 'admin', '2023-06-09 21:32:42', 'admin');
INSERT INTO `msg_record` VALUES (1667162888968282114, '1665975916706852866', '7F00000132EC18B4AAC22DD2ADE10003', 'pat_topic:TAG_HOT', NULL, 2, 1, '0', '2023-06-09 21:33:02', 'admin', '2023-06-09 21:32:47', 'admin');
INSERT INTO `msg_record` VALUES (1667162906966040578, '1666298635495378946', '7F00000132EC18B4AAC22DD2BEA00004', 'pat_topic:TAG_HOT', NULL, 2, 1, '0', '2023-06-09 21:33:06', 'admin', '2023-06-09 21:32:52', 'admin');
INSERT INTO `msg_record` VALUES (1667162922271055873, '1665981054884499457', '7F00000132EC18B4AAC22DD2CCDF0005', 'pat_topic:TAG_HOT', NULL, 2, 1, '0', '2023-06-09 21:33:10', 'admin', '2023-06-09 21:32:55', 'admin');
INSERT INTO `msg_record` VALUES (1667162938926637057, '1665983308198776835', '7F00000132EC18B4AAC22DD2DC620006', 'pat_topic:TAG_HOT', NULL, 2, 1, '0', '2023-06-09 21:33:14', 'admin', '2023-06-09 21:32:59', 'admin');
INSERT INTO `msg_record` VALUES (1667443381391458306, '{\"bedNum\":\"\",\"bedSort\":\"\",\"birthday\":\"2021-06-07T00:00:00\",\"deptId\":1659386264763965441,\"diabetesType\":\"\",\"diagnosis\":\"\",\"docId\":1659458202844733441,\"gender\":\"1\",\"id\":1665983308198776835,\"idCard\":\"\",\"iptNum\":\"1928783412\",\"iptTime\":\"2020-06-07T02:02:00\",\"leaveTime\":\"2023-06-07T19:48:20\",\"name\":\"汉武帝\",\"phone\":\"\",\"scanCode\":\"\",\"status\":0,\"visitId\":\"1665983308198776834\"}', '7F0000011A0018B4AAC231CF1DB60000', 'pat_topic:TAG_COLD', NULL, 2, 1, '0', '2023-06-10 16:07:36', 'admin', '2023-06-10 16:07:24', 'admin');

-- ----------------------------
-- Table structure for pat_info_cold
-- ----------------------------
DROP TABLE IF EXISTS `pat_info_cold`;
CREATE TABLE `pat_info_cold`  (
  `id` bigint(20) NOT NULL COMMENT '主键id',
  `ipt_num` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '患者住院号，每次住院唯一',
  `visit_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '就诊流水号，终身唯一',
  `visit_time` bigint(20) NULL DEFAULT 0 COMMENT '住院次数',
  `id_card` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `phone` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机',
  `gender` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别 来源字典',
  `bed_num` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '床号',
  `bed_sort` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '正则后的床位，用于床位排序',
  `hos_id` bigint(20) NOT NULL DEFAULT 1 COMMENT '医院hos_id',
  `doc_id` bigint(20) NULL DEFAULT NULL COMMENT '主治医生Id',
  `nurse_id` bigint(20) NULL DEFAULT NULL COMMENT '责任护士',
  `dept_id` bigint(20) NOT NULL,
  `ipt_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '住院时间',
  `leave_time` datetime(0) NULL DEFAULT NULL,
  `birthday` date NULL DEFAULT NULL COMMENT '出生日期',
  `height` int(11) NULL DEFAULT NULL COMMENT '身高',
  `weight` float(4, 1) NULL DEFAULT NULL COMMENT '体重',
  `diabetes_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '糖尿病类型：I型，II型，来源字典',
  `diagnosis` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '入院诊断',
  `scan_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '患者腕带条码值',
  `status` int(1) NULL DEFAULT 0 COMMENT '0 出院 1 在院',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标记  -1：已删除  0：正常',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `phone`(`phone`) USING BTREE,
  INDEX `ipt_num`(`ipt_num`) USING BTREE,
  INDEX `dept_id`(`dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '住院患者表-冷数据' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pat_info_cold
-- ----------------------------
INSERT INTO `pat_info_cold` VALUES (1665983308198776835, '1928783412', '1665983308198776834', 2, '', '汉武帝', '', '1', '', '', 1659018792143663105, 1659458202844733441, NULL, 1659386264763965441, '2020-06-07 02:02:00', '2023-06-07 19:48:20', '2021-06-07', NULL, NULL, '', '', '', 0, '0', '2023-06-09 21:33:14', 'admin', '2023-06-09 21:34:34', 'admin');

-- ----------------------------
-- Table structure for pat_info_hot
-- ----------------------------
DROP TABLE IF EXISTS `pat_info_hot`;
CREATE TABLE `pat_info_hot`  (
  `id` bigint(20) NOT NULL COMMENT '主键id',
  `ipt_num` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '患者住院号，每次住院唯一',
  `visit_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '就诊流水号，终身唯一',
  `visit_time` bigint(20) NULL DEFAULT 0 COMMENT '住院次数',
  `id_card` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `phone` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机',
  `gender` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别 来源字典',
  `bed_num` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '床号',
  `bed_sort` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '正则后的床位，用于床位排序',
  `hos_id` bigint(20) NOT NULL DEFAULT 1 COMMENT '医院hos_id',
  `doc_id` bigint(20) NULL DEFAULT NULL COMMENT '主治医生Id',
  `nurse_id` bigint(20) NULL DEFAULT NULL COMMENT '责任护士',
  `dept_id` bigint(20) NOT NULL COMMENT '科室ID',
  `ipt_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '住院时间',
  `leave_time` datetime(0) NULL DEFAULT NULL COMMENT '出院时间',
  `birthday` date NULL DEFAULT NULL COMMENT '出生日期',
  `height` int(11) NULL DEFAULT NULL COMMENT '身高',
  `weight` float(4, 1) NULL DEFAULT NULL COMMENT '体重',
  `diabetes_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '糖尿病类型：I型，II型，来源字典',
  `diagnosis` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '入院诊断',
  `scan_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '患者腕带条码值',
  `status` int(1) NULL DEFAULT 0 COMMENT '0 出院 1 在院',
  `cold_status` int(1) NULL DEFAULT 0 COMMENT '0 未触发冷热分离 1 已触发冷热分离',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标记  -1：已删除  0：正常',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `phone`(`phone`) USING BTREE,
  INDEX `ipt_num`(`ipt_num`) USING BTREE,
  INDEX `dept_id`(`dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '住院患者表-热数据' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pat_info_hot
-- ----------------------------
INSERT INTO `pat_info_hot` VALUES (1665959134235422723, '82736', '1665959134235422722', 2, '', '步惊云', '', '1', '12', '12', 1659018792143663105, 1659441176889720833, 1662432715555459074, 1659789309465214978, '2023-06-06 13:49:32', '2023-06-07 19:49:18', '2016-06-06', NULL, NULL, '1', '', '', 1, 0, '0', '2023-06-09 21:32:57', 'admin', '2023-06-09 21:32:57', 'admin');
INSERT INTO `pat_info_hot` VALUES (1665972505156452354, '9283471234', '1665972505106120706', 2, '', '朱元璋', '', '1', '24', '24', 1659018792143663105, 1659441176889720833, 1662432715555459074, 1659789309465214978, '2023-04-03 00:00:00', '2023-06-07 19:49:25', '2009-06-15', NULL, NULL, '1', '', '', 1, 0, '0', '2023-06-07 19:51:21', 'admin', '2023-06-07 19:51:21', 'admin');
INSERT INTO `pat_info_hot` VALUES (1665973871627141122, '192837', '1665973871627141121', 2, '', '刘伯温', '', '1', '22', '22', 1659018792143663105, 1659458202844733441, 1662432715555459074, 1659386542775017473, '2023-06-06 14:48:00', '2023-06-07 19:49:20', '2019-06-18', NULL, NULL, '1', '', '', 1, 0, '0', '2023-06-09 21:32:45', 'admin', '2023-06-09 21:32:45', 'admin');
INSERT INTO `pat_info_hot` VALUES (1665975916706852866, '123455', '1665975916539080705', 2, '', '秦始皇', '', '1', '12', '12', 1659018792143663105, 1659441176889720833, 1662432715555459074, 1659789309465214978, '2019-06-17 00:00:00', '2023-06-07 19:49:15', '2019-06-10', NULL, NULL, '', '', '', 1, 0, '0', '2023-06-09 21:33:02', 'admin', '2023-06-09 21:33:02', 'admin');
INSERT INTO `pat_info_hot` VALUES (1665976484137463810, '192878', '1665976484137463809', 2, '', '小鬼子', '', '1', '22', '22', 1659018792143663105, 1659458202844733441, 1662432715555459074, 1659789309465214978, '2023-06-06 14:58:32', '2023-06-07 19:49:23', '2021-06-14', NULL, NULL, '', '', '', 1, 0, '0', '2023-06-09 21:32:31', 'admin', '2023-06-09 21:32:31', 'admin');
INSERT INTO `pat_info_hot` VALUES (1665981054884499457, '123444', '1665981054817390594', 2, '', '董天宝', '', '1', '11', '11', 1659018792143663105, 1659441176889720833, 1662432715555459074, 1659789309465214978, '2022-06-06 00:00:00', '2023-06-07 19:49:10', '2020-06-15', NULL, NULL, '', '', '', 1, 0, '0', '2023-06-09 21:33:10', 'admin', '2023-06-10 16:10:46', 'admin');
INSERT INTO `pat_info_hot` VALUES (1666298635495378946, '123884233', '1666298635466018818', 2, '', '李斯', '', '1', '12', '12', 1659018792143663105, 1659887681463345154, 1662432715555459074, 1659386542775017473, '2019-06-03 12:18:28', '2023-06-07 19:49:13', '2015-06-15', NULL, NULL, '', '', '', 1, 0, '0', '2023-06-09 21:33:06', 'admin', '2023-06-09 21:33:06', 'admin');

-- ----------------------------
-- Table structure for pat_record
-- ----------------------------
DROP TABLE IF EXISTS `pat_record`;
CREATE TABLE `pat_record`  (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `pat_id` bigint(20) NOT NULL COMMENT '患者ID',
  `visit_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '就诊流水号，终身唯一',
  `hos_id` bigint(20) NULL DEFAULT NULL COMMENT '住院的医院id',
  `ipt_num` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '住院号',
  `bed_num` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '床位号',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '科室ID',
  `doc_id` bigint(20) NULL DEFAULT NULL COMMENT '主治医生ID',
  `nurse_id` bigint(20) NULL DEFAULT NULL COMMENT '责任护士id',
  `ipt_time` datetime(0) NULL DEFAULT NULL COMMENT '入院时间',
  `leave_time` datetime(0) NULL DEFAULT NULL COMMENT '出院时间',
  `comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ipt_type` int(1) NOT NULL DEFAULT 1 COMMENT '出入院类型：0：出院；1：入院',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标记  -1：已删除  0：正常',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `hos_id`(`hos_id`) USING BTREE,
  INDEX `pat_id_index`(`pat_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '住院记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pat_record
-- ----------------------------
INSERT INTO `pat_record` VALUES (1665959006414008322, 1665959006283984897, '1665959006229458945', 1659018792143663105, '19238347', '23', 1659386542775017473, 1659441176889720833, 1662432715555459074, '2023-06-06 13:48:55', '2023-06-06 13:49:49', NULL, 0, '0', '2023-06-06 13:49:14', 'admin', '2023-06-06 13:49:49', 'admin');
INSERT INTO `pat_record` VALUES (1665959134302531586, 1665959134235422723, '1665959134235422722', 1659018792143663105, '82736', '12', 1659789309465214978, 1659441176889720833, 1662432715555459074, '2023-06-06 13:49:32', '2023-06-06 14:38:51', NULL, 0, '0', '2023-06-06 13:49:44', 'admin', '2023-06-06 14:38:52', 'admin');
INSERT INTO `pat_record` VALUES (1665972505215172610, 1665972505156452354, '1665972505106120706', 1659018792143663105, '928347', '24', 1659789309465214978, 1659441176889720833, 1662432715555459074, '2023-04-03 00:00:00', '2023-06-06 14:42:56', NULL, 0, '0', '2023-06-06 14:42:52', 'admin', '2023-06-06 14:42:56', 'admin');
INSERT INTO `pat_record` VALUES (1665973871690055682, 1665973871627141122, '1665973871627141121', 1659018792143663105, '192837', '22', 1659386542775017473, 1659458202844733441, 1662432715555459074, '2023-06-06 14:48:00', '2023-06-06 14:48:47', NULL, 0, '0', '2023-06-06 14:48:18', 'admin', '2023-06-06 14:48:47', 'admin');
INSERT INTO `pat_record` VALUES (1665975916966899713, 1665975916706852866, '1665975916539080705', 1659018792143663105, '123455', '12', 1659789309465214978, 1659441176889720833, 1662432715555459074, '2019-06-17 00:00:00', '2023-06-06 14:56:36', NULL, 0, '0', '2023-06-06 14:56:26', 'admin', '2023-06-06 14:56:36', 'admin');
INSERT INTO `pat_record` VALUES (1665976484267487234, 1665976484137463810, '1665976484137463809', 1659018792143663105, '192878', '22', 1659789309465214978, 1659458202844733441, 1662432715555459074, '2023-06-06 14:58:32', '2023-06-06 14:58:49', NULL, 0, '0', '2023-06-06 14:58:41', 'admin', '2023-06-06 14:58:49', 'admin');
INSERT INTO `pat_record` VALUES (1665981054951608321, 1665981054884499457, '1665981054817390594', 1659018792143663105, '123444', '11', 1659386542775017473, 1659441176889720833, 1662432715555459074, '2022-06-06 00:00:00', '2023-06-06 15:16:58', NULL, 0, '0', '2023-06-06 15:16:51', 'admin', '2023-06-06 15:16:58', 'admin');
INSERT INTO `pat_record` VALUES (1665983308219748354, 1665983308198776835, '1665983308198776834', 1659018792143663105, '19287834', '', 1659386542775017473, NULL, NULL, '2020-06-07 00:00:00', '2023-06-06 15:26:01', NULL, 0, '0', '2023-06-06 15:25:48', 'admin', '2023-06-06 15:26:01', 'admin');
INSERT INTO `pat_record` VALUES (1666298635684122626, 1666298635495378946, '1666298635466018818', 1659018792143663105, '123884', '12', 1659789309465214978, 1659441176889720833, 1662432715555459074, '2019-06-03 12:18:28', '2023-06-07 13:43:02', NULL, 0, '0', '2023-06-07 12:18:48', 'admin', '2023-06-07 13:43:02', 'admin');
INSERT INTO `pat_record` VALUES (1666353593032863746, 1666298635495378946, '1666298635466018818', 1659018792143663105, '123884233', '12', 1659386542775017473, 1659887681463345154, 1662432715555459074, '2019-06-03 12:18:28', '2023-06-07 19:49:13', NULL, 0, '0', '2023-06-07 15:57:11', 'admin', '2023-06-07 19:49:13', 'admin');
INSERT INTO `pat_record` VALUES (1666354951752478722, 1665983308198776835, '1665983308198776834', 1659018792143663105, '1928783412', '', 1659386542775017473, NULL, NULL, '2020-06-07 02:02:00', '2023-06-07 19:48:21', NULL, 0, '0', '2023-06-07 16:02:35', 'admin', '2023-06-07 19:48:21', 'admin');
INSERT INTO `pat_record` VALUES (1666358984051486722, 1665981054884499457, '1665981054817390594', 1659018792143663105, '123444', '11', 1659386542775017473, 1659441176889720833, 1662432715555459074, '2022-06-06 00:00:00', '2023-06-07 19:49:11', NULL, 0, '0', '2023-06-07 16:18:36', 'admin', '2023-06-07 19:49:11', 'admin');
INSERT INTO `pat_record` VALUES (1666362864093040641, 1665975916706852866, '1665975916539080705', 1659018792143663105, '123455', '12', 1659789309465214978, 1659441176889720833, 1662432715555459074, '2019-06-17 00:00:00', '2023-06-07 19:49:16', NULL, 0, '0', '2023-06-07 16:34:01', 'admin', '2023-06-07 19:49:16', 'admin');
INSERT INTO `pat_record` VALUES (1666372395153969154, 1665959134235422723, '1665959134235422722', 1659018792143663105, '82736', '12', 1659789309465214978, 1659441176889720833, 1662432715555459074, '2023-06-06 13:49:32', '2023-06-07 19:49:18', NULL, 0, '0', '2023-06-07 17:11:53', 'admin', '2023-06-07 19:49:18', 'admin');
INSERT INTO `pat_record` VALUES (1666374987753947137, 1665976484137463810, '1665976484137463809', 1659018792143663105, '192878', '22', 1659789309465214978, 1659458202844733441, 1662432715555459074, '2023-06-06 14:58:32', '2023-06-07 19:49:23', NULL, 0, '0', '2023-06-07 17:22:11', 'admin', '2023-06-07 19:49:23', 'admin');
INSERT INTO `pat_record` VALUES (1666375089650368514, 1665973871627141122, '1665973871627141121', 1659018792143663105, '192837', '22', 1659386542775017473, 1659458202844733441, 1662432715555459074, '2023-06-06 14:48:00', '2023-06-07 19:49:21', NULL, 0, '0', '2023-06-07 17:22:36', 'admin', '2023-06-07 19:49:21', 'admin');
INSERT INTO `pat_record` VALUES (1666411546670755842, 1665972505156452354, '1665972505106120706', 1659018792143663105, '928347', '24', 1659789309465214978, 1659441176889720833, 1662432715555459074, '2023-04-03 00:00:00', '2023-06-07 19:49:25', NULL, 0, '0', '2023-06-07 19:47:28', 'admin', '2023-06-07 19:49:25', 'admin');
INSERT INTO `pat_record` VALUES (1666412524182663170, 1665972505156452354, '1665972505106120706', 1659018792143663105, '9283471234', '24', 1659789309465214978, 1659441176889720833, 1662432715555459074, '2023-04-03 00:00:00', NULL, NULL, 1, '0', '2023-06-07 19:51:21', 'admin', '2023-06-07 19:51:21', 'admin');
INSERT INTO `pat_record` VALUES (1667162758277963778, 1665976484137463810, '1665976484137463809', 1659018792143663105, '192878', '22', 1659789309465214978, 1659458202844733441, 1662432715555459074, '2023-06-06 14:58:32', NULL, NULL, 1, '0', '2023-06-09 21:32:31', 'admin', '2023-06-09 21:32:31', 'admin');
INSERT INTO `pat_record` VALUES (1667162817451204610, 1665973871627141122, '1665973871627141121', 1659018792143663105, '192837', '22', 1659386542775017473, 1659458202844733441, 1662432715555459074, '2023-06-06 14:48:00', NULL, NULL, 1, '0', '2023-06-09 21:32:45', 'admin', '2023-06-09 21:32:45', 'admin');
INSERT INTO `pat_record` VALUES (1667162868231643137, 1665959134235422723, '1665959134235422722', 1659018792143663105, '82736', '12', 1659789309465214978, 1659441176889720833, 1662432715555459074, '2023-06-06 13:49:32', NULL, NULL, 1, '0', '2023-06-09 21:32:57', 'admin', '2023-06-09 21:32:57', 'admin');
INSERT INTO `pat_record` VALUES (1667162888834064385, 1665975916706852866, '1665975916539080705', 1659018792143663105, '123455', '12', 1659789309465214978, 1659441176889720833, 1662432715555459074, '2019-06-17 00:00:00', NULL, NULL, 1, '0', '2023-06-09 21:33:02', 'admin', '2023-06-09 21:33:02', 'admin');
INSERT INTO `pat_record` VALUES (1667162906903126017, 1666298635495378946, '1666298635466018818', 1659018792143663105, '123884233', '12', 1659386542775017473, 1659887681463345154, 1662432715555459074, '2019-06-03 12:18:28', NULL, NULL, 1, '0', '2023-06-09 21:33:06', 'admin', '2023-06-09 21:33:06', 'admin');
INSERT INTO `pat_record` VALUES (1667162922203947010, 1665981054884499457, '1665981054817390594', 1659018792143663105, '123444', '11', 1659386542775017473, 1659441176889720833, 1662432715555459074, '2022-06-06 00:00:00', NULL, NULL, 1, '0', '2023-06-09 21:33:10', 'admin', '2023-06-09 21:33:10', 'admin');
INSERT INTO `pat_record` VALUES (1667162938863722497, 1665983308198776835, '1665983308198776834', 1659018792143663105, '1928783412', '', 1659386542775017473, NULL, NULL, '2020-06-07 02:02:00', '2023-06-07 19:48:20', NULL, 0, '0', '2023-06-09 21:33:14', 'admin', '2023-06-10 16:07:36', 'admin');

-- ----------------------------
-- Table structure for pat_transfer
-- ----------------------------
DROP TABLE IF EXISTS `pat_transfer`;
CREATE TABLE `pat_transfer`  (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `hos_id` bigint(20) NOT NULL COMMENT '医院id',
  `pat_id` bigint(32) NOT NULL COMMENT '患者id',
  `visit_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '就诊流水号，终身唯一',
  `ipt_num` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '住院号',
  `bed_num` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '床号',
  `transfer_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '转科时间',
  `old_dept` bigint(20) NOT NULL COMMENT '原科室',
  `new_dept` bigint(20) NOT NULL COMMENT '当前科室',
  `doc_id` bigint(20) NULL DEFAULT NULL COMMENT '责任医生的id',
  `nurse_id` bigint(20) NULL DEFAULT NULL COMMENT '责任护士id',
  `remark` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标记  -1：已删除  0：正常',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `hos_id`(`hos_id`) USING BTREE,
  INDEX `user_id`(`ipt_num`) USING BTREE,
  INDEX `patient_id`(`pat_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '转科管理表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pat_transfer
-- ----------------------------
INSERT INTO `pat_transfer` VALUES (1667163276903653377, 1659018792143663105, 1665983308198776835, '1665983308198776834', '1928783412', '', '2023-06-09 21:34:34', 1659386542775017473, 1659386264763965441, 1659458202844733441, NULL, NULL, '0', '2023-06-09 21:34:34', 'admin', '2023-06-09 21:34:34', 'admin');
INSERT INTO `pat_transfer` VALUES (1667444177956896770, 1659018792143663105, 1665981054884499457, '1665981054817390594', '123444', '11', '2023-06-10 16:10:46', 1659386542775017473, 1659789309465214978, 1659441176889720833, 1662432715555459074, NULL, '0', '2023-06-10 16:10:46', 'admin', '2023-06-10 16:10:46', 'admin');

SET FOREIGN_KEY_CHECKS = 1;
