

package com.code.ape.codeape.inhos.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.inhos.api.entity.PatRecord;

/**
 * 患者出入院记录
 *
 * @author pig code generator
 * @date 2023-06-02 17:00:26
 */
public interface PatRecordService extends IService<PatRecord> {

}
