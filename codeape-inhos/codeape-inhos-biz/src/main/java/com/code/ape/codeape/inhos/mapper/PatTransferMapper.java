

package com.code.ape.codeape.inhos.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.inhos.api.dto.PatTransferDTO;
import com.code.ape.codeape.inhos.api.entity.PatTransfer;
import com.code.ape.codeape.inhos.api.vo.PatTransferVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 转科管理
 *
 * @author pig code generator
 * @date 2023-06-04 10:03:26
 */
@Mapper
public interface PatTransferMapper extends BaseMapper<PatTransfer> {
	Page<PatTransferVO> selectPatTransferPage(Page<PatTransferVO> page, @Param("dto") PatTransferDTO dto);
}
