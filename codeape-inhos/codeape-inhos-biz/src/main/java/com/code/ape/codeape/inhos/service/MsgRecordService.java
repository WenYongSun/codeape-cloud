

package com.code.ape.codeape.inhos.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.inhos.api.entity.MsgRecord;

/**
 * 消息日志
 *
 * @author pig code generator
 * @date 2023-06-05 19:27:14
 */
public interface MsgRecordService extends IService<MsgRecord> {
	/**
	 * 重新推送消息
	 * @param msgRecord
	 * @return
	 */
	boolean rePushMq(MsgRecord msgRecord);
}
