

package com.code.ape.codeape.inhos.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.log.annotation.SysLog;
import com.code.ape.codeape.common.security.annotation.InjectAuth;
import com.code.ape.codeape.common.security.annotation.RepeatSubmit;
import com.code.ape.codeape.inhos.api.dto.PatInfoDTO;
import com.code.ape.codeape.inhos.api.vo.PatInfoVO;
import com.code.ape.codeape.inhos.service.PatInfoHotService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 住院患者
 *
 * @author pig code generator
 * @date 2023-06-02 11:34:04
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/pat" )
@Tag(name = "住院患者管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class PatInfoHotController {

    private final  PatInfoHotService patInfoHotService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param dto 住院患者
     * @return
     */
    @Operation(summary = "分页查询在院患者", description = "分页查询在院患者")
    @GetMapping("/page" )
	@InjectAuth
    @PreAuthorize("@pms.hasPermission('inhos_patinfohot_get')" )
    public R<Page<PatInfoVO>> getPatInfoHotPage(Page<PatInfoVO> page, PatInfoDTO dto) {
        return R.ok(patInfoHotService.listPage(page,dto));
    }


	/**
	 * 分页查询
	 * @param page 分页对象
	 * @param dto 查询条件
	 * @return
	 */
	@Operation(summary = "分页查询出院患者", description = "分页查询出院患者")
	@GetMapping("/out/page" )
	@InjectAuth
	@PreAuthorize("@pms.hasPermission('inhos_out_pat_get')" )
	public R<Page<PatInfoVO>> getOutPatPage(Page<PatInfoVO> page, PatInfoDTO dto) {
		return R.ok(patInfoHotService.listOutPage(page,dto));
	}


    /**
     * 通过id查询住院患者
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id查询", description = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('inhos_patinfohot_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(patInfoHotService.getPatById(id));
    }

    /**
     * 新增住院患者
     * @param dto 住院患者
     * @return R
     */
    @Operation(summary = "新增住院患者", description = "新增住院患者")
    @SysLog("新增住院患者" )
    @PostMapping
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('inhos_patinfohot_add')" )
    public R save(@Validated(value = {PatInfoDTO.PatInfoDTOADD.class}) @RequestBody PatInfoDTO dto) {
        return R.ok(patInfoHotService.savePat(dto));
    }

	@Operation(summary = "办理出院", description = "办理出院")
	@SysLog("办理出院" )
	@PostMapping("/leave")
	@RepeatSubmit
	@PreAuthorize("@pms.hasPermission('inhos_patinfo_leave')" )
	public R leavePat(@Validated(value = {PatInfoDTO.PatInfoDTOLeave.class}) @RequestBody PatInfoDTO dto) {
		return R.ok(patInfoHotService.leavePat(dto));
	}

	@Operation(summary = "办理入院", description = "办理入院")
	@SysLog("办理入院" )
	@PostMapping("/in")
	@RepeatSubmit
	@PreAuthorize("@pms.hasPermission('inhos_out_pat_in')" )
	public R inPat(@Validated(value = {PatInfoDTO.PatInfoDTOIN.class}) @RequestBody PatInfoDTO dto) {
		return R.ok(patInfoHotService.inPat(dto));
	}

    /**
     * 修改住院患者
     * @param dto 住院患者
     * @return R
     */
    @Operation(summary = "修改住院患者", description = "修改住院患者")
    @SysLog("修改住院患者" )
    @PutMapping
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('inhos_patinfohot_edit')" )
    public R updateById(@Validated(value = PatInfoDTO.PatInfoDTOUpdate.class)@RequestBody PatInfoDTO dto) {
        return R.ok(patInfoHotService.updatePat(dto));
    }

    /**
     * 通过id删除住院患者
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id删除住院患者", description = "通过id删除住院患者")
    @SysLog("通过id删除住院患者" )
    @DeleteMapping("/{id}" )
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('inhos_patinfohot_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(patInfoHotService.removeById(id));
    }

}
