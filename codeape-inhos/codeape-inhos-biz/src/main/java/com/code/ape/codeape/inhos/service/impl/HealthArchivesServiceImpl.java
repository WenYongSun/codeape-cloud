
package com.code.ape.codeape.inhos.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.ape.codeape.common.core.util.AssertUtil;
import com.code.ape.codeape.common.security.util.SecurityUtils;
import com.code.ape.codeape.inhos.api.dto.HealthArchivesDTO;
import com.code.ape.codeape.inhos.api.entity.HealthArchives;
import com.code.ape.codeape.inhos.api.entity.PatInfoHot;
import com.code.ape.codeape.inhos.api.vo.HealthArchivesVO;
import com.code.ape.codeape.inhos.mapper.HealthArchivesMapper;
import com.code.ape.codeape.inhos.mapper.PatInfoHotMapper;
import com.code.ape.codeape.inhos.service.HealthArchivesService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * 控糖目标
 *
 * @author pig code generator
 * @date 2023-06-14 17:16:37
 */
@Service
@RequiredArgsConstructor
public class HealthArchivesServiceImpl extends ServiceImpl<HealthArchivesMapper, HealthArchives> implements HealthArchivesService {

	private final PatInfoHotMapper patInfoHotMapper;


	/**
	 * 分页获取控糖目标，只提供院内的患者，出院的不需要
	 */
	@Override
	public Page<HealthArchivesVO> listPage(Page<HealthArchivesVO> page, HealthArchivesDTO dto) {
		return baseMapper.selectHealthArchivesPage(page,dto);
	}

	@Transactional
    @Override
    public boolean saveHealthArchives(HealthArchivesDTO dto) {
		//step1查库
		HealthArchives healthArchives = baseMapper.selectOne(Wrappers.<HealthArchives>lambdaQuery()
				.eq(HealthArchives::getTimeType,dto.getTimeType())
				.eq(HealthArchives::getPatId, dto.getPatId()));
		AssertUtil.isTrue(Objects.isNull(healthArchives),"控糖目标已存在！");

		//入库
		HealthArchives bean = BeanUtil.toBean(dto, HealthArchives.class);
		bean.setHosId(Objects.requireNonNull(SecurityUtils.getUser()).getHosId());
		int rows = baseMapper.insert(bean);
		AssertUtil.isTrue(rows==1,"添加失败！");
		return true;
    }

    @Transactional
	@Override
	public boolean updateHealthArchives(HealthArchivesDTO dto) {
		//step1 查库
		HealthArchives original = baseMapper.selectById(dto.getId());
		AssertUtil.isTrue(Objects.nonNull(original),"控糖目标不存在！");
		//step2 更新，只允许更新高低值
		int rows = baseMapper.update(null,Wrappers.<HealthArchives>lambdaUpdate()
				.set(Objects.nonNull(dto.getValueUp()),HealthArchives::getValueUp,dto.getValueUp())
				.set(Objects.nonNull(dto.getValueDown()),HealthArchives::getValueDown,dto.getValueDown()));
		AssertUtil.isTrue(rows==1,"修改失败！");
		return true;
	}

	@Override
	public HealthArchivesVO getByPatId(HealthArchivesDTO dto) {
		//step1 查询患者信息
		PatInfoHot patInfo = patInfoHotMapper.selectById(dto.getPatId());
		AssertUtil.isTrue(Objects.nonNull(patInfo),"该患者不存在！");
		//step2 查询控糖目标
		HealthArchives healthArchives = baseMapper.selectOne(Wrappers.<HealthArchives>lambdaQuery()
				.eq(HealthArchives::getPatId, dto.getPatId())
				.eq(HealthArchives::getHosId, Objects.requireNonNull(SecurityUtils.getUser()).getHosId()));

		HealthArchivesVO vo = BeanUtil.toBean(healthArchives, HealthArchivesVO.class);
		if (Objects.nonNull(vo)){
			vo.setPatName(patInfo.getName());
			vo.setIptNum(patInfo.getIptNum());
		}
		return vo;
	}

	@Override
	public HealthArchivesVO getById(Long id) {
		HealthArchives healthArchives = baseMapper.selectById(id);
		return BeanUtil.toBean(healthArchives,HealthArchivesVO.class);
	}
}
