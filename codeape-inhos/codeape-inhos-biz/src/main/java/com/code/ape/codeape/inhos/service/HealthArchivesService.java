

package com.code.ape.codeape.inhos.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.inhos.api.dto.HealthArchivesDTO;
import com.code.ape.codeape.inhos.api.entity.HealthArchives;
import com.code.ape.codeape.inhos.api.vo.HealthArchivesVO;

/**
 * 控糖目标
 *
 * @author pig code generator
 * @date 2023-06-14 17:16:37
 */
public interface HealthArchivesService extends IService<HealthArchives> {
	Page<HealthArchivesVO> listPage(Page<HealthArchivesVO> page, HealthArchivesDTO dto);
	boolean saveHealthArchives(HealthArchivesDTO dto);
	boolean updateHealthArchives(HealthArchivesDTO dto);
	HealthArchivesVO getByPatId(HealthArchivesDTO dto);
	HealthArchivesVO getById(Long id);
}
