

package com.code.ape.codeape.inhos.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.ape.codeape.inhos.api.entity.PatRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * 患者出入院记录
 *
 * @author pig code generator
 * @date 2023-06-02 17:00:26
 */
@Mapper
public interface PatRecordMapper extends BaseMapper<PatRecord> {

}
