

package com.code.ape.codeape.inhos.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.device.api.dto.QcRecordDTO;
import com.code.ape.codeape.device.api.vo.QcRecordVO;
import com.code.ape.codeape.inhos.api.dto.PatTransferDTO;
import com.code.ape.codeape.inhos.api.entity.PatTransfer;
import com.code.ape.codeape.inhos.api.vo.PatTransferVO;

/**
 * 转科管理
 *
 * @author pig code generator
 * @date 2023-06-04 10:03:26
 */
public interface PatTransferService extends IService<PatTransfer> {
	/**
	 * 分页查询
	 * @param page 分页
	 * @param dto 条件
	 */
	Page<PatTransferVO> listPage(Page<PatTransferVO> page, PatTransferDTO dto);
}
