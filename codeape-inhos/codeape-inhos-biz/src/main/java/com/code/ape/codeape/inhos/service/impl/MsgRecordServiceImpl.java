
package com.code.ape.codeape.inhos.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.ape.codeape.common.core.util.AssertUtil;
import com.code.ape.codeape.inhos.api.entity.MsgRecord;
import com.code.ape.codeape.inhos.api.entity.constant.MsgStatusEnum;
import com.code.ape.codeape.inhos.mapper.MsgRecordMapper;
import com.code.ape.codeape.inhos.service.MsgRecordService;
import lombok.RequiredArgsConstructor;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 消息日志
 *
 * @author pig code generator
 * @date 2023-06-05 19:27:14
 */
@Service
@RequiredArgsConstructor
public class MsgRecordServiceImpl extends ServiceImpl<MsgRecordMapper, MsgRecord> implements MsgRecordService {

	private final RocketMQTemplate rocketMQTemplate;


	@Transactional
	@Override
	public boolean rePushMq(MsgRecord msgRecord) {
		SendResult sendResult = rocketMQTemplate.syncSend(msgRecord.getTopic(), msgRecord.getContent());
		//发送成功
		if (sendResult.getSendStatus().equals(SendStatus.SEND_OK)){
			//更新状态
			msgRecord.setStatus(MsgStatusEnum.MSG_SUCCESS.getValue());
			msgRecord.setFailNum(0);
		}else{  //发送失败，更新失败次数
			msgRecord.setFailNum(1);
			msgRecord.setStatus(MsgStatusEnum.MSG_FAIL.getValue());
		}
		msgRecord.setMsgId(sendResult.getMsgId());
		baseMapper.updateStatus(msgRecord);
		return true;
	}
}
