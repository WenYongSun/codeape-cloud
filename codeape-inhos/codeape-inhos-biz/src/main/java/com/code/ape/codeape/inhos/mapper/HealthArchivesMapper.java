

package com.code.ape.codeape.inhos.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.inhos.api.dto.HealthArchivesDTO;
import com.code.ape.codeape.inhos.api.entity.HealthArchives;
import com.code.ape.codeape.inhos.api.vo.HealthArchivesVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 控糖目标
 *
 * @author pig code generator
 * @date 2023-06-14 17:16:37
 */
@Mapper
public interface HealthArchivesMapper extends BaseMapper<HealthArchives> {
	Page<HealthArchivesVO> selectHealthArchivesPage(Page<HealthArchivesVO> page, @Param("dto") HealthArchivesDTO dto);
}
