

package com.code.ape.codeape.inhos.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.inhos.api.dto.PatInfoDTO;
import com.code.ape.codeape.inhos.api.entity.PatInfoHot;
import com.code.ape.codeape.inhos.api.vo.PatInfoVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 住院患者
 *
 * @author pig code generator
 * @date 2023-06-02 11:34:04
 */
@Mapper
public interface PatInfoHotMapper extends BaseMapper<PatInfoHot> {
	Page<PatInfoVO> selectPatInfoPage(Page<PatInfoVO> page, @Param("dto") PatInfoDTO dto);

	/**
	 * 真正删除，不是逻辑删除
	 */
	int deleteNoLogic(Long id);
}
