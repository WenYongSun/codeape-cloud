
package com.code.ape.codeape.inhos.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.ape.codeape.inhos.api.entity.PatInfoCold;
import com.code.ape.codeape.inhos.mapper.PatInfoColdMapper;
import com.code.ape.codeape.inhos.service.PatInfoColdService;
import org.springframework.stereotype.Service;

/**
 * 住院患者冷数据
 *
 * @author pig code generator
 * @date 2023-06-06 11:40:24
 */
@Service
public class PatInfoColdServiceImpl extends ServiceImpl<PatInfoColdMapper, PatInfoCold> implements PatInfoColdService {

}
