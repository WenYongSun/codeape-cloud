
package com.code.ape.codeape.inhos.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.ape.codeape.inhos.api.entity.PatRecord;
import com.code.ape.codeape.inhos.mapper.PatRecordMapper;
import com.code.ape.codeape.inhos.service.PatRecordService;
import org.springframework.stereotype.Service;

/**
 * 患者出入院记录
 *
 * @author pig code generator
 * @date 2023-06-02 17:00:26
 */
@Service
public class PatRecordServiceImpl extends ServiceImpl<PatRecordMapper, PatRecord> implements PatRecordService {

}
