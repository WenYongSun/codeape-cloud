
package com.code.ape.codeape.inhos.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.ape.codeape.inhos.api.dto.PatTransferDTO;
import com.code.ape.codeape.inhos.api.entity.PatTransfer;
import com.code.ape.codeape.inhos.api.vo.PatTransferVO;
import com.code.ape.codeape.inhos.mapper.PatTransferMapper;
import com.code.ape.codeape.inhos.service.PatTransferService;
import org.springframework.stereotype.Service;

/**
 * 转科管理
 *
 * @author pig code generator
 * @date 2023-06-04 10:03:26
 */
@Service
public class PatTransferServiceImpl extends ServiceImpl<PatTransferMapper, PatTransfer> implements PatTransferService {

    @Override
    public Page<PatTransferVO> listPage(Page<PatTransferVO> page, PatTransferDTO dto) {
		/**
		 * 筛选条件：
		 * 1. 只查在院的，做了冷热分离，只关联pat_info_hot这张表即可
		 * 2. 只查最近一次的入院转科信息，则是根据：转科时间 > 当前患者的入院时间
		 */
		return baseMapper.selectPatTransferPage(page, dto);
	}
}
