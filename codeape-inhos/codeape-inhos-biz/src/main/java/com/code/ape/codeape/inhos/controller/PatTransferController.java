

package com.code.ape.codeape.inhos.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.log.annotation.SysLog;
import com.code.ape.codeape.common.security.annotation.InjectAuth;
import com.code.ape.codeape.inhos.api.dto.PatTransferDTO;
import com.code.ape.codeape.inhos.api.vo.PatTransferVO;
import com.code.ape.codeape.inhos.service.PatTransferService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 转科管理
 *
 * @author pig code generator
 * @date 2023-06-04 10:03:26
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/pat/transfer" )
@Tag(name = "转科管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class PatTransferController {

    private final PatTransferService patTransferService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param dto 转科管理
     * @return
     */
    @Operation(summary = "分页查询转出患者", description = "分页查询转出患者")
    @GetMapping("/page")
	@InjectAuth
    @PreAuthorize("@pms.hasPermission('api_pattransfer_get')" )
    public R<Page<PatTransferVO>> getPatTransferPage(Page<PatTransferVO> page, PatTransferDTO dto) {
        return R.ok(patTransferService.listPage(page, dto));
    }


    /**
     * 通过id查询转科管理
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id查询", description = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('api_pattransfer_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(patTransferService.getById(id));
    }
}
