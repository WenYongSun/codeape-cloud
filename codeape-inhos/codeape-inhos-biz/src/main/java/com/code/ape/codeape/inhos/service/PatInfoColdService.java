

package com.code.ape.codeape.inhos.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.inhos.api.entity.PatInfoCold;

/**
 * 住院患者冷数据
 *
 * @author pig code generator
 * @date 2023-06-06 11:40:24
 */
public interface PatInfoColdService extends IService<PatInfoCold> {

}
