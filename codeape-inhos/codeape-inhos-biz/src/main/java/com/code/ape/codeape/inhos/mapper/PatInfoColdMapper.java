

package com.code.ape.codeape.inhos.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.ape.codeape.inhos.api.entity.PatInfoCold;
import org.apache.ibatis.annotations.Mapper;

/**
 * 住院患者冷数据
 *
 * @author pig code generator
 * @date 2023-06-06 11:40:24
 */
@Mapper
public interface PatInfoColdMapper extends BaseMapper<PatInfoCold> {
	/**
	 * 真正删除，不是逻辑删除
	 */
	int deleteNoLogic(Long id);
}
