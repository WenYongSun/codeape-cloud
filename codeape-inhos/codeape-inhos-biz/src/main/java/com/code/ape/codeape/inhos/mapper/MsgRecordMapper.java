

package com.code.ape.codeape.inhos.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.ape.codeape.inhos.api.entity.MsgRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * 消息日志
 *
 * @author pig code generator
 * @date 2023-06-05 19:27:14
 */
@Mapper
public interface MsgRecordMapper extends BaseMapper<MsgRecord> {

	/**
	 * 更新消息的状态
	 * @param msgRecord
	 * @return
	 */
	int updateStatus(MsgRecord msgRecord);

}
