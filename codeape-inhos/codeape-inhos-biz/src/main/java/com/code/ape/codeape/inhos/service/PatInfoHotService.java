

package com.code.ape.codeape.inhos.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.code.ape.codeape.device.api.dto.QcRecordDTO;
import com.code.ape.codeape.device.api.vo.QcRecordVO;
import com.code.ape.codeape.inhos.api.dto.PatInfoDTO;
import com.code.ape.codeape.inhos.api.entity.PatInfoHot;
import com.code.ape.codeape.inhos.api.vo.PatInfoVO;

/**
 * 住院患者
 *
 * @author pig code generator
 * @date 2023-06-02 11:34:04
 */
public interface PatInfoHotService extends IService<PatInfoHot> {
	/**
	 * 办理住院
	 * @param dto
	 * @return
	 */
	Boolean savePat(PatInfoDTO dto);

	Boolean updatePat(PatInfoDTO dto);

	Page<PatInfoVO> listPage(Page<PatInfoVO> page, PatInfoDTO dto);

	/**
	 * 查询出院患者
	 * @param page 分页对象
	 * @param dto 查询条件
	 * @return
	 */
	Page<PatInfoVO> listOutPage(Page<PatInfoVO> page, PatInfoDTO dto);

	/**
	 * 办理出院
	 * @param dto 参数
	 * @return
	 */
	Boolean leavePat(PatInfoDTO dto);

	/**
	 * 办理住院
	 * @param dto 患者新
	 * @return
	 */
	Boolean inPat(PatInfoDTO dto);

	PatInfoVO getPatById(Long id);
}
