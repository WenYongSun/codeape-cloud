

package com.code.ape.codeape.inhos.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.log.annotation.SysLog;
import com.code.ape.codeape.common.security.annotation.InjectAuth;
import com.code.ape.codeape.common.security.annotation.RepeatSubmit;
import com.code.ape.codeape.inhos.api.dto.HealthArchivesDTO;
import com.code.ape.codeape.inhos.api.entity.HealthArchives;
import com.code.ape.codeape.inhos.api.vo.HealthArchivesVO;
import com.code.ape.codeape.inhos.service.HealthArchivesService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 控糖目标
 *
 * @author pig code generator
 * @date 2023-06-14 17:16:37
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/health/archives" )
@Tag(name = "控糖目标管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class HealthArchivesController {

    private final HealthArchivesService healthArchivesService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param dto 控糖目标
     */
    @Operation(summary = "分页查询-在院患者", description = "分页查询-在院患者")
    @GetMapping("/page" )
	@InjectAuth
    @PreAuthorize("@pms.hasPermission('api_healtharchives_get')" )
    public R<Page<HealthArchivesVO>> getHealthArchivesPage(Page<HealthArchivesVO> page, HealthArchivesDTO dto) {
        return R.ok(healthArchivesService.listPage(page, dto));
    }


	/**
	 * 通过患者ID查询控糖目标
	 * @param dto 参数
	 * @return R
	 */
	@Operation(summary = "通过患者ID查询", description = "通过患者ID查询")
	@GetMapping("/pat" )
	@PreAuthorize("@pms.hasPermission('api_healtharchives_get')" )
	public R<HealthArchivesVO> getByPatId(@Validated(value = HealthArchivesDTO.HealthArchivesDTOPatId.class) @RequestBody HealthArchivesDTO dto) {
		return R.ok(healthArchivesService.getByPatId(dto));
	}


    /**
     * 通过id查询控糖目标
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id查询", description = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('api_healtharchives_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(healthArchivesService.getById(id));
    }

    /**
     * 新增控糖目标
     * @param dto 控糖目标
     * @return R
     */
    @Operation(summary = "新增控糖目标", description = "新增控糖目标")
    @SysLog("新增控糖目标" )
    @PostMapping
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('api_healtharchives_add')" )
    public R save(@Validated(value = {HealthArchivesDTO.HealthArchivesDTOAdd.class}) @RequestBody HealthArchivesDTO dto) {
        return R.ok(healthArchivesService.saveHealthArchives(dto));
    }

    /**
     * 修改控糖目标
     * @param dto 控糖目标
     * @return R
     */
    @Operation(summary = "修改控糖目标", description = "修改控糖目标")
    @SysLog("修改控糖目标" )
    @PutMapping
	@RepeatSubmit
    @PreAuthorize("@pms.hasPermission('api_healtharchives_edit')" )
    public R updateById(@RequestBody HealthArchivesDTO dto) {
        return R.ok(healthArchivesService.updateHealthArchives(dto));
    }

    /**
     * 通过id删除控糖目标
     * @param id id
     * @return R
     */
    @Operation(summary = "通过id删除控糖目标", description = "通过id删除控糖目标")
    @SysLog("通过id删除控糖目标" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('api_healtharchives_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(healthArchivesService.removeById(id));
    }

}
