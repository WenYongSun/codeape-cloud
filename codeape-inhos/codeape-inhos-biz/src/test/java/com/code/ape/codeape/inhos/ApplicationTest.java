package com.code.ape.codeape.inhos;

import com.alibaba.fastjson.JSON;
import com.code.ape.codeape.common.elasticsearch.ResetElasticSearchClient;
import com.code.ape.codeape.inhos.api.entity.PatInfoHot;
import com.code.ape.codeape.inhos.api.entity.constant.PatInfoConstant;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CodeapeInHosApplication.class)
public class ApplicationTest {

	@Autowired
	private  ResetElasticSearchClient resetElasticSearchClient;

	@Test
	public void test1() throws Exception {
		DeleteResponse delete = resetElasticSearchClient.delete(PatInfoConstant.PAT_INFO_INDEX_NAME, "1665959006283984897");
		System.out.println(JSON.toJSONString(delete));
	}
}
