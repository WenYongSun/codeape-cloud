

package com.code.ape.codeape.common.security.service;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2019/2/1 扩展用户信息
 */
public class CodeapeUser extends User implements OAuth2AuthenticatedPrincipal {

	private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

	private final Map<String, Object> attributes = new HashMap<>();

	/**
	 * 用户ID
	 */
	@Getter
	@JsonSerialize(using = ToStringSerializer.class)
	private final Long id;

	/**
	 * 部门ID
	 */
	@Getter
	@JsonSerialize(using = ToStringSerializer.class)
	private final Long deptId;

	/**
	 * 当前登录医院的hosId，不是所在医院的hosId
	 */
	@Getter
	@JsonSerialize(using = ToStringSerializer.class)
	private final Long hosId;

	@Getter
	private final Long[] deptAuths;

	/**
	 * 角色列表
	 */
	@Getter
	private final String[] roleCodes;

	/**
	 * 手机号
	 */
	@Getter
	private final String phone;

	/**
	 * 客户端ID
	 */
	@Getter
	private final String clientId;

	/**
	 * 设备的SN，只有clientId=PDA才有值
	 */
	@Getter
	private final String sn;

	public CodeapeUser(Long id, Long deptId, String username, String password, String phone, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked,Long hosId,Long[] deptAuths,
			String[] roleCodes,
			Collection<? extends GrantedAuthority> authorities,String clientId,String sn) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.id = id;
		this.deptId = deptId;
		this.phone = phone;
		this.hosId=hosId;
		this.deptAuths=deptAuths;
		this.roleCodes=roleCodes;
		this.clientId=clientId;
		this.sn=sn;
	}

	/**
	 * Get the OAuth 2.0 token attributes
	 * @return the OAuth 2.0 token attributes
	 */
	@Override
	public Map<String, Object> getAttributes() {
		return this.attributes;
	}

	@Override
	public String getName() {
		return this.getUsername();
	}

}
