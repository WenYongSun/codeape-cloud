package com.code.ape.codeape.common.security.annotation;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description 防重注解
 */

import java.lang.annotation.*;

@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RepeatSubmit {
	/**
	 * 限制时长，单位秒
	 */
	long seconds() default 5;
}
