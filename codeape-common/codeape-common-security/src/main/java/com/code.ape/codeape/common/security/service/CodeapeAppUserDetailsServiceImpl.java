

package com.code.ape.codeape.common.security.service;

import com.code.ape.codeape.admin.api.dto.UserInfo;
import com.code.ape.codeape.admin.api.feign.RemoteUserService;
import com.code.ape.codeape.common.core.constant.CacheConstants;
import com.code.ape.codeape.common.core.constant.SecurityConstants;
import com.code.ape.codeape.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * 用户详细信息
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究 hccake
 */
@Slf4j
@RequiredArgsConstructor
public class CodeapeAppUserDetailsServiceImpl implements CodeapeUserDetailsService {

	private final RemoteUserService remoteUserService;

	private final CacheManager cacheManager;

	/**
	 * 手机号登录
	 * @param phone 手机号
	 * @return
	 */
	@Override
	@SneakyThrows
	public UserDetails loadUserByUsername(String phone) {
		Cache cache = cacheManager.getCache(CacheConstants.USER_DETAILS);
		if (cache != null && cache.get(phone) != null) {
			return (CodeapeUser) cache.get(phone).get();
		}

		R<UserInfo> result = remoteUserService.infoByMobile(phone);

		UserDetails userDetails = getUserDetails(result);
		if (cache != null) {
			cache.put(phone, userDetails);
		}
		return userDetails;
	}

	/**
	 * check-token 使用
	 * @param codeapeUser user
	 * @return
	 */
	@Override
	public UserDetails loadUserByUser(CodeapeUser codeapeUser) {
		return this.loadUserByUsername(codeapeUser.getPhone());
	}

	/**
	 * 是否支持此客户端校验
	 * @param clientId 目标客户端
	 * @return true/false
	 */
	@Override
	public boolean support(String clientId, String grantType) {
		return SecurityConstants.APP.equals(grantType);
	}

	@Override
	public UserDetails loadUserByUsernameAndOther(String username, String... others) throws UsernameNotFoundException {
		return null;
	}
}
