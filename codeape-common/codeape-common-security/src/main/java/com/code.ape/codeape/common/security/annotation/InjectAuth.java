package com.code.ape.codeape.common.security.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description 业务注解，自动注入当前用户的相关权限
 * 自动注入：{@link com.code.ape.codeape.common.core.entity.BaseParam}
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface InjectAuth {
//	String[] type() default {"1", "0"}; //1,院内数据权限,0院外数据权限

	boolean enable() default true;  //是否开启数据检测
}
