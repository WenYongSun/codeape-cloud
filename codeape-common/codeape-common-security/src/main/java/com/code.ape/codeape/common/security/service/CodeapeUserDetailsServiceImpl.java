

package com.code.ape.codeape.common.security.service;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.code.ape.codeape.admin.api.dto.UserInfo;
import com.code.ape.codeape.admin.api.entity.SysUser;
import com.code.ape.codeape.admin.api.feign.RemoteUserService;
import com.code.ape.codeape.common.core.constant.CacheConstants;
import com.code.ape.codeape.common.core.constant.CommonConstants;
import com.code.ape.codeape.common.core.constant.SecurityConstants;
import com.code.ape.codeape.common.core.util.MD5Util;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.core.util.RetOps;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * 处理用户名、密码登录，WEB端的逻辑
 * 自定义
 */
@Slf4j
@Primary
@RequiredArgsConstructor
public class CodeapeUserDetailsServiceImpl implements CodeapeUserDetailsService {

	private final RemoteUserService remoteUserService;

	private final CacheManager cacheManager;

	/**
	 * 用户名密码登录
	 * @param username 用户名
	 * @return
	 */
	@Override
	@SneakyThrows
	public UserDetails loadUserByUsername(String username) {
		Cache cache = cacheManager.getCache(CacheConstants.USER_DETAILS);
		if (cache != null && cache.get(username) != null) {
			return (CodeapeUser) cache.get(username).get();
		}

		R<UserInfo> result = remoteUserService.info(username);
		UserDetails userDetails = getUserDetails(result);
		if (cache != null) {
			cache.put(username, userDetails);
		}
		return userDetails;
	}

	//优先级最高
	@Override
	public int getOrder() {
		return Integer.MIN_VALUE;
	}

	/**
	 * 仅仅支持WEB的密码模式登录
	 * @param clientId 目标客户端
	 * @param grantType  授权类型
	 */
	@Override
	public boolean support(String clientId, String grantType) {
		return StrUtil.equals(clientId,SecurityConstants.WEB)&&StrUtil.equals(grantType,"password");
	}

	@Override
	public UserDetails loadUserByUsernameAndOther(String username, String... others) throws UsernameNotFoundException {
		Long hosId=Long.valueOf(others[0]);
		Cache cache = cacheManager.getCache(CacheConstants.USER_DETAILS);
		String key= MD5Util.encrypt(MessageFormat.format(CacheConstants.USER_DETAILS_INFO,username,hosId.toString()));
		if (cache != null && cache.get(key) != null) {
			return (CodeapeUser) cache.get(key).get();
		}
		R<UserInfo> result = remoteUserService.loadInfoByUserNameAndHosId(username,hosId);
		UserDetails userDetails = getUserDetails(result,SecurityConstants.WEB,hosId);
		if (cache != null) {
			cache.put(key, userDetails);
		}
		return userDetails;
	}

	@Override
	public UserDetails loadUserByUser(CodeapeUser codeapeUser) {
		return this.loadUserByUsernameAndOther(codeapeUser.getUsername(),codeapeUser.getHosId().toString());
	}

	@Override
	public UserDetails getUserDetails(R<UserInfo> result, String clientId, Long hosId) {
		UserInfo info = RetOps.of(result).getData().orElseThrow(() -> new UsernameNotFoundException("用户不存在"));

		Set<String> dbAuthsSet = new HashSet<>();

		if (ArrayUtil.isNotEmpty(info.getRoles())) {
			// 获取角色
			Arrays.stream(info.getRoles()).forEach(role -> dbAuthsSet.add(SecurityConstants.ROLE + role));
			// 获取资源
			dbAuthsSet.addAll(Arrays.asList(info.getPermissions()));
		}

		Collection<GrantedAuthority> authorities = AuthorityUtils
				.createAuthorityList(dbAuthsSet.toArray(new String[0]));
		SysUser user = info.getSysUser();

		//角色集合
		String[] roleCodes = info.getRoleList().stream().map(data-> SecurityConstants.ROLE+data.getRoleCode()).toArray(String[]::new);

		// 构造security用户
		return new CodeapeUser(user.getUserId(), user.getDeptId(), user.getUsername(),
				SecurityConstants.BCRYPT + user.getPassword(), user.getPhone(), true, true, true,
				StrUtil.equals(user.getLockFlag(), CommonConstants.STATUS_NORMAL),hosId,info.getDeptAuths(),roleCodes,authorities,clientId,null);
	}
}
