package com.code.ape.codeape.common.security.service;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.code.ape.codeape.admin.api.dto.UserInfo;
import com.code.ape.codeape.admin.api.entity.SysUser;
import com.code.ape.codeape.common.core.constant.CommonConstants;
import com.code.ape.codeape.common.core.constant.SecurityConstants;
import com.code.ape.codeape.common.core.util.AssertUtil;
import com.code.ape.codeape.common.core.util.R;
import com.code.ape.codeape.common.core.util.RetOps;
import com.code.ape.codeape.device.api.vo.DeviceInfoVO;
import org.springframework.core.Ordered;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2021/12/21
 */
public interface CodeapeUserDetailsService extends UserDetailsService, Ordered {

	/**
	 * 是否支持此客户端校验
	 * @param clientId 目标客户端
	 * @return true/false
	 */
	default boolean support(String clientId, String grantType) {
		return true;
	}

	/**
	 * 排序值 默认取最大的
	 * @return 排序值
	 */
	default int getOrder() {
		return 0;
	}

	/**
	 * 根据userName和其他字段的登录
	 * @param username  用户名
	 * @param others  其他的字段（比如hosId，SN号）
	 * @throws UsernameNotFoundException
	 */
	UserDetails loadUserByUsernameAndOther(String username,String... others) throws UsernameNotFoundException;


	/**
	 * 构建userdetails WEB登录+pad端登录所有需的处理接口，这里的登录用户权限：当前用户的权限集合
	 * @param result 用户信息
	 * @param clientId 客户端ID
	 * @param hosId 医院Id
	 * @return UserDetails
	 */
	default UserDetails getUserDetails(R<UserInfo> result,String clientId,Long hosId) {
		return null;
	}


	/**
	 * 构建userdetails PDA端登录所需的接口，这里的登录用户权限：设备的权限+当前用户的权限集合
	 * @param result 用户信息
	 * @param clientId 客户端ID
	 * @param deviceInfoVO 设备的信息
	 * @param sn PDA设备的SN号
	 * @return UserDetails
	 */
	default UserDetails getUserDetails(R<UserInfo> result, DeviceInfoVO deviceInfoVO,String clientId, String sn) {
		return null;
	}

	/**
	 * 构建userdetails
	 * @param result 用户信息
	 * @return UserDetails
	 */
	@Deprecated
	default UserDetails getUserDetails(R<UserInfo> result) {
		UserInfo info = RetOps.of(result).getData().orElseThrow(() -> new UsernameNotFoundException("用户不存在"));

		Set<String> dbAuthsSet = new HashSet<>();

		if (ArrayUtil.isNotEmpty(info.getRoles())) {
			// 获取角色
			Arrays.stream(info.getRoles()).forEach(role -> dbAuthsSet.add(SecurityConstants.ROLE + role));
			// 获取资源
			dbAuthsSet.addAll(Arrays.asList(info.getPermissions()));
		}

		Collection<GrantedAuthority> authorities = AuthorityUtils
			.createAuthorityList(dbAuthsSet.toArray(new String[0]));
		SysUser user = info.getSysUser();

		//角色集合
		String[] roleCodes = info.getRoleList().stream().map(data-> SecurityConstants.ROLE+data.getRoleCode()).toArray(String[]::new);

		return null;
//		// 构造security用户
//		return new CodeapeUser(user.getUserId(), user.getDeptId(), user.getUsername(),
//				SecurityConstants.BCRYPT + user.getPassword(), user.getPhone(), true, true, true,
//				StrUtil.equals(user.getLockFlag(), CommonConstants.STATUS_NORMAL),user.getHosId(),info.getDeptAuths(),roleCodes,authorities);
	}

	/**
	 * 通过用户实体查询
	 * @param codeapeUser user
	 * @return
	 */
	default UserDetails loadUserByUser(CodeapeUser codeapeUser) {
		return this.loadUserByUsernameAndOther(codeapeUser.getUsername(),codeapeUser.getHosId().toString());
	}

}
