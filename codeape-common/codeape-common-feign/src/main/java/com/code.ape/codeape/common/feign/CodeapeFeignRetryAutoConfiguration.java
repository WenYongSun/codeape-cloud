

package com.code.ape.codeape.common.feign;

import com.code.ape.codeape.common.feign.retry.FeignRetryAspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.support.RetryTemplate;

/**
 * 重试配置
 *
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2023年03月09日
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(RetryTemplate.class)
public class CodeapeFeignRetryAutoConfiguration {

	@Bean
	public FeignRetryAspect feignRetryAspect() {
		return new FeignRetryAspect();
	}

}
