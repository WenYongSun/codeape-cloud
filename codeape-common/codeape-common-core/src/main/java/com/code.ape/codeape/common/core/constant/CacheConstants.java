

package com.code.ape.codeape.common.core.constant;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2020年01月01日
 * <p>
 * 缓存的key 常量
 */
public interface CacheConstants {

	/**
	 * oauth 缓存前缀
	 */
	String PROJECT_OAUTH_ACCESS = "token::access_token";

	/**
	 * oauth 缓存令牌前缀
	 */
	String PROJECT_OAUTH_TOKEN = "codeape_oauth:token:";

	/**
	 * 验证码前缀
	 */
	String DEFAULT_CODE_KEY = "DEFAULT_CODE_KEY:";

	/**
	 * 菜单信息缓存
	 */
	String MENU_DETAILS = "menu_details";

	/**
	 * 用户信息缓存
	 */
	String USER_DETAILS = "user_details";

	/**
	 * 单个用户信息的缓存，码猿慢病云管理系统中的username+hosId联合主键是唯一的
	 * {0}:username
	 * {1}:hosId
	 */
	String USER_DETAILS_INFO="{0}_{1}";

	/**
	 * 字典信息缓存
	 */
	String DICT_DETAILS = "dict_details";

	/**
	 * oauth 客户端信息
	 */
	String CLIENT_DETAILS_KEY = "client:details";

	/**
	 * 参数缓存
	 */
	String PARAMS_DETAILS = "params_details";

	/**
	 * 检测时间段的缓存
	 */
	String SYS_TIME_TYPE="sys_time_type";

	/**
	 * 当前医院设备SN缓存
	 */
	String DEVICE_DEVICE_SN="device_device_sn";

	/**
	 * 防重提交的缓存，{0}：URI，{1}：IP，{2}：入参
	 */
	String REPEAT_SUBMIT_KEY="repeat:{0}:{1}:{2}";

	/**
	 * 医院的预警缓存
	 */
	String SYS_WARN_HOS="sysWarn:hos";

}
