
package com.code.ape.codeape.common.core.constant.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2019-05-16
 * <p>
 * 删除/正常状态
 */
@Getter
@RequiredArgsConstructor
public enum DelFlagEnum {

	/**
	 * 状态已删除
	 */
	DELETE("1", "已删除"),

	/**
	 * 状态正常
	 */
	NORMAL("0", "正常");

	/**
	 * 类型
	 */
	private final String value;

	/**
	 * 描述
	 */
	private final String description;

}
