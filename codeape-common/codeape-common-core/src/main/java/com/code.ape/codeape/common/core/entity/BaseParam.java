package com.code.ape.codeape.common.core.entity;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description 封装基础请求实体，自动注入一些属性
 */
@Data
public class BaseParam {

	/**
	 * 医院Id
	 */
	private Long hosId;

	/**
	 * 用户ID
	 */
	private Long userId;

	/**
	 * 医护的科室权限
	 */
	private List<Long> dataAuth;

	/**
	 * 请求来源客户端ID
	 */
	private String clientId;

	/**
	 * 设备的SN号
	 */
	private String sn;
}
