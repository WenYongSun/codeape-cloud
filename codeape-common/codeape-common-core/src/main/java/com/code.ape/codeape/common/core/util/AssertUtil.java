package com.code.ape.codeape.common.core.util;

import cn.hutool.core.util.StrUtil;
import com.code.ape.codeape.common.core.exception.ServiceException;
import org.springframework.lang.Nullable;

import java.util.Objects;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description 业务校验断言工具类
 */
public class AssertUtil {

	public static void isTrue(boolean res,String msg){
		if (!res)
			throw new ServiceException(msg);
	}

	public static void hasText(String text,String msg){
		if (StrUtil.isBlank(text))
			throw new ServiceException(msg);
	}

	public static void isNull(@Nullable Object object, String msg){
		if (Objects.nonNull(object))
			throw new ServiceException(msg);
	}

	public static void nonNull(@Nullable Object object, String msg){
		if (Objects.isNull(object))
			throw new ServiceException(msg);
	}
}
