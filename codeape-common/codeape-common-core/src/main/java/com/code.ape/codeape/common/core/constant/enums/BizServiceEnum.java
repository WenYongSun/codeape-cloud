package com.code.ape.codeape.common.core.constant.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */
@Getter
@RequiredArgsConstructor
public enum BizServiceEnum {
	PHONE_EXIST(10001,"该手机号码已经注册！");
	private final int code;
	private final String msg;
}
