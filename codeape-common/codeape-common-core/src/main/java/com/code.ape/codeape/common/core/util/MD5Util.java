package com.code.ape.codeape.common.core.util;

import org.springframework.util.DigestUtils;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description
 */

public class MD5Util {

	public static String encrypt(String value){
		return DigestUtils.md5DigestAsHex(value.getBytes());
	}
}
