

package com.code.ape.codeape.common.core.constant;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2018年06月22日16:41:01 服务名称
 */
public interface ServiceNameConstants {

	/**
	 * 认证服务的SERVICEID
	 */
	String AUTH_SERVICE = "codeape-auth";

	/**
	 * UMPS模块
	 */
	String UMPS_SERVICE = "codeape-upms-biz";

	String DEVICE_SERVICE="codeape-device-biz";

}
