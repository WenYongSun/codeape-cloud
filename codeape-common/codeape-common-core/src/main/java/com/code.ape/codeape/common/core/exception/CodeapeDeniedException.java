

package com.code.ape.codeape.common.core.exception;

import lombok.NoArgsConstructor;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究
 * @date 2018年06月22日16:22:03 403 授权拒绝
 */
@NoArgsConstructor
public class CodeapeDeniedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CodeapeDeniedException(String message) {
		super(message);
	}

	public CodeapeDeniedException(Throwable cause) {
		super(cause);
	}

	public CodeapeDeniedException(String message, Throwable cause) {
		super(message, cause);
	}

	public CodeapeDeniedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
