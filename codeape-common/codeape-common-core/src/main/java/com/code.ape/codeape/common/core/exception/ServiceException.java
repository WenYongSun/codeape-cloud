package com.code.ape.codeape.common.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description 业务处理异常
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ServiceException extends RuntimeException{
	public ServiceException(String msg) {
		super(msg);
	}
}
