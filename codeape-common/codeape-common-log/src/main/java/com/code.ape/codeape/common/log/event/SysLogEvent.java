
package com.code.ape.codeape.common.log.event;

import com.code.ape.codeape.admin.api.entity.SysLog;
import org.springframework.context.ApplicationEvent;

/**
 * @author 公众号：码猿技术专栏   版权：不才陈某所署，侵权必究 系统日志事件
 */
public class SysLogEvent extends ApplicationEvent {

	public SysLogEvent(SysLog source) {
		super(source);
	}

}
