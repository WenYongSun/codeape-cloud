package com.code.ape.codeape.common.elasticsearch.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description ElasticSearch的属性配置类
 */
@Data
@ConfigurationProperties(prefix = "elasticsearch")
public class ElasticSearchProperties {
	/**
	 * 用户名
	 */
	private String user;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * ElasticSearch的地址，集群地址用逗号分隔
	 */
	private String hosts;

	/**
	 * 是否开启
	 */
	private Boolean enabled;
}
