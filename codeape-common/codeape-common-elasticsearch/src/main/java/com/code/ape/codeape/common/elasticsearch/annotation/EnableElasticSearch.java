package com.code.ape.codeape.common.elasticsearch.annotation;

import com.code.ape.codeape.common.elasticsearch.config.ElasticSearchClientAutoConfig;
import com.code.ape.codeape.common.elasticsearch.properties.ElasticSearchProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author 公众号：码猿技术专栏
 * @url: www.java-family.cn
 * @description  ES模块的注解，标注在项目的启动类则自动注入Client
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@EnableConfigurationProperties(ElasticSearchProperties.class)
@Import({ ElasticSearchClientAutoConfig.class })
public @interface EnableElasticSearch {
}
